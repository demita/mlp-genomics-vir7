import egglib, os

ctg = 'LG_15'
mutation_pos = 2233368 # 0-based

# get reference genome
ref = egglib.io.from_fasta(
    os.path.expanduser('~/data/projects/rust_genome/Mellp2_3/Mellp2_3_AssemblyScaffolds_Repeatmasked.fasta'),
    alphabet=egglib.alphabets.DNA)
print(len(ref), 'contigs')

# get annotation
gff3 = egglib.io.GFF3(
    os.path.expanduser('~/data/projects/rust_genome/Mellp2_3/Mellp2_3_GeneCatalog_20151130.gff3'))

desc = None
for fea in gff3:
    if fea.seqid == ctg and mutation_pos >= fea.start and mutation_pos <= fea.end:
        assert desc is None
        [desc] = fea.descendants
assert desc.type == 'mRNA'

pos = None
strand = None
for frag in desc.descendants:
    if frag.type == 'CDS':
        assert pos is None
        pos = [(i, j+1) for (i,j,k) in frag.segments]
        strand = frag.strand

# get CDS, and protein sequence
assert strand == '-'
seq = ref.find(ctg).sequence
cds = ''.join([seq[i:j] for (i,j) in pos])
cds = egglib.tools.rc(cds)
prot = egglib.tools.translate(cds)

# get position of mutation in protein
# iterate over cds positions
codons = []
codon = []
for a, b in pos[::-1]:
    for p in range(a, b)[::-1]:
        codon.append(p)
        if len(codon) == 3:
            codons.append(codon)
            codon = []
assert codon == []

# find codon hit by mutation
codon = None
for cod in codons:
    if mutation_pos in cod:
        assert codon is None
        codon = cod
        idx = codon.index(mutation_pos)

print('position in prot:', codons.index(codon)+1)
codon = seq[codon[2]]+seq[codon[1]]+seq[codon[0]]
assert seq[mutation_pos] == 'C'
assert idx == 0
print(egglib.tools.rc(codon)) # -> GGT (glycine, G)
# mutant is AGT (serine, S)
