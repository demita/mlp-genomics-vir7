"""
Make plot of freq-hmm results.

PATH: work directory
K: k parameter (among the ones that have been used), as a string
"""

from matplotlib import pyplot, rc as matplotlib_rc
import os, sys, math
sys.path.append('../data')
import lib

PATH = os.path.expanduser('~/disks/backup2/work/pop-genomics-work')
K = '1e-15'

samples = ['1993', '1994', '1998', '2008', 'vir7']
matplotlib_rc('font', size=16)

# offset for all chromosomes
offset = {}
mids = []
acc = 0
for LG in lib.LGs:
    offset[LG] = acc
    mids.append(acc + lib.LGs[LG] / 2)
    acc += lib.LGs[LG]

# initiate main graphic
mainF, mainA = pyplot.subplots(5, 1, figsize=(40, 20))
all_sweeps = []

print('processing LG00 0000', end='')
for LG in lib.LGs:

    # LG graphic
    fig, axes = pyplot.subplots(5, 1, figsize=(40, 20))

    for idxsam, sam in enumerate(samples):
        pred = []
        probas = ([], [])
        sweeps = []
        print('\b'*9 + LG + ' ' + sam, end='', flush=True)

        # load site mapping
        mapping = []
        with open(os.path.join(PATH, 'freq-hmm', sam + '-{0}.segemit'.format(LG))) as f:
            for line in f:
                ch, pos, p1, p2, p3 = line.split()
                assert ch == LG
                mapping.append(int(pos))

        # load predictions
        with open(os.path.join(PATH, 'freq-hmm', '{0}-{1}_{2}.pred'.format(sam, LG, K))) as f:
            for i, line in enumerate(f):
                pos, p = line.split()
                pos = int(pos)
                p = int(pos)
                assert pos == mapping[i]
                pred.append((pos, p))
            assert i == len(mapping)-1

        # load probas
        with open(os.path.join(PATH, 'freq-hmm', '{0}-{1}_{2}.post'.format(sam, LG, K))) as f:
            for i, line in enumerate(f):
                pos, p = line.split()
                pos = int(pos)
                p = float(p)
                assert pos == mapping[i]
                probas[0].append(pos)
                probas[1].append(p)
            assert i == len(mapping)-1

        # load sweep locations
        with open(os.path.join(PATH, 'freq-hmm', '{0}-{1}_{2}.stat'.format(sam, LG, K))) as f:
            num = int(f.readline())
            cur = 0
            for i in range(num):
                p1, p2, P = f.readline().split()
                p1 = int(p1)
                p2 = int(p2)
                p = float(p)
                assert p1 > cur and p2 > p1
                cur = p2
                sweeps.append((p1, p2))
                all_sweeps.append((LG, sam, p1, p2, p))
            assert f.readline() == ''

        # load data in main graphic
        ax = mainA[idxsam]
        #ax.axvline(offset['LG15'] + 2233055, ls=':', c='k') # position of candidate gene
        x, y = probas
        x = [i+offset[LG] for i in x]
        y = list(map(math.log, y))
        ax.plot(x, y, marker='o', ms=1, ls='None')

        # add sweep locations
        x = [(i+offset[LG], j-i) for (i,j) in sweeps]
        ax.broken_barh(x, (0, 5), color='r')

        # load data in LG graphic
        ax = axes[idxsam]
        #if LG == 'LG15':
        #    ax.axvline(2233055/1e6, ls=':', c='k') # position of candidate gene
        x, y = probas
        x = [i/1e6 for i in x]
        y = list(map(math.log, y))
        ax.plot(x, y, marker='o', ms=1, ls='None', c='0.5')

        # add sweep locations
        x = [(i/1e6, (j-i)/1e6) for (i,j) in sweeps]
        ax.broken_barh(x, (0, 5), color='r')

    # save LG figure
    ticksM = [i for i in range(int(lib.LGs[LG]/1e6)+1)]
    ticksm = [i/10 for i in range(0, int(lib.LGs[LG]/1e5), 2)]
    for sam, ax in zip(samples, axes):
        ax.set_xlim(0, lib.LGs[LG]/1e6)
        t = ax.text(0.5, 0.90, sam, horizontalalignment='center', verticalalignment='center', transform=ax.transAxes)
        t.set_fontweight('bold')
        ax.set_ylabel('log P')
        ax.set_xticks(ticksM)
        ax.set_xticks(ticksm, minor=True)
        ax.xaxis.set_tick_params(which='major', length=8)
        ax.xaxis.set_tick_params(which='minor', length=4)
        ax.set_xticklabels([])
    ax.set_xticks(ticksM)
    ax.set_xticks(ticksm, minor=True)
    ax.set_xticklabels(ticksM)
    axes[-1].set_xlabel('Position (Mbp)')
    fig.suptitle(LG, fontsize=16)
    fig.savefig('plots/{0}.png'.format(LG))
    pyplot.close(fig)

print('\b'*9 + 'done     ')

# save main figure
print('saving', end='', flush=True)
for ax in mainA:
    ax.set_xlim(0, sum(lib.LGs.values()))
    ax.xaxis.set_tick_params(color='None')
    ax.set_xticks([])
for ax, sam in zip(mainA, ['1993', '1994', '1998', '2008', 'vir7']):
    t = ax.set_title(sam)
    t.set_fontweight('bold')
    ax.set_ylabel('log P')
mainA[-1].set_xticks(mids)
mainA[-1].set_xticklabels(lib.LGs.keys())
mainF.savefig('main.png')
print(' done')

# export sweeps
all_sweeps.sort(key=lambda x: (int(x[0][2:]), x[2]))
with open('sweeps.txt', 'w') as f:
    f.write('LG sample start end proba\n')
    for sw in all_sweeps:
        f.write(' '.join(map(str, sw)) + '\n')

