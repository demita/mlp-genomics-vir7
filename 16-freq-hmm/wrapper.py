"""
Wrapper of freq-hmm. The spectrum phase is perform only if .spectrum
files are not present in the work directory.


PATH: work directory
PATHfreq_hmm: path to freq-hmm python program
NUM_THREADS: number of threads
"""

# header
import subprocess, os, sys, shutil
PATH = os.path.expanduser('~/disks/backup2/work/pop-genomics-work')
PATHfreq_hmm = os.path.expanduser('~/data/software/freq-hmm/freq-hmm.py')
NUM_THREADS = 8
K_list = [1e-8, 1e-7, 1e-6, 1e-5, 5e-9, 2e-9, 1e-9, 5e-10, 1e-10, 1e-11, 1e-12, 1e-15, 1e-20, 1e-25]

# population parameters (number of samples and estimated theta to be used as starting value)
params = {
    '1993': (36, 0.0020),
    '1994': (36, 0.0018),
    '1998': (42, 0.0020),
    '2008': (38, 0.0023),
    'vir7': (78, 0.0020)
}

# process all chromosomes separately
for K in K_list:
    for LGi in range(18):
        LG = 'LG{0:0>2}'.format(LGi+1)

        for sample, (ns, t) in params.items():

            # generate spectrum
            path = os.path.join(PATH, 'freq-hmm', sample + '-' + LG)

            if not os.path.isfile(path + '.spectrum'):
                print('### {0}-{1} -- spectrum ###'.format(LG, sample))
                p = subprocess.Popen(['python2', PATHfreq_hmm, 
                    '-f', path,
                    '-R', LG,
                    '-n', str(ns),
                    '--ancestral-allele', 'unknown',
                    '-P', str(NUM_THREADS),
                    '--only-spectrum',
                    '--ratio', str(10),
                    '--theta', str(t)])
                p.communicate()
                shutil.move(path + '_{0}.spectrum'.format(LG), path + '.spectrum')

            # prediction
            if not os.path.isfile(path + '_{0}.post'.format(K)):
                print('### {0}-{1} -- prediction {2} ###'.format(LG, sample, K))
                args = ['python2', PATHfreq_hmm, 
                    '-f', path,
                    '-n', str(ns),
                    '--ancestral-allele', 'unknown',
                    '-P', str(NUM_THREADS),
                    '--pred',
                    '-k', str(K)]
                if os.path.isfile(path + '.segemit'):
                    args.append('--emit-file')
                else:
                    args.extend(['--spectrum-file', path])
                p = subprocess.Popen(args)
                p.communicate()

                shutil.move(path + '.post', path + '_{0}.post'.format(K))
                shutil.move(path + '.pred', path + '_{0}.pred'.format(K))
                shutil.move(path + '.stat', path + '_{0}.stat'.format(K))
