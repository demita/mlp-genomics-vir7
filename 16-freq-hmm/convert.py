"""
Convert SNP data into the file format for freq-hmm

PATH: work directory
NUM_THREADS: number of parallelization threads
"""

# header
import os, sys, multiprocessing, queue
PATH = os.path.expanduser('~/disks/backup2/work/pop-genomics-work')
NUM_THREADS = 8

# sample structure
check = ['08EA01', '08EA06', '08EA09', '08EA105', '08EA106', '08EA11', '08EA15', '08EA16', '08EA18', '08EA19', '08EA22', '08EA42', '08EA43', '08EA50', '08EA55', '08EA61', '08EA66', '08EA85', '08EA95', '93CU01', '93CV01', '93DL02', '93EA02', '93EC04', '93EF08', '93GR07', '93GS03', '93GT01', '93GV08', '93GW03', '93HW05', '93IB07', '93IC08', '93NU02', '93NX06', '93OC05', '93Q01', '94ZZ01', '94ZZ03', '94ZZ07', '94ZZ10', '94ZZ11', '94ZZ12', '94ZZ13', '94ZZ14', '94ZZ15', '94ZZ16', '94ZZ18', '94ZZ19', '94ZZ20', '94ZZ21', '94ZZ33', '94ZZ35', '94ZZ36', '94ZZ37', '98AA06', '98AB02', '98AB07', '98AC08', '98AD04', '98AG03', '98AG18', '98AG36', '98AG42', '98AG49', '98AG54', '98AG56', '98AG69', '98AI01', '98AI35', '98AI58', '98AP02', '98AP03', '98GB03', '98GC04', '98GD05']
samples = {
    '1993': [i for i,v in enumerate(check) if v[:2] == '93'],
    '1994': [i for i,v in enumerate(check) if v[:2] == '94'],
    '1998': [i for i,v in enumerate(check) if v[:2] == '98'],
    '2008': [i for i,v in enumerate(check) if v[:2] == '08'],
    'vir7': [i for i,v in enumerate(check) if v[:2] == '94' or v[:2] == '98']}

# function to extract sites from a chromosome
def process(LG, queue):
    cur = 0
    sites = {k: [] for k in samples}
    with open(os.path.join(PATH, 'filter-sites', LG + '.txt')) as f:
        h = f.readline().split()
        assert h == ['pos', 'type'] + check
        for line in f:
            pos, cat, *site = line.split()
            pos = int(pos)
            p = round(pos / 1e6, 1)
            if p != cur:
                print('importing {0} {1:.1f}Mbp'.format(LG, p))
                cur = p
            if cat == 'V':
                for k, sam in samples.items():
                    subsite = ''.join([site[i] for i in sam])
                    alls = set(subsite)
                    alls.discard('?')
                    if len(alls) > 1:
                        a, b = alls
                        p = subsite.count(a)
                        q = subsite.count(b)
                        sites[k].append('{0} {1} {2} {3} 1\n'.format(LG, pos, q, p+q))
    queue.put({LG: sites})

# extract sites (parallel)
LGs = ['LG{0:0>2}'.format(LGi+1) for LGi in range(18)]
data = {}
q = multiprocessing.Queue()
queue = queue.Queue()
for LG in LGs:
    queue.put(multiprocessing.Process(target=process, args=(LG, q)))
for i in range(NUM_THREADS):
    if not queue.empty():
        queue.get().start()
for i in range(18):
    data.update(q.get())
    if not queue.empty():
        queue.get().start()

# function to export sites
def export(sam, LG, sites, q):
    print('exporting', sam, LG)
    with open(os.path.join(PATH, 'freq-hmm', '{0}-{1}.in'.format(sam, LG)), 'w') as f:
        for site in sites:
            f.write(site)
    q.put(None)

n = 0
for sam in samples:
    for LG in LGs:
        queue.put(multiprocessing.Process(target=export, args=[sam, LG, data[LG][sam], q]))
        n += 1
for i in range(NUM_THREADS):
    if not queue.empty():
        queue.get().start()
for i in range(n):
    q.get()
    if not queue.empty():
        queue.get().start()
print('all done')
