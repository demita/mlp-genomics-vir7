"""
Compute distance between populations using both Jost's D and FstWC.

PATH: work directory.
"""

import egglib, sys
sys.path.append('../../data')
import lib

PATH = '/home/user/WORK/pop-genomics-work'

Dj = [[], [], [], [], [], []]
WC = [[], [], [], [], [], []]

cs = egglib.stats.ComputeStats()
cs.add_stats('Dj', 'FstWC')

for LGi in range(18):
    LG = 'LG{0:0>2}'.format(LGi + 1)
    with open(PATH + '/filter-sites/{0}.txt'.format(LG)) as f:
        f.readline() #header
        c = 0
        for line in f:
            pos, cat, *site = line.split()
            if cat == 'F': continue
            pos = int(pos) - 1
            site = egglib.site_from_list(''.join(site), alphabet=egglib.alphabets.DNA)

            for idx, k in enumerate(['1993v1994', '1993v1998', '1993v2008',
                                     '1994v1998', '1994v2008', '1998v2008']):
                cs.configure(struct=lib.dict_structs[k])
                stats = cs.process_site(site)
                Dj[idx].append(stats['Dj'])
                if stats['FstWC'] is not None: WC[idx].append(stats['FstWC'])
            if (c%1000 == 0):
                print(LG, '{0:.2f}Mbp (of {1:.2f})'.format((pos+1)/1e6, lib.LGs[LG]/1e6))
            c += 1

f = open('output.txt', 'w')
f.write('pair        Djost    Fst\n')
f.write('1993v1994  {0:.4f} {1:.4f}\n'.format(sum(Dj[0])/len(Dj[0]), sum(WC[0])/len(WC[0])))
f.write('1993v1998  {0:.4f} {1:.4f}\n'.format(sum(Dj[1])/len(Dj[1]), sum(WC[1])/len(WC[1])))
f.write('1993v2008  {0:.4f} {1:.4f}\n'.format(sum(Dj[2])/len(Dj[2]), sum(WC[2])/len(WC[2])))
f.write('1994v1998  {0:.4f} {1:.4f}\n'.format(sum(Dj[3])/len(Dj[3]), sum(WC[3])/len(WC[3])))
f.write('1994v2008  {0:.4f} {1:.4f}\n'.format(sum(Dj[4])/len(Dj[4]), sum(WC[4])/len(WC[4])))
f.write('1998v2008  {0:.4f} {1:.4f}\n'.format(sum(Dj[5])/len(Dj[5]), sum(WC[5])/len(WC[5])))
f.close()

# trees reconstructed with PHYLIP's neighbor with default setting
# representation by https://itol.embl.de/
