"""
Compute the tree based on the results of 1-cp-distance.py and using the
neighbor commands of the PHYLIP package.
"""

import subprocess, os

pops = '1993', '1994', '1998', '2008'

matrix = {p: {q: [0, 0] for q in pops} for p in pops}
with open('output.txt') as f:
    header = f.readline().split()
    assert header == ['pair', 'Djost', 'Fst']
    for line in f:
        pair, D, F = line.split()
        D = float(D)
        F = float(F)
        p, q = pair.split('v')
        matrix[p][q] = D, F
        matrix[q][p] = D, F

with open('infile-1', 'w') as f1, open('infile-2', 'w') as f2:
    f1.write(' 4\n')
    f2.write(' 4\n')
    for p in pops:
        f1.write(p.ljust(10))
        f2.write(p.ljust(10))
        for q in pops:
            f1.write('  {0:.6f}'.format(matrix[p][q][0]))
            f2.write('  {0:.6f}'.format(matrix[p][q][1]))
        f1.write('\n')
        f2.write('\n')

p = subprocess.Popen(['neighbor'], stdin=subprocess.PIPE)
p.communicate(b'infile-1\nY\n')
os.rename('outtree', 'tree-DJost.tre')
os.unlink('outfile')
os.unlink('infile-1')

p = subprocess.Popen(['neighbor'], stdin=subprocess.PIPE)
p.communicate(b'infile-2\nY\n')
os.rename('outtree', 'tree-FstWC.tre')
os.unlink('outfile')
os.unlink('infile-2')

# representation by https://itol.embl.de/
