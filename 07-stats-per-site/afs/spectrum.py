"""
Compute the allele frequency spectrum.

PATH: work directory
"""

import sys, egglib
sys.path.append('../../data')
import lib

PATH = '/home/user/WORK/pop-genomics-work'

with open(PATH+'/stats-sites/AFS0000.txt', 'w') as f0000:
    with open(PATH+'/stats-sites/AFS1993.txt', 'w') as f1993:
        with open(PATH+'/stats-sites/AFS1994.txt', 'w') as f1994:
            with open(PATH+'/stats-sites/AFS1998.txt', 'w') as f1998:
                with open(PATH+'/stats-sites/AFS2008.txt', 'w') as f2008:

                    ITER = [
                        (f0000, lib.dict_structs['0000']),
                        (f1993, lib.dict_structs['1993']),
                        (f1994, lib.dict_structs['1994']),
                        (f1998, lib.dict_structs['1998']),
                        (f2008, lib.dict_structs['2008'])]

                    freq = egglib.stats.Freq()

                    for LGi in range(18):
                        LG = 'LG{0:0>2}'.format(LGi+1)
                        print(LG)
                        with open(PATH + '/filter-sites/{0}.txt'.format(LG)) as f:
                            f.readline() # header
                            for line in f:
                                pos, cat, *site = line.split()
                                if cat == 'F': continue
                                pos = int(pos)-1
                                site = egglib.site_from_list(''.join(site), alphabet=egglib.alphabets.DNA)

                                for f, struct in ITER:
                                    freq.from_site(site, struct=struct)
                                    if freq.num_alleles != 2: continue
                                    MAF = float(min(freq.freq_allele(0), freq.freq_allele(1))) / freq.nseff()
                                    if MAF > 0:
                                        f.write(str(MAF) + '\n')

