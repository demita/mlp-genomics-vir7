"""
Plot the allele frequency spectrum.

PATH: work directory.
NCAT: number of discretization categories.
"""

from matplotlib import pyplot

PATH = '/home/user/WORK/pop-genomics-work'

NCAT = 20
mids = [0.25/NCAT + (0.5/NCAT)*i for i in range(NCAT)]

for c, lab in [('k', '0000'), ('b', '1993'), ('r', '1994'), ('m', '1998'), ('g', '2008')]:
    cat = [0] * NCAT
    with open(PATH + '/stats-sites/AFS{0}.txt'.format(lab)) as f:
        for line in f:
            p = float(line)
            if p == 0.5: cat[-1] += 1
            else: cat[int(p / 0.5 * NCAT)] += 1
    pyplot.plot(mids, cat, c+'-', label=lab)
pyplot.legend()
pyplot.savefig('AFS.png')
