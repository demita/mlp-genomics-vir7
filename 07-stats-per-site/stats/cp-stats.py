"""
Compute statistics per site along all chromosomes.  

PATH: work directory
NUM_THREADS: number of parallelization threads
"""

import egglib, sys, os, multiprocessing
sys.path.append('../../data')
import lib

PATH = os.path.expanduser('~/WORK1/pop-genomics-work')
NUM_THREADS = 40

def process(LGi, queue):
    cs = egglib.stats.ComputeStats()
    cs.add_stats('He', 'FstWC', 'FisctWC', 'Dj', 'Gste')

    LG = 'LG{0:0>2}'.format(LGi + 1)

    with open(PATH + '/filter-sites/{0}.txt'.format(LG)) as f:
        with open(PATH + '/stats-sites/{0}.txt'.format(LG), 'w') as outf:
            outf.write('pos He He_vir7 He_avr7 theta_viravr Dj_viravr Gst\'_viravr theta2\n')
            c = 0
            f.readline() # header
            for line in f:
                pos, cat, *site = line.split()
                if cat == 'F': continue
                pos = int(pos)-1
                site = egglib.site_from_list(''.join(site), alphabet=egglib.alphabets.DNA)

                outf.write(str(pos+1))

                cs.configure()
                res = cs.process_site(site)
                outf.write(' ' + str(res['He']))

                cs.configure(lib.dict_structs['vir7'])
                res = cs.process_site(site)
                outf.write(' ' + str(res['He']))

                cs.configure(lib.dict_structs['avr7'])
                res = cs.process_site(site)
                outf.write(' ' + str(res['He']))

                cs.configure(lib.dict_structs['vir7avr7'])
                res = cs.process_site(site)
                outf.write(' ' + str(res['FstWC']))
                outf.write(' ' + str(res['Dj']))
                outf.write(' ' + str(res['Gste']))

                cs.configure(lib.dict_structs['hier'])
                res = cs.process_site(site)
                outf.write(' ' + str(res['FisctWC'][2]) + '\n')

                c+=1
                if c%1000 == 0:
                    print('{0} {1:.2f}/{2:.2f}Mbp'.format(LG, (pos+1)/1e6, lib.LGs[LG]/1e6))

    queue.put(None)


queue = multiprocessing.Queue()
q = [multiprocessing.Process(target=process, args=[i, queue]) for i in range(18)]

for i in range(NUM_THREADS):
    if len(q):
        q.pop(0).start()

for i in range(18):
    queue.get()
    if len(q):
        q.pop(0).start()

