"""
Make Manhattan plots with per-site statistics. Also generate lists of
candidates based on theta_2.

PATH: work directory
"""

from matplotlib import pyplot, rcParams
import os, math

a, b = rcParams['figure.figsize']
rcParams['figure.figsize'] = 12*a, 3*b
rcParams['font.size'] *= 2.5

PATH = os.path.expanduser('~/disks/backup2/work/pop-genomics-work')

theta2_all = []
for LGi in range(18):
    LG = 'LG{0:0>2}'.format(LGi+1)
    print(LG)
    He_vir = [[], []]
    He_avr = [[], []]
    theta = [[], []]
    Dj = [[], []]
    Gste = [[], []]
    theta2 = [[], []]
    with open(PATH + '/stats-sites/{0}.txt'.format(LG)) as f:
        h = f.readline()
        for line in f:
            pos, Het, Hev, Hea, Fst, dj, gste, Fct = line.split()
            pos = int(pos)
            He_vir[0].append(pos/1e6)
            He_vir[1].append(float(Hev))
            He_avr[0].append(pos/1e6)
            He_avr[1].append(float(Hea))
            Dj[0].append(pos/1e6)
            Dj[1].append(float(dj))
            Gste[0].append(pos/1e6)
            Gste[1].append(float(gste))
            if Fst != 'None':
                theta[0].append(pos/1e6)
                theta[1].append(float(Fst))
            if Fct != 'None':
                theta2[0].append(pos/1e6)
                theta2[1].append(float(Fct))
                theta2_all.append((LG, pos, float(Fct)))

    pyplot.subplot(4,1,1)
    pyplot.plot(theta[0], theta[1], 'k', marker= 'o', markersize= 2, ls= 'None')
    pyplot.ylabel(r'$\theta$ (vir v. avr)')

    pyplot.subplot(4,1,2)
    pyplot.plot(theta2[0], theta2[1], 'k', marker= 'o', markersize= 2, ls= 'None')
    pyplot.ylabel(r'$\theta_2$')

    pyplot.subplot(4,1,3)
    pyplot.plot(Dj[0], Dj[1], 'k', marker= 'o', markersize= 2, ls= 'None')
    pyplot.ylabel(r'Jost\'s $D$')

    pyplot.subplot(4,1,4)
    pyplot.plot(Gste[0], Gste[1], 'k',marker= 'o', markersize= 2, ls= 'None')
    pyplot.ylabel('${G}_{ST}\'$')

    pyplot.xlabel('Position (Mbp)')
    pyplot.savefig('plots/{0}.png'.format(LG))
    pyplot.clf()

# save top Fct values
theta2_all.sort(key=lambda x: -x[2])
for lvl in 2,3,4,5:
    c = int(10**-lvl * len(theta2_all))
    candidates = theta2_all[:c]
    ref = c-1
    while abs(theta2_all[c][2] - candidates[ref][2]) < 1e-6:
        candidates.append(theta2_all[c])
        c += 1
    with open('candidates_lvl{0}.txt'.format(lvl), 'w') as f:
        f.write('expected: {0} candidates: {1}\n'.format(ref+1, len(candidates)))
        for (i,j,k) in candidates:
            f.write('{0} {1} {2}\n'.format(i, j, k))
