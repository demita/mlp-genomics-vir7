"""
Generate an additional file from GWAS results.
"""

import matplotlib, sys, re, math
from matplotlib import pyplot, gridspec
sys.path.append('../data')
import lib

matplotlib.rc('font', size=16)
fig = pyplot.figure(figsize=(60, 5))
grid = gridspec.GridSpec(nrows=1, ncols=5, figure=fig, wspace=1)
ax1 = fig.add_subplot(grid[0, -1])
ax2 = fig.add_subplot(grid[0, :-1])

# chromosome structure
acc = 0
offsets = {}
mids = []
LGs = sorted(lib.LGs)
for LG in LGs:
    offsets[LG] = acc
    mids.append(acc + lib.LGs[LG]/2)
    acc += lib.LGs[LG]
offsets['tot'] = acc

# import variance partitioning data
expl = []
h2 = []
unexpl = []
loci = []
candX = []
with open('GWAS_variance-partition.txt') as f:
    assert f.readline().rstrip() == '"expl_RSS"	"h2_RSS"	"unexpl_RSS"	"SNP"'
    for line in f:
        e, h, u, locus = line.split()
        expl.append(float(e))
        h2.append(float(h))
        unexpl.append(float(u))
        assert unexpl[-1] == 1
        if re.match('"-?NA"$', locus): locus = ''
        else:
            sign, LG, pos = re.match('"(\+|-)SNP_(\d{2})_(\d+)"$', locus).groups()
            locus = '{0}chr{1}:{2}'.format(sign, LG, pos)
            LG = 'LG'+LG
            pos = float(pos) + offsets[LG]
            if pos not in candX:
                    candX.append(pos)
            if locus[0] == '+': locus = '$+$ ' + locus[1:]
            elif locus[0] == '-': locus = '$-$ ' + locus[1:]
        loci.append(locus)

# plot variance partitioning
ax1.fill_between(x=range(len(loci)), y1=expl, y2=0, fc='b', ec='None', label='explained')
ax1.fill_between(x=range(len(loci)), y1=h2, y2=expl, fc='g', label='genetic')
ax1.fill_between(x=range(len(loci)), y1=1, y2=h2, fc='r', label='error')
for i in range(1, len(loci)-1): ax1.axvline(i, c='0.8', lw=1)
ax1.set_xlim(0, len(loci)-1)
ax1.set_ylim(0, 1)
ax1.set_xticks(range(len(loci)))
ax1.set_xticklabels(loci, rotation=90)
ax1.set_ylabel('Proportion of explained variance')
ax1.legend(bbox_to_anchor=(1,1), loc="upper left")

# import scan data
X = [[] for i in range(18)]
Y = [[] for i in range(18)]
candY = [None] * len(candX)
with open('GWAS_allPval_step1.txt') as f:
    assert f.readline().rstrip() == '"SNP"\t"pval"'
    for line in f:
        SNP, P = line.split()
        x, LG, pos = SNP.strip('"').split('_')
        assert x == 'SNP'
        LG = 'LG'+LG
        pos = int(pos) + offsets[LG]
        P = math.log10(1/float(P))
        if pos in candX:
            assert candY[candX.index(pos)] is None
            candY[candX.index(pos)] = P
        assert P>=0
        X[LGs.index(LG)].append(pos)
        Y[LGs.index(LG)].append(P)
assert None not in candY

# plot
cols = ['0.0', '0.5'] * 9
for x, y, c in zip(X, Y, cols):
    ax2.plot(x, y, marker='o', ms=1, ls='None', c=c)
ax2.set_xlim(1, offsets['tot'])
ax2.set_ylim(0, ax2.get_ylim()[1])
ax2.set_ylabel('$log_{10} \\left( 1/P \\right)$')

# final configuration
ax1.set_title('b', loc='left', fontdict={'fontsize': 30, 'fontweight': 'bold'})
ax2.set_title('a', loc='left', fontdict={'fontsize': 30, 'fontweight': 'bold'})
ax1.tick_params(axis='x', length=10, bottom=True)
ax2.tick_params(axis='x', bottom=False)
ax2.tick_params(axis='y', length=10, left=True, right=True)
ax2.set_xticks(mids)
ax2.set_xticklabels(['chr' + i[2:] for i in LGs])
for i in range(1, 18): ax2.axvline(offsets[LGs[i]], c='0.8', ls=':')

fig.savefig('Additional file 9.png', bbox_inches='tight')

