"""
Generate an additional file from GWAS results.
"""

import matplotlib, sys, re, math
from matplotlib import pyplot
sys.path.append('../data')
import lib

matplotlib.rc('font', size=16)
fig = pyplot.figure(figsize=(20, 5))
ax = fig.add_subplot()

# import variance partitioning data
expl = []
h2 = []
unexpl = []
loci = []
candX = []
with open('GWAS_variance-partition.txt') as f:
    assert f.readline().rstrip() == '"expl_RSS"	"h2_RSS"	"unexpl_RSS"	"SNP"'
    for line in f:
        e, h, u, locus = line.split()
        expl.append(float(e))
        h2.append(float(h))
        unexpl.append(float(u))
        assert unexpl[-1] == 1
        if re.match('"-?NA"$', locus): locus = ''
        else:
            sign, LG, pos = re.match('"(\+|-)SNP_(\d{2})_(\d+)"$', locus).groups()
            locus = '{0}chr{1}:{2}'.format(sign, LG, pos)
            LG = 'LG'+LG
            if locus[0] == '+': locus = '$+$ ' + locus[1:]
            elif locus[0] == '-': locus = '$-$ ' + locus[1:]
        loci.append(locus)

# plot variance partitioning
ax.fill_between(x=range(len(loci)), y1=expl, y2=0, fc='b', ec='None', label='explained')
ax.fill_between(x=range(len(loci)), y1=h2, y2=expl, fc='g', label='genetic')
ax.fill_between(x=range(len(loci)), y1=1, y2=h2, fc='r', label='error')
for i in range(1, len(loci)-1): ax.axvline(i, c='0.8', lw=1)
ax.set_xlim(0, len(loci)-1)
ax.set_ylim(0, 1)
ax.set_xticks(range(len(loci)))
ax.set_xticklabels(loci, rotation=90)
ax.set_ylabel('Proportion of explained variance')
ax.legend(bbox_to_anchor=(1,1), loc="upper left")

# final configuration
ax.tick_params(axis='x', length=10, bottom=True)

fig.savefig('Supplementary_figure_5.png', bbox_inches='tight')

