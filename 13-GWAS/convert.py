"""
Create the GWAS input file.

PATH: work directory.
"""

import os
PATH = os.path.expanduser('~/WORK/pop-genomics-work')

nt = 0
for LGi in range(18):
    LG = 'LG{0:0>2}'.format(LGi+1)
    ni = 0

    with open(PATH + '/filter-sites/{0}.txt'.format(LG)) as f:

        # read header
        pos, status, *samples = f.readline().split()
        assert pos == 'pos', pos
        assert status == 'type', status

        with open('input/{0}.txt'.format(LG), 'w') as outf:
            outf.write('Position\ttype\t{0}\tnum_alleles\talleles\n'.format('\t'.join(samples)))

            for line in f:
                pos, status, *site = line.split()
                if status == 'F': continue
                pos = int(pos) - 1

                site = ''.join(site)
                alls = set(site)
                alls.discard('?')
                assert len(alls) == 2
                Lalls = list(alls)
                alls = {v: str(i+1) for i, v in enumerate(Lalls)}
                alls['?'] = '.'

                site = '\t'.join(['{0}{1}'.format(alls[i], alls[j]) for (i,j) in zip(site[::2], site[1::2])])
                outf.write('{0}\tSNP\t{1}\t2\t{2[0]},{2[1]}\n'.format(pos+1, site, Lalls))
                ni += 1
            print(LG, ni)
            nt += ni
print('total', nt)
