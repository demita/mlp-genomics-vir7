"""
Importable module providing useful information:
- size of chromosomes
- sample list
- dictionary with population composition
- different egglib.Structure objects
"""

import os, egglib
path = os.path.dirname(__file__)

LGs = {}
with open(os.path.join(path, 'LGs.txt')) as f:
    for line in f:
        a, b = line.split()
        LGs[a] = int(b)

pops = {}
with open(os.path.join(path, 'pops.txt')) as f:
    for line in f:
        bits = line.split()
        assert bits[0][-1] == ':'
        pops[bits[0][:-1]] = bits[1:]

samples = []
for p in ['2008', '1993', '1994', '1998']:
    samples.extend(pops[p])

vir7 = pops['1994'] + pops['1998']
avr7 = pops['1993'] + pops['2008']
vir7.remove('98AB07')
avr7.append('98AB07')

structs_indiv = [
  egglib.struct_from_dict({None: {None: dict([(name, [i]) for i,name in enumerate(samples)])}}, None),
  egglib.struct_from_dict({None: {None: dict([(name, [samples.index(name)]) for name in pops['1993']])}}, None),
  egglib.struct_from_dict({None: {None: dict([(name, [samples.index(name)]) for name in pops['1994']])}}, None),
  egglib.struct_from_dict({None: {None: dict([(name, [samples.index(name)]) for name in pops['1998']])}}, None),
  egglib.struct_from_dict({None: {None: dict([(name, [samples.index(name)]) for name in pops['2008']])}}, None)]

dict_structs = {
    '0000': egglib.struct_from_dict({None: {None: dict([(name, [2*i, 2*i+1]) for i,name in enumerate(samples)])}}, None),
    '1993': egglib.struct_from_dict({None: {None: dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in pops['1993']])}}, None),
    '1994': egglib.struct_from_dict({None: {None: dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in pops['1994']])}}, None),
    '1998': egglib.struct_from_dict({None: {None: dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in pops['1998']])}}, None),
    '2008': egglib.struct_from_dict({None: {None: dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in pops['2008']])}}, None),
    'vir7': egglib.struct_from_dict({None: {None: dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in vir7])}}, None),
    'avr7': egglib.struct_from_dict({None: {None: dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in avr7])}}, None),
    'hier': egglib.struct_from_dict({
        'vir7': {
            '1994': dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in pops['1994']]),
            '1998': dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in pops['1998']])},
        'avr7': {
            '1993': dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in pops['1993']]),
            '2008': dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in pops['2008']])}}, None),
    '4pop': egglib.struct_from_dict({None: {
            '1993': dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in pops['1993']]),
            '1994': dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in pops['1994']]),
            '1998': dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in pops['1998']]),
            '2008': dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in pops['2008']])}}, None),
    'vir7avr7': egglib.struct_from_dict({None: {
            'vir7': dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in vir7]),
            'avr7': dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in avr7])}}, None),
    '1993v1994': egglib.struct_from_dict({None: {
            '1993': dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in pops['1993']]),
            '1994': dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in pops['1994']])}}, None),
    '1993v1998': egglib.struct_from_dict({None: {
            '1993': dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in pops['1993']]),
            '1998': dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in pops['1998']])}}, None),
    '1993v2008': egglib.struct_from_dict({None: {
            '1993': dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in pops['1993']]),
            '2008': dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in pops['2008']])}}, None),
    '1994v1998': egglib.struct_from_dict({None: {
            '1994': dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in pops['1994']]),
            '1998': dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in pops['1998']])}}, None),
    '1994v2008': egglib.struct_from_dict({None: {
            '1994': dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in pops['1994']]),
            '2008': dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in pops['2008']])}}, None),
    '1998v2008': egglib.struct_from_dict({None: {
            '1998': dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in pops['1998']]),
            '2008': dict([(name, [2*samples.index(name), 2*samples.index(name)+1]) for name in pops['2008']])}}, None)}
