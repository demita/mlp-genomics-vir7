import egglib, multiprocessing, sys, random
sys.path.append('../data')
import lib

samples = ['1993', '1994', '1998', '2008']
stats = ['S', 'thetaW', 'Pi', 'Fis', 'D', 'D*', 'F*']
num_threads = 40
nrep = 100000
seed = egglib.random.get_seed()

# get number of samples per pop
NI = [len(lib.pops[k]) for k in samples]
NI.append(sum(NI))

# import data table
data = []
with open('stats.txt') as f:
    h = f.readline().split()
    for line in f:
        line = line.split()
        assert len(line) == len(h)
        d = {}
        for k, v in zip(h, line):
            if k not in ['chr', 'first', 'last']:
                if v == 'None':
                    v = None
                else:
                    try: v = int(v)
                    except ValueError: v = float(v)
                d[k] = v
        data.append(d)
total = data.pop(-1)

# get necessary data
print('number of windows:', len(data))
windows = []
for d in data:
    ni = [round(d[f'nseff_{k}']/2) for k in samples]
    assert min(ni) > 10
    theta = sum(d[f'thetaW_{k}'] for k in samples)/4
    ni.append(sum(ni))
    windows.append((theta, ni))

# simulation function
def simulate(queue):
    cs_list = []
    for i in range(5):
        cs = egglib.stats.ComputeStats(multi=True, struct=egglib.struct_from_samplesizes([NI[i]], ploidy=2))
        cs.add_stats(*stats)
        cs_list.append(cs)
    sim = egglib.coalesce.Simulator(num_pop=1)
    aln = egglib.Align(egglib.alphabets.DNA)
    for theta, ni in windows:
        for pop_idx in range(5):
            sim.params['num_indiv'][0] = NI[pop_idx]
            sim.params['theta'] = theta
            sim.simul(dest=aln)

            # apply randomly missing data
            if aln.ls > 0:
                for i in range(ni[pop_idx], NI[pop_idx]):
                    while True:
                        idx = random.randrange(0, NI[pop_idx])
                        if aln[2*idx][0] != 'N': break
                    aln[2*idx].sequence = 'N' * aln.ls
                    aln[2*idx+1].sequence = 'N' * aln.ls

            # compute
            cs_list[pop_idx].process_align(aln, max_missing=1)
    queue.put([cs.results() for cs in cs_list])

# configure processes
q = multiprocessing.Queue()
queue = [multiprocessing.Process(target=simulate, args=(q,)) for i in range(nrep)]
for i in range(num_threads):
    egglib.random.set_seed(seed+1)
    seed += 1
    queue.pop().start()

# run
with open('simuls.txt', 'w') as outf:
    outf.write(' '.join([f'{stat}_{sample}' for sample in samples+['total'] for stat in stats]) + '\n')
    for i in range(nrep):
        outf.write(' '.join(map(str, [results[stat] for results in q.get() for stat in stats])) + '\n')
        print(i+1, 'of', nrep)
        if len(queue):
            egglib.random.set_seed(seed+1)
            seed += 1
            queue.pop().start()
