import math

# get observed stats
with open('stats.txt') as f:
    h = f.readline().split()
    for line in f:
        continue
    line = line.split()
    assert len(line) == len(h)
    d = {}
    for k, v in zip(h, line):
        if k not in ['chr', 'first', 'last']:
            if v == 'None':
                v = None
            else:
                try: v = int(v)
                except ValueError: v = float(v)
            d[k] = v
obs = d
for sam in ['1993',  '1994', '1998', '2008', 'total']:
    for stat in ['thetaW', 'Pi']:
        obs[f'{stat}_{sam}'] = obs[f'{stat}_{sam}']/obs[f'lseff_{sam}']

# get stat differences
pairs = [('1993', '1994'), ('1993', '1998'), ('1993', '2008'),
         ('1994', '1998'), ('1994', '2008'), ('1998', '2008')]
stats = ['thetaW', 'Pi', 'D', 'D*', 'F*']
diffs = {}
for stat in stats:
    for a, b in pairs:
        diffs[(stat, a, b)] = obs[f'{stat}_{b}']-obs[f'{stat}_{a}']
diff_sims = {k: [] for k in diffs}

# import simuls
with open('simuls.txt') as f:
    h = f.readline().split()
    simuls = {i: [] for i in h}
    for line in f:
        line = line.split()
        assert len(line) == len(h)
        simul = {}
        for k, v in zip(h, line):
            if v != 'None':
                try: v = int(v)
                except ValueError: v = float(v)
                simul[k] = v
            else:
                simul[k] = None
        for sam in ['1993',  '1994', '1998', '2008', 'total']:
            for stat in ['thetaW', 'Pi']:
                simul[f'{stat}_{sam}'] = simul[f'{stat}_{sam}']/obs[f'lseff_{sam}']
        for k, v in simul.items():
            if v is not None:
                simuls[k].append(v)
        for stat in stats:
            for a, b in pairs:
                v1 = simul[f'{stat}_{a}']
                v2 = simul[f'{stat}_{b}']
                if a is not None and b is not None:
                    diff_sims[(stat, a, b)].append(v2-v1)
        

# compute p values
with open('p-values.txt', 'w') as outf:
    for stat in ['S', 'thetaW', 'Pi', 'Fis', 'D', 'D*', 'F*']:
        outf.write(stat)
        for sample in ['1993', '1994', '1998', '2008']:
            key = f'{stat}_{sample}'
            print(key)
            simuls[key].sort()
            p_left = 0
            p_right = 0
            i = 0
            while i < len(simuls[key]) and simuls[key][i] <= obs[key]:
                p_left += 1
                i += 1
            i = len(simuls[key])-1
            while i >= 0 and simuls[key][i] >= obs[key]:
                p_right += 1
                i -= 1
            p_left /= len(simuls[key])
            p_right /= len(simuls[key])
            mini = simuls[key][0]
            maxi = simuls[key][-1]
            avg = sum(simuls[key]) / len(simuls[key])
            avg2 = sum([i**2 for i in simuls[key]]) / len(simuls[key])
            sd = math.sqrt(avg2 - avg**2)
            diff = (obs[key]-avg)/sd
            sign = ''
            if p_left < 0.0001: sign = '***'
            elif p_left < 0.001: sign = '**'
            elif p_left < 0.01: sign = '*'
            elif p_right < 0.0001: sign = '***'
            elif p_right < 0.001: sign = '**'
            elif p_right < 0.01: sign = '*'
            outf.write(f' {obs[key]:.5f} {diff:.2f} {sign}')
        outf.write('\n')
    outf.write('\n')

    for stat in ['thetaW', 'Pi', 'D', 'D*', 'F*']:
        outf.write(f'{stat}\n')
        samples = ['1993', '1994', '1998', '2008']
        for a in range(3):
            sam1 = samples[a]
            for b in range(a+1, 4):
                sam2 = samples[b]
                key = (stat, sam1, sam2)
                print(key)
                diff_sims[key].sort()
                p_left = 0
                p_right = 0
                i = 0
                while i < len(diff_sims[key]) and diff_sims[key][i] <= diffs[key]:
                    p_left += 1
                    i += 1
                i = len(diff_sims[key])-1
                while i >= 0 and diff_sims[key][i] >= diffs[key]:
                    p_right += 1
                    i -= 1
                p_left /= len(diff_sims[key])
                p_right /= len(diff_sims[key])
                mini = diff_sims[key][0]
                maxi = diff_sims[key][-1]
                avg = sum(diff_sims[key]) / len(diff_sims[key])
                avg2 = sum([i**2 for i in diff_sims[key]]) / len(diff_sims[key])
                sd = math.sqrt(avg2 - avg**2)
                diff = (diffs[key]-avg)/sd
                sign = ''
                if p_left < 0.0001: sign = '***'
                elif p_left < 0.001: sign = '**'
                elif p_left < 0.01: sign = '*'
                elif p_right < 0.0001: sign = '***'
                elif p_right < 0.001: sign = '**'
                elif p_right < 0.01: sign = '*'
                outf.write(f'    {diff:.2f} {sign}')
            outf.write('\n')
