import egglib, sys, os
sys.path.append('../data')
import lib

NLG = 18 # number of LGs
WSIZE = 5000
WSTEP = 100000
PATH = os.path.expanduser('~/WORK1/pop-genomics-work/filter-sites/')
samples = ['1993', '1994', '1998', '2008', 'total']
MAXMISSING = 0.2
totn = 0

with open('stats.txt', 'w') as outf:
    outf.write('chr first last ls')
    for k in samples:
        outf.write(' S_{0} thetaW_{0} Pi_{0} D_{0} D*_{0} F*_{0} Fis_{0} nseff_{0} lseff_{0}'.format(k))
    outf.write('\n')

    # prepare statistics objects
    cs1 = {}
    cs2 = {}
    for k in samples:
        if k == 'total':
            cs1['total'] = egglib.stats.ComputeStats(lib.dict_structs['4pop'])
            cs1['total'].add_stats('S', 'thetaW', 'lseff', 'Pi', 'D', 'D*', 'F*', 'Fis', 'nseff', 'lseff')
            cs2['total'] = egglib.stats.ComputeStats(lib.dict_structs['4pop'])
            cs2['total'].add_stats('S', 'thetaW', 'lseff', 'Pi', 'D', 'D*', 'F*', 'Fis', 'nseff', 'lseff')
        else:
            cs1[k] = egglib.stats.ComputeStats(struct=lib.dict_structs[k])
            cs1[k].add_stats('S', 'thetaW', 'lseff', 'Pi', 'D', 'D*', 'F*', 'Fis', 'nseff', 'lseff')
            cs2[k] = egglib.stats.ComputeStats(struct=lib.dict_structs[k])
            cs2[k].add_stats('S', 'thetaW', 'lseff', 'Pi', 'D', 'D*', 'F*', 'Fis', 'nseff', 'lseff')

    for LGi in range(NLG):
        LG = 'LG{0:0>2}'.format(LGi+1)

        # define window bounds (stolen code from stats-per-window)
        ls = lib.LGs[LG] # length of chrososome
        starts = [] # create list of start of windows
        pos = 0
        while True:
            if pos+WSIZE > ls: break # don't include truncated window
            starts.append(pos)
            pos += WSTEP
        leftover = ls-starts[-1]+WSIZE # unused data at the end of chromosome
        offset = leftover//2
        bounds = [[i+offset, i+offset+WSIZE] for i in starts]

        print('{0}: 00/{1:0>2}'.format(LG, len(bounds)), end='', flush=True)

        # iterate over windows
        with open(PATH + LG + '.txt') as f:
            header = f.readline()
            pos, typ, *site = f.readline().split()
            pos = int(pos) - 1
            for i, (start, stop) in enumerate(bounds):
                n = 0
                while True:

                    # load sites
                    if pos is None or pos >= stop: break
                    if pos >= start:
                        site = egglib.site_from_list(''.join(site), alphabet=egglib.alphabets.DNA)
                        if site.num_missing < 10:
                            n+=1
                            for k in samples:
                                cs1[k].process_site(site)
                                cs2[k].process_site(site)
                    line = f.readline()
                    if line == '': pos = None
                    else:
                        pos, typ, *site = line.split()
                        pos = int(pos)-1

                print('\b\b\b\b\b{0:0>2}/{1:0>2}'.format(i, len(bounds)), end='', flush=True)

                if n > 999:
                    outf.write(f'{LG} {start+1} {stop} {n}')
                    totn += n

                    # compute stats
                    for k in samples:
                        stats = cs1[k].results()
                        outf.write(f' {stats["S"]} {stats["thetaW"]} {stats["Pi"]} {stats["D"]} {stats["D*"]} {stats["F*"]} {stats["Fis"]} {stats["nseff"]} {stats["lseff"]}')
                    outf.write('\n')
                    outf.flush()
            print('\b\b\b\b\b\b\b     ')

    # global stats
    outf.write(f'all - - {totn}')
    for k in samples:
        stats = cs2[k].results()
        outf.write(f' {stats["S"]} {stats["thetaW"]} {stats["Pi"]} {stats["D"]} {stats["D*"]} {stats["F*"]} {stats["Fis"]} {stats["nseff"]} {stats["lseff"]}')
    outf.write('\n')
