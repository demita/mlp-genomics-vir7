"""
Make a figure combining stats per window and result of genome scan test.
This script pick up results from other sections and generates the list
of windows with highest Fct (WC's theta2).

With an argument, this script generates the figure for a given
chromosome.

PATH: work directory
WSIZE/WSTEP: which sliding window analysis to use
K: which parameter value of the freq-hmm analysis to use
EHH_LVL: detection level for EHH
BAYPASS_THR: significance threshold for BayPass
"""

threshold = 30

import os, sys, math, matplotlib, re
from matplotlib import pyplot
sys.path.append('../data')
import lib

# interface
if len(sys.argv) == 1: focusLG = None
elif len(sys.argv) == 2:
    focusLG = sys.argv[1]
    assert focusLG[:2] == 'LG'
    assert int(focusLG[2:]) in list(range(1, 19))
else: raise ValueError

# parameters
PATH = os.path.expanduser('~/WORK1/pop-genomics-work')
WSIZE = 1000
WSTEP = 250
K = '1e-20'
EHH_LVL = 4
BAYPASS_THR = '1e-6'
GWAS_THR = 6

# variables
LGs = ['LG{0:0>2}'.format(i+1) for i in range(18)]
mids = []
offsets = []
acc = 0
for LG in LGs:
    offsets.append(acc)
    mids.append(acc+lib.LGs[LG]/2)
    acc += lib.LGs[LG]
END = sum(lib.LGs.values()) # last position in genome

# initiate picture
matplotlib.rc('font', size=20)
bigmarker = 5
biggermarker = 10
ticklen = 10
fig, axes = pyplot.subplots(8, 1, figsize=(40, 30))

# function to plot a series
def plot(idx, X, Y, lbl, Xc=None, Yc=None, XYc2=None):
    if Xc is None:
        assert Yc is None
        Xc = [[] for i in X]
        Yc = [[] for i in Y]
    else:
        assert Yc is not None
    assert len(X) == len(Y) == len(Xc) == len(Yc)
    for lg, (x, y, xc, yc) in enumerate(zip(X, Y, Xc, Yc)):
        assert len(x) == len(y) and len(xc) == len(yc)
        col = ['0.0', '0.5'][lg%2] if focusLG is None else 'k'
        axes[idx].plot([v+offsets[lg] for v in x], y, marker='o', ms=1, ls='None', c=col)
        axes[idx].plot([v+offsets[lg] for v in xc], yc, marker='o', ms=bigmarker, ls='None', c='r')
        for i in xc:
            axes[idx].axvline(i+offsets[lg], c=[0.2,0.6,1], ls='-', zorder=-1)
        if XYc2 is not None:
            axes[idx].plot([v+offsets[lg] for v in XYc2[0][lg]], XYc2[1][lg], marker='*', ms=biggermarker, ls='None', c='r')
            for i in XYc2[0][lg]:
                axes[idx].axvline(i+offsets[lg], c=[0.2,0.6,1], ls='-', zorder=-1)
    axes[idx].set_ylabel(lbl)

# function to place significant values as ticks
twin_axes = []
def add_ticks(idx, array):
    ax = axes[idx].twiny()
    ax.set_xlim(0, END)
    ax.set_xticks(array)
    ax.set_xticklabels([])
    ax.tick_params(axis='x', direction='in', length=ticklen, color='r')
    twin_axes.append(ax)

#### IMPORT SUMMARY STATISTICS ####

# get depths (only of filtered sites)
depth = ([[] for i in lib.LGs], [[] for i in lib.LGs])
print('processing depth: LG00', end='')
for idx, LG in enumerate(LGs):
    print(f'\b\b\b\b{LG}', end='', flush=True)
    dpt = {}
    with open(os.path.join(PATH, 'depths', f'{LG}.txt')) as f:
        for line in f:
            pos, d = line.split()
            pos = int(pos) - 1
            d = float(d)
            assert pos not in dpt
            dpt[pos] = d

    with open(os.path.join(PATH, 'filter-sites', f'{LG}.txt')) as f:
        f.readline()
        for line in f:
            pos = line.split()[0]
            pos = int(pos) - 1
            depth[0][idx].append(pos)
            depth[1][idx].append(dpt[pos])
print('\b\b\b\bdone')

# get stats
Pi = ([[] for i in lib.LGs], [[] for i in lib.LGs], [[] for i in lib.LGs])
print('processing total: LG00', end='')
for idx, LG in enumerate(LGs):
    print(f'\b\b\b\b{LG}', end='', flush=True)
    with open(os.path.join(PATH, f'stats-windows-wsize{WSIZE}-wstep{WSTEP}', f'{LG}-total.txt')) as f:
        header = f.readline().split()
        for line in f:
            line = line.split()
            assert len(line) == len(header)
            d = dict(zip(header, line))

            bounds = int(d['start']), int(d['stop'])

            v = d['Pi']
            if v != 'None':
                Pi[0][idx].append(bounds)
                Pi[1][idx].append((bounds[0]+bounds[1])/2)
                Pi[2][idx].append(float(v))
print('\b\b\b\bdone')

Dxy = ([[] for i in lib.LGs], [[] for i in lib.LGs], [[] for i in lib.LGs])
Da = ([[] for i in lib.LGs], [[] for i in lib.LGs], [[] for i in lib.LGs])
print('processing vir7avr7: LG00', end='')
for idx, LG in enumerate(LGs):
    print(f'\b\b\b\b{LG}', end='', flush=True)
    with open(os.path.join(PATH, f'stats-windows-wsize{WSIZE}-wstep{WSTEP}', f'{LG}-vir7avr7.txt')) as f:
        header = f.readline().split()
        for line in f:
            line = line.split()
            assert len(line) == len(header)
            d = dict(zip(header, line))

            bounds = int(d['start']), int(d['stop'])

            v = d['Dxy']
            if v != 'None':
                Dxy[0][idx].append(bounds)
                Dxy[1][idx].append((bounds[0]+bounds[1])/2)
                Dxy[2][idx].append(float(v))
            v = d['Da']
            if v != 'None':
                Da[0][idx].append(bounds)
                Da[1][idx].append((bounds[0]+bounds[1])/2)
                Da[2][idx].append(float(v))
print('\b\b\b\bdone')

#TajD = {}
#for sample in ['vir7', 'avr7']:
#    print('processing {0}: LG00'.format(sample), end='')
#
#    res = ([[] for i in lib.LGs],
#           [[] for i in lib.LGs],
#           [[] for i in lib.LGs])
#
#    for idx, LG in enumerate(LGs):
#        print('\b\b\b\b{0}'.format(LG), end='', flush=True)
#        with open(os.path.join(PATH, 'stats-windows-wsize{0}-wstep{1}'.format(WSIZE, WSTEP), '{0}-{1}.txt'.format(LG, sample))) as f:
#            header = f.readline().split()
#            for line in f:
#                line = line.split()
#                assert len(line) == len(header)
#                d = dict(zip(header, line))
#
#                bounds = int(d['start']), int(d['stop'])
#
#                v = d['D']
#                if v != 'None':
#                    res[0][idx].append(bounds)
#                    res[1][idx].append((bounds[0]+bounds[1])/2)
#                    res[2][idx].append(float(v))
#
#    print('\b\b\b\bdone')
#    TajD[sample] = res

print('processing theta2: LG00', end='')
theta2 = ([[] for i in lib.LGs], [[] for i in lib.LGs])
for idx, LG in enumerate(LGs):
    print('\b\b\b\b{0}'.format(LG), end='', flush=True)
    with open(os.path.join(PATH, 'stats-sites/{0}.txt'.format(LG))) as f:
        h = f.readline().split()
        for line in f:
            line = line.split()
            assert len(line) == len(h)
            d = dict(zip(h, line))
            theta2[0][idx].append(int(d['pos']))
            theta2[1][idx].append(float(d['theta2']))
print('\b\b\b\bdone')

### plot stats
plot(0, depth[0], depth[1],  'Sequencing\ndepth')
plot(1, Pi[1], Pi[2],  '$\pi$')
#plot(, TajD['avr7'][1], TajD['avr7'][2],  'Tajima\'s $D$\navirulent')
#plot(, TajD['vir7'][1], TajD['vir7'][2], 'Tajima\'s $D$\nvirulent')
plot(3, Da[1], Da[2], '$D_a$')
plot(4, theta2[0], theta2[1], '$\hat{\\theta}_2$vir v. avr \n(per site)')
axes[4].set_ylim(axes[4].get_ylim()[0], 1)

### PLOT FREQ-HMM

print('processing freq-hmm: LG00', end='')

# get data
xP = []
yP = []
xPc = []
yPc = []
for idx, LG in enumerate(lib.LGs):
    xP.append([])
    yP.append([])
    xPc.append([])
    yPc.append([])
    print('\b\b\b\b' + LG, end='', flush=True)

    # load sweep locations
    sweep_pos = []
    with open(os.path.join(PATH, 'freq-hmm', '1998-{0}_{1}.stat'.format(LG, K))) as f:
        num = int(f.readline())
        cur = 0
        for i in range(num):
            p1, p2, P = f.readline().split()
            p1 = int(p1)
            p2 = int(p2)
            p = float(p)
            assert p1 > cur and p2 > p1
            cur = p2
            sweep_pos.append((p1, p2))
        assert f.readline() == ''

    # load probas
    with open(os.path.join(PATH, 'freq-hmm', '1998-{0}_{1}.post'.format(LG, K))) as f:
        for i, line in enumerate(f):
            pos, p = line.split()
            pos = int(pos)
            p = float(p)
            for a,b in sweep_pos:
                if pos >=a and pos <= b:
                    xPc[-1].append(pos)
                    yPc[-1].append(math.log10(p))
                    break
            else:
                xP[-1].append(pos)
                yP[-1].append(math.log10(p))

print('\b\b\b\bdone')

# plot
plot(2, xP, yP, 'freq-hmm\n$log_{10}(P)$', xPc, yPc)
axes[2].set_yticks([-80, -60, -40, -20, 0])


### PLOT XTX TEST FROM BAYPASS ###

# get marker positions
markers = []
with open(os.path.join('..', '11-baypass', 'logfile.txt')) as f:
    for line in f:
        idx, LG, pos = line.split()
        idx = int(idx)
        pos = int(pos)
        markers.append((LG, pos))
        assert idx == len(markers)

# get P-values
print('processing Baypass results...', end='', flush=True)
Pval = []
with open(os.path.join('..', '11-baypass', 'core', 'output_summary_pi_xtx.out')) as f:
    h = f.readline().split()
    idx = h.index('log10(1/pval)')
    for line in f:
        line = line.split()
        assert len(line) == len(h)
        Pval.append(float(line[idx]))
assert len(markers) == len(Pval)

# get significant markers
candX = [[] for i in range(18)]
candY = [[] for i in range(18)]
with open(os.path.join('..', '11-baypass', 'core', 'markers_{0}.txt'.format(BAYPASS_THR))) as f:
    for line in f:
        idx, LG, pos, P = line.split()
        idx = int(idx) - 1
        pos = int(pos)
        P = float(P)
        assert markers[idx] == (LG, pos)
        assert Pval[idx] == P
        candX[LGs.index(LG)].append(pos)
        candY[LGs.index(LG)].append(P)

# organize data and plot
xBayPass = [[] for i in LGs]
yBayPass = [[] for i in LGs]
for (LG, pos), P in zip(markers, Pval):
    LGi = LGs.index(LG)
    if pos not in candX[LGi]:
        xBayPass[LGi].append(pos)
        yBayPass[LGi].append(P)
plot(5, xBayPass, yBayPass, 'Baypass\n$log_{10}\\left( 1/P \\right)$', candX, candY)
axes[5].set_ylim(0, axes[4].get_ylim()[1])
axes[5].axhline(6, c='k', ls=':', zorder=6)
axes[5].set_ylim(axes[5].get_ylim()[0], 10)
print('\b\b\b: done')

### PLOT GWAS ###
print('processing GWAS...', end='', flush=True)
xGWAS = [[] for i in LGs]
yGWAS = [[] for i in LGs]
xGWASc = [[] for i in LGs]
yGWASc = [[] for i in LGs]
xGWASc2 = [[] for i in LGs]
yGWASc2 = [[] for i in LGs]
GWASd = {}
n = 0
with open(os.path.join('..', '13-GWAS', 'GWAS_allPval_step0.txt')) as f:
    h = f.readline()
    for line in f:
        loc, P = line.split()
        LG, pos = re.match('"SNP_(\d{2})_(\d+)"', loc).groups()
        pos = int(pos)
        P = float(P)
        P = math.log10(1/P)
        LGi = LGs.index('LG' + LG)
        if P > GWAS_THR:
            xGWASc[LGi].append(pos)
            yGWASc[LGi].append(P)
            n += 1
        else:
            xGWAS[LGi].append(pos)
            yGWAS[LGi].append(P)
        GWASd[(LG, pos)] = P
with open(os.path.join('..', '13-GWAS', 'GWAS_allPval_step7.txt')) as f:
    h = f.readline()
    for i in range(7):
        line = f.readline()
        loc, P = line.split()
        LG, pos = re.match('"SNP_(\d{2})_(\d+)"', loc).groups()
        pos = int(pos)
        LGi = LGs.index('LG' + LG)
        xGWASc2[LGi].append(pos)
        yGWASc2[LGi].append(GWASd[(LG, pos)])

plot(6, xGWAS, yGWAS, 'GWAS\n$log_{10} \\left( 1/P \\right)$', xGWASc, yGWASc, [xGWASc2, yGWASc2])
axes[6].axhline(GWAS_THR, c='k', ls=':', zorder=6)
#axes[].set_ylim(axes[].get_ylim()[0], 80)
axes[6].set_ylim(0, axes[6].get_ylim()[1])

print('\b\b\b done', n)

### PLOT RSB FROM EHH ###

print('processing EHH: LG00', end='')
xRsb = [[] for i in range(18)]
yRsb = [[] for i in range(18)]
xRsbC = [[] for i in range(18)]
yRsbC = [[] for i in range(18)]

# get significant SNPs
with open(os.path.join('..', '12-EHH', 'candidates', 'Rsb1_lvl{0}.txt'.format(EHH_LVL))) as f:
    f.readline()
    for line in f:
        LG, pos, IEG, lnRsb = line.split()
        pos = int(pos)
        lnRsb = float(lnRsb)
        LGi = LGs.index(LG)
        xRsbC[LGi].append(pos)
        yRsbC[LGi].append(lnRsb)

# import Rsb values
with open(os.path.join('..', '12-EHH', 'data.txt')) as f:
    h = f.readline().split()
    for line in f:
        line = line.split()
        assert len(line) == len(h)
        d = dict(zip(h, line))
        if d['Rsb1'] != 'NA':
            LGi = LGs.index(d['LG'])
            pos = float(d['pos'])
            if pos not in xRsbC[LGi]:
                xRsb[LGi].append(pos)
                yRsb[LGi].append(float(d['Rsb1']))

# plot
plot(7, xRsb, yRsb, '$ln(Rsb)$\nVir1994 v. Avr1993', xRsbC, yRsbC)
print('\b\b\b\bdone')


### FINISH ####

# configure and save figure
print('saving graph...', end='', flush=True)
for ax in axes:
    ax.set_xlim(0, END)
    ax.set_xticks([])
    ax.tick_params(axis='y', length=ticklen, left=True, right=True)
    for i in range(1, 18): ax.axvline(offsets[i], c='0.8', ls=':')
for i in 0, -1:
    axes[i].set_xticks(mids)
    axes[i].set_xticklabels(['chr' + i[2:] for i in lib.LGs.keys()])
axes[0].tick_params(axis='x', labeltop=True, labelbottom=False, bottom=False)
axes[-1].tick_params(axis='x', bottom=False)

# crop figure
if focusLG is not None:
    idx = LGs.index(focusLG)
    for ax in axes:
        ax.set_xlim(offsets[idx], offsets[idx]+lib.LGs[focusLG]-1)
    for ax in twin_axes:
        ax.set_xlim(offsets[idx], offsets[idx]+lib.LGs[focusLG]-1)
    points = range(0, int(math.ceil(lib.LGs[focusLG]/5e5)))
    axes[-1].set_xticks([offsets[idx]+p*5e5 for p in points])
    axes[-1].set_xticklabels(['{0:.1f}Mbp'.format(p/2) for p in points])
    axes[-1].tick_params(axis='x', bottom=True, length=ticklen)

# savefig
if focusLG is None: fig.savefig('Fig2.png', bbox_inches='tight')
else: fig.savefig(focusLG + '.png', bbox_inches='tight')
print('\b\b\b done')
