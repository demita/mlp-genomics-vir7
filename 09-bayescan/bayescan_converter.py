"""
Prepare the input file for bayescan (input.txt) and write down the list
of loci in logfile.txt.

PATH: work directory.
"""

import re
import sys
import egglib
sys.path.append('../data')
import lib

PATH = '/home/user/WORK/pop-genomics-work'
logfile = open('logfile.txt', 'w')


pop1 = []
pop2 = []
pop3 = []
pop4 = []
locus_index=0

for LGi in range(18):
    LG = 'LG{0:0>2}'.format(LGi+1)
    print(LG)
    with open(PATH + '/filter-sites/{0}.txt'.format(LG)) as f:
        f.readline()
        for line in f:            
            pos, cat, *site = line.split()
            if (int(pos)-1) % 1000000 == 0:
                print('{0} {1:.2f}Mbp'.format(LG, (int(pos)-1)/1e6))
            if cat == 'F': continue
            site = egglib.site_from_list(''.join(site), alphabet=egglib.alphabets.DNA)
            frq = egglib.stats.freq_from_site(site, lib.dict_structs['4pop'])
            if frq.num_alleles < 2: continue
            locus_index += 1
            pop1.append((locus_index , frq.nseff(frq.population, 0) , 2 ,frq.freq_allele(0, frq.population, 0) , frq.freq_allele(1, frq.population, 0)))
            pop2.append((locus_index , frq.nseff(frq.population, 1) , 2 , frq.freq_allele(0, frq.population, 1) , frq.freq_allele(1, frq.population, 1)))
            pop3.append((locus_index , frq.nseff(frq.population, 2) , 2, frq.freq_allele(0, frq.population, 2) , frq.freq_allele(1, frq.population, 2)))
            pop4.append((locus_index , frq.nseff(frq.population, 3) , 2, frq.freq_allele(0, frq.population, 3) , frq.freq_allele(1, frq.population, 3)))
            logfile.write('{0}\t{1}\t{2}\n'.format(locus_index, LG, pos))

print((len(pop1)), len(pop2), len(pop3), len(pop4))



with open('input.txt', 'w') as out:
    out.write('[loci]={0}\n\n[populations]=4\n'.format(locus_index))
    out.write('\n[pop]=1\n')
    for i in range(locus_index):
        out.write('{0}\n'.format("\t".join(map(str, pop1[i]))))
    out.write('\n[pop]=2\n')
    for i in range(locus_index):
        out.write('{0}\n'.format("\t".join(map(str, pop2[i]))))
    out.write('\n[pop]=3\n')
    for i in range(locus_index):
        out.write('{0}\n'.format("\t".join(map(str, pop3[i]))))
    out.write('\n[pop]=4\n')
    for i in range(locus_index):
        out.write('{0}\n'.format("\t".join(map(str, pop4[i]))))
        
