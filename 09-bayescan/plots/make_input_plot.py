"""
generate inputfile for manhatanplot
"""

out1=open('LG01.txt', 'w')
out2=open('LG02.txt', 'w')
out3=open('LG03.txt', 'w')
out4=open('LG04.txt', 'w')
out5=open('LG05.txt', 'w')
out6=open('LG06.txt', 'w')
out7=open('LG07.txt', 'w')
out8=open('LG08.txt', 'w')
out9=open('LG09.txt', 'w')
out10=open('LG10.txt', 'w')
out11=open('LG11.txt', 'w')
out12=open('LG12.txt', 'w')
out13=open('LG13.txt', 'w')
out14=open('LG14.txt', 'w')
out15=open('LG15.txt', 'w')
out16=open('LG16.txt', 'w')
out17=open('LG17.txt', 'w')
out18=open('LG18.txt', 'w')

pos_abs=[]
LG = []
pos_real = []
proba = []
FST=[]



with open('../output_fst.txt') as f:
    f.readline()
    for line in f:
        MRK, prob, log10, qval, alpha, fst = line.split()
        proba.append(prob)
        FST.append(fst)
        
with open('../logfile.txt') as f2:
    for lines in f2:
        pos_ab, Lg, pos_rea = lines.split()
        pos_abs.append(pos_ab)
        LG.append(Lg)    
        pos_real.append(pos_rea)
        
for i in range(len(pos_abs)):
    if LG[i] == 'LG01':
        out1.write('{0}\t{1}\t{2}\t{3}\n'.format(pos_abs[i], pos_real[i], proba[i], FST[i]))
    if LG[i] == 'LG02':
        out2.write('{0}\t{1}\t{2}\t{3}\n'.format(pos_abs[i], pos_real[i], proba[i], FST[i]))
    if LG[i] == 'LG03':
        out3.write('{0}\t{1}\t{2}\t{3}\n'.format(pos_abs[i], pos_real[i], proba[i], FST[i]))
    if LG[i] == 'LG04':
        out4.write('{0}\t{1}\t{2}\t{3}\n'.format(pos_abs[i], pos_real[i], proba[i], FST[i]))
    if LG[i] == 'LG05':
        out5.write('{0}\t{1}\t{2}\t{3}\n'.format(pos_abs[i], pos_real[i], proba[i], FST[i]))
    if LG[i] == 'LG06':
        out6.write('{0}\t{1}\t{2}\t{3}\n'.format(pos_abs[i], pos_real[i], proba[i], FST[i]))
    if LG[i] == 'LG07':
        out7.write('{0}\t{1}\t{2}\t{3}\n'.format(pos_abs[i], pos_real[i], proba[i], FST[i]))
    if LG[i] == 'LG08':
        out8.write('{0}\t{1}\t{2}\t{3}\n'.format(pos_abs[i], pos_real[i], proba[i], FST[i]))
    if LG[i] == 'LG09':
        out9.write('{0}\t{1}\t{2}\t{3}\n'.format(pos_abs[i], pos_real[i], proba[i], FST[i]))
    if LG[i] == 'LG10':
        out10.write('{0}\t{1}\t{2}\t{3}\n'.format(pos_abs[i], pos_real[i], proba[i], FST[i]))
    if LG[i] == 'LG11':
        out11.write('{0}\t{1}\t{2}\t{3}\n'.format(pos_abs[i], pos_real[i], proba[i], FST[i]))
    if LG[i] == 'LG12':
        out12.write('{0}\t{1}\t{2}\t{3}\n'.format(pos_abs[i], pos_real[i], proba[i], FST[i]))
    if LG[i] == 'LG13':
        out13.write('{0}\t{1}\t{2}\t{3}\n'.format(pos_abs[i], pos_real[i], proba[i], FST[i]))
    if LG[i] == 'LG14':
        out14.write('{0}\t{1}\t{2}\t{3}\n'.format(pos_abs[i], pos_real[i], proba[i], FST[i]))
    if LG[i] == 'LG15':
        out15.write('{0}\t{1}\t{2}\t{3}\n'.format(pos_abs[i], pos_real[i], proba[i], FST[i]))
    if LG[i] == 'LG16':
        out16.write('{0}\t{1}\t{2}\t{3}\n'.format(pos_abs[i], pos_real[i], proba[i], FST[i]))
    if LG[i] == 'LG17':
        out17.write('{0}\t{1}\t{2}\t{3}\n'.format(pos_abs[i], pos_real[i], proba[i], FST[i]))
    if LG[i] == 'LG18':
        out18.write('{0}\t{1}\t{2}\t{3}\n'.format(pos_abs[i], pos_real[i], proba[i], FST[i]))
