"""
Plot the correlation of heterozygosity with depth. Generate plots in
the local directory.

THRESHOLDS: list of threshold values.
"""

import sys, re
from matplotlib import pyplot, rcParams

THRESHOLDS = 15, 30, 45

for thre in THRESHOLDS:

    NNM = 0
    NM = 0
    counts = {}
    for LG in range(18):
        with open('data/LG{0:0>2}-{1}.txt'.format(LG+1, thre)) as f:
            nnm, nm = map(int, f.readline().split())
            NNM += nnm
            NM += nm
            for line in f:
                dp, nm, m = map(int, line.split())
                if dp not in counts:
                    counts[dp] = [0, 0]
                counts[dp][0] += nm
                counts[dp][1] += m

    maxi = max(counts)
    X = range(maxi + 1)
    Y1 = [0] * (maxi + 1)
    Y2 = [0] * (maxi + 1)

    for i in counts:
        Y1[i] = counts[i][0] / NNM
        Y2[i] = counts[i][1] / NM

    a, b = rcParams['figure.figsize']
    rcParams['figure.figsize'] = 3*a, 2*b

    pyplot.subplot(2,3,1)
    pyplot.fill_between(X[:50], 0, Y1[:50], facecolor='blue', alpha=0.5)
    pyplot.fill_between(X[:50], 0, Y2[:50], facecolor='red', alpha=0.5)
    pyplot.xlabel('Depth')
    pyplot.ylabel('Number of genotypes (scaled)')
    pyplot.yticks([])
    pyplot.title('zoom left')

    pyplot.subplot(2,3,2)
    pyplot.fill_between(X, 0, Y1, facecolor='blue', alpha=0.5)
    pyplot.fill_between(X, 0, Y2, facecolor='red', alpha=0.5)
    pyplot.xlabel('Depth')
    pyplot.ylabel('Number of genotypes (scaled)')
    pyplot.yticks([])
    pyplot.title('all')

    pyplot.subplot(2,3,3)
    pyplot.fill_between(X[150:], 0, Y1[150:], facecolor='blue', alpha=0.5, label='non masked')
    pyplot.fill_between(X[150:], 0, Y2[150:], facecolor='red', alpha=0.5, label='masked')
    pyplot.xlabel('Depth')
    pyplot.ylabel('Number of genotypes (scaled)')
    pyplot.yticks([])
    pyplot.title('zoom right')
    pyplot.legend()

    pyplot.subplot(2,3,4)
    pyplot.fill_between(X[:5], 0, Y1[:5], facecolor='blue', alpha=0.5)
    pyplot.fill_between(X[:5], 0, Y2[:5], facecolor='red', alpha=0.5)
    pyplot.xlabel('Depth')
    pyplot.ylabel('Number of genotypes (scaled)')
    pyplot.yticks([])

    pyplot.subplot(2,3,6)
    pyplot.fill_between(X[250:], 0, Y1[250:], facecolor='blue', alpha=0.5, label='non masked')
    pyplot.fill_between(X[250:], 0, Y2[250:], facecolor='red', alpha=0.5, label='masked')
    pyplot.xlabel('Depth')
    pyplot.ylabel('Number of genotypes (scaled)')
    pyplot.yticks([])

    pyplot.savefig('threshold{0}.png'.format(thre))
    pyplot.clf()
