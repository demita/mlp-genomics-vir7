"""
Compute correlation of depth according to masked/non-masked region
Use the plotting script after this one is done. Output file are saved
in the directory data/.

PATH: work directory.
THRESHOLDS: list of threshold values (as available in raw-sites).
MAX_THREADS: number of parallelizing threads.
REFF: path to the fasta file containing the reference.
"""

import sys, egglib, multiprocessing

PATH = '/home/user/WORK/pop-genomics-work'
THRESHOLDS = 15, 30, 45
MAX_THREADS = 10
REFF = '/home/user/data/projects/rust_genome/Mellp2_3/Mellp2_3_AssemblyScaffolds_Repeatmasked.fasta'

alph = egglib.Alphabet('char', 'ACGTacgt', 'N', case_insensitive=False)
ref = egglib.io.from_fasta(REFF, alphabet=alph)

def process(LGi, THRESHOLD, queue):
    dico = {}
    nnonmasked = 0
    nmasked=  0

    LG = 'LG{0:0>2}'.format(LGi+1)
    with open(PATH + '/raw-sites/THRESHOLD{0}-SNP-{1}.txt'.format(THRESHOLD, LG)) as f:
        c = 0
        header = f.readline().split()
        assert header[0] == 'pos'
        for line in f:
            pos, site = line.split()
            pos = int(pos) - 1
            if ref.find('LG_{0:0>2}'.format(LGi+1)) .sequence[pos].islower():
                masked = True
                nmasked += 1
            else:
                masked = False
                nnonmasked += 1
            for geno in site.split(','):
                dp, ab = geno.split(':')
                dp = int(dp)
                if dp not in dico: dico[dp] = [0, 0]
                if masked: dico[dp][1] += 1
                else: dico[dp][0] += 1
            c += 1
            if c%100000 == 0:
                print('{0} - {1} - {2:.2f}Mbp'.format(THRESHOLD, LG, pos/1e6))

    # export data
    with open('data/{0}-{1}.txt'.format(LG, THRESHOLD), 'w') as f:
        f.write('{0} {1}\n'.format(nnonmasked, nmasked))
        for dp in sorted(dico):
            f.write('{0} {1[0]} {1[1]}\n'.format(dp, dico[dp]))

    queue.put(None)

# define list of tasks
queue = multiprocessing.Queue()
tasks = [[ch, thr, queue] for thr in THRESHOLDS for ch in range(18)]
num = len(tasks)

# setup list of processes
wait = [multiprocessing.Process(target=process, args=args) for args in tasks]

# start enough jobs
for i in range(MAX_THREADS):
    wait.pop(0).start()

# wait for jobs to complete
done = 0
while done < num:
    queue.get()
    done += 1
    if len(wait) > 0:
        wait.pop(0).start()
    print('{0} to go'.format(num-done))
