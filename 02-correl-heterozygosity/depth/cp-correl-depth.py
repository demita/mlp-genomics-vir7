"""
Correlation of number of heterozygote genotypes and depth
Based on file with raw SNPs (with max 2 alleles). Output files are saved
in the local directory data/

PATH: work directory.
THRESHOLDS: list of threshold values (as available in raw-sites).
MAX_THREADS: number of parallelizing threads.
"""

import sys, multiprocessing, math, time
sys.path.append('../../data')
import lib

PATH = '/home/user/WORK/pop-genomics-work'
THRESHOLDS = 15, 30, 45
MAX_THREADS = 10

# process a given LGs
def process(LG, THRESHOLD, q):

    # open the file with all sites
    LG = 'LG{0:0>2}'.format(LG+1)
    fname = PATH + '/raw-sites/THRESHOLD{0}-SNP-{1}.txt'.format(THRESHOLD, LG)
    ln = lib.LGs[LG]
    d = {}
    cur = 0
    with open(fname) as f:
        header = f.readline().split()
        assert header[0] == 'pos'
        for line in f:
            pos, indiv = line.split()
            indiv = indiv.split(',')
            pos = int(pos) - 1
            assert len(indiv) == 76
            line = []
            for i in indiv:
                dp, g = i.split(':')
                dp = int(dp)
                if g == '?': g = 0
                else:
                    g1, g2 = g.split('/')
                    if g1 == g2: g = 1
                    else: g = 2
                if dp not in d: d[dp] = [0, 0, 0]
                d[dp][g] += 1
            rpos = pos//100000
            if rpos != cur:
                cur = rpos
                print('{0} {1} {2:.2f}/{3:.2f}Mbp'.format(THRESHOLD, LG, pos/1000000, lib.LGs[LG]/1000000))

    # open output file
    with open('data/{0}-{1}.txt'.format(LG, THRESHOLD), 'w') as outf:
        for dp in sorted(d):
            outf.write('{0} {1[0]} {1[1]} {1[2]}\n'.format(dp, d[dp]))

    q.put(None)

# define list of tasks
queue = multiprocessing.Queue()
tasks = [[ch, thr, queue] for thr in THRESHOLDS for ch in range(18)]
num = len(tasks)

# setup list of processes
wait = [multiprocessing.Process(target=process, args=args) for args in tasks]

# start enough jobs
for i in range(MAX_THREADS):
    wait.pop(0).start()

# wait for jobs to complete
done = 0
while done < num:
    queue.get()
    done += 1
    if len(wait) > 0:
        wait.pop(0).start()
    print('{0} to go'.format(num-done))
