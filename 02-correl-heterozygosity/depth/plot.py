"""
Plot the correlation of heterozygosity with depth. Generate plots in
the local directory.
"""

from matplotlib import pyplot, rcParams
a, b = rcParams['figure.figsize']
rcParams['figure.figsize'] = 2*a, 2*b

for thre in 15, 30, 45:
    counts = {}
    for ch in range(18):
        with open('data/LG{0:0>2}-{1}.txt'.format(ch+1, thre)) as f:
            for line in f:
                dp, unc, fix, var = map(int, line.split())
                if dp not in counts:
                    counts[dp] = [0, 0, 0]
                counts[dp][0] += unc
                counts[dp][1] += fix
                counts[dp][2] += var
    
    X = []
    X4 = []
    Y1 = []
    Y2 = []
    Y3 = []
    Y4 = []
    T = []
    for i in sorted(counts):
        X.append(i)
        n = sum(counts[i])
        Y1.append(counts[i][0]/n)
        Y2.append(counts[i][1]/n)
        Y3.append(counts[i][2]/n)
        n4 = counts[i][1]+counts[i][2]
        if n4 > 0:
            X4.append(i)
            Y4.append(counts[i][2]/n4)
        T.append(n)

    pyplot.subplot(2,2,1)
    pyplot.plot(X, Y1, label='uncalled')
    pyplot.plot(X, Y2, label='fixed')
    pyplot.xlabel('depth')
    pyplot.ylabel('proportion of genotypes')
    pyplot.legend()
    pyplot.axvspan(20, 200, facecolor='0.9')

    pyplot.subplot(2,2,2)
    pyplot.plot(X, T)
    pyplot.xlabel('depth')
    pyplot.ylabel('number of genotypes')
    pyplot.axvspan(20, 200, facecolor='0.9')

    pyplot.subplot(2,2,3)
    pyplot.plot(X, Y3, label='heterozygote')
    pyplot.xlabel('depth')
    pyplot.ylabel('heterozygotes / total')
    pyplot.axvspan(20, 200, facecolor='0.9')

    pyplot.subplot(2,2,4)
    pyplot.plot(X4, Y4, label='x')
    pyplot.plot(X4, [i*100 for i in Y4], label='x * 100')
    pyplot.ylim(0, 1)
    pyplot.xlabel('depth')
    pyplot.ylabel('heterozygotes / called')
    pyplot.legend()
    pyplot.axvspan(20, 200, facecolor='0.9')

    pyplot.savefig('threshold{0}.png'.format(thre))
    pyplot.clf()
