"""
Plot DL for all chromosomes.

PATH: work directory.
"""

from matplotlib import pyplot, rcParams
import sys, os, math
sys.path.append('../data')
import lib

a, b = rcParams['figure.figsize']
rcParams['figure.figsize'] = 2*a, 2*b

PATH = os.path.expanduser('~/WORK1/pop-genomics-work')

def colour(r2):
    return '{0:.2f}'.format(1-r2)

for LGi in range(18):
    LG = 'LG{0:0>2}'.format(LGi+1)
    num_bits = int(lib.LGs[LG]/1000) # number of Kbp (last one might be used)

    X = [[], [], [], []]
    Y = [[], [], [], []]
    Z = [[], [], [], []]

    with open(PATH + '/LD-pairwise/ld-{0}.txt'.format(LG)) as f:
        n = 0
        for line in f:
            x, y, *zs = line.split()
            for i, z in enumerate(zs):
                if z != 'NA':
                    x = int(x)
                    y = int(y)
                    Y[i].append(y-x-1) # if sites are neighbour: closer to bottom
                    X[i].append((x+y)//2) # shift on the right
                    Z[i].append(colour(float(z)))
            n += 1
            if n%5e5 == 0:
                print('{0} {1:.2f}Mbp'.format(LG, int(x)/1000))

    lim = int(lib.LGs[LG]/1000)+1
    ticklabels = [i/2 for i in range(math.ceil((lim)/1000)*2)]
    ticks = [i*1000 for i in ticklabels]

    print(LG, 'generating plot')
    fig, axes = pyplot.subplots(2, 2)

    #X[0] = X[0][::100]
    #X[1] = X[1][::100]
    #X[2] = X[2][::100]
    #X[3] = X[3][::100]
    #Y[0] = Y[0][::100]
    #Y[1] = Y[1][::100]
    #Y[2] = Y[2][::100]
    #Y[3] = Y[3][::100]
    #Z[0] = Z[0][::100]
    #Z[1] = Z[1][::100]
    #Z[2] = Z[2][::100]
    #Z[3] = Z[3][::100]

    axes[0][0].scatter(X[0], Y[0], ls='None', s=1, marker='.', c=Z[0])
    axes[0][0].set_title('Avr1993')
    axes[0][0].set_xticks(ticks)
    axes[0][0].set_xticklabels(ticklabels, fontsize=6)
    axes[0][0].set_yticks([])
    axes[0][0].spines["top"].set_visible(False)
    axes[0][0].spines["left"].set_visible(False)
    axes[0][0].spines["right"].set_visible(False)
    axes[0][0].set_xlim(0, lim)
    axes[0][0].set_ylim(0, max(Y[0]))
    axes[0][0].set_aspect("equal")

    axes[0][1].scatter(X[1], Y[1], ls='None', s=1, marker='.', c=Z[1])
    axes[0][1].set_title('Vir1994')
    axes[0][1].set_xticks(ticks)
    axes[0][1].set_xticklabels(ticklabels, fontsize=6)
    axes[0][1].set_yticks([])
    axes[0][1].spines["top"].set_visible(False)
    axes[0][1].spines["left"].set_visible(False)
    axes[0][1].spines["right"].set_visible(False)
    axes[0][1].set_xlim(0, lim)
    axes[0][1].set_ylim(0, max(Y[1]))
    axes[0][1].set_aspect("equal")

    axes[1][0].scatter(X[2], Y[2], ls='None', s=1, marker='.', c=Z[2])
    axes[1][0].set_title('Vir1998')
    axes[1][0].set_xticks(ticks)
    axes[1][0].set_xticklabels(ticklabels, fontsize=6)
    axes[1][0].set_yticks([])
    axes[1][0].spines["top"].set_visible(False)
    axes[1][0].spines["left"].set_visible(False)
    axes[1][0].spines["right"].set_visible(False)
    axes[1][0].set_xlim(0, lim)
    axes[1][0].set_ylim(0, max(Y[2]))
    axes[1][0].set_aspect("equal")

    axes[1][1].scatter(X[3], Y[3], ls='None', s=1, marker='.', c=Z[3])
    axes[1][1].set_title('Wild2008')
    axes[1][1].set_xticks(ticks)
    axes[1][1].set_xticklabels(ticklabels, fontsize=6)
    axes[1][1].set_yticks([])
    axes[1][1].spines["top"].set_visible(False)
    axes[1][1].spines["left"].set_visible(False)
    axes[1][1].spines["right"].set_visible(False)
    axes[1][1].set_xlim(0, lim)
    axes[1][1].set_ylim(0, max(Y[3]))
    axes[1][1].set_aspect("equal")
    fig.savefig('plots/{0}.png'.format(LG))
    pyplot.close(fig)
