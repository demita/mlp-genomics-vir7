"""
Compute LD for a zoomed region.

LG: chromosome name.
first: first position to extract (included).
last: last position to extract (included).
PATH: work directory.
LEVELS: name of samples.
"""

import os, sys, egglib, time
sys.path.append('../data')
import lib
LG = 'LG15'
first = 1800000
last = 2400000
PATH = os.path.expanduser('~/WORK/pop-genomics-work')
LEVELS = '1993', '1994', '1998', '2008'

print('loading sites')
sites = []
positions = []
with open(PATH + '/LD-pairwise/sites-zoom-{0}-{1}-{2}.txt'.format(LG, first, last)) as f:
    for line in f:
        pos, site = line.split()
        sites.append(egglib.site_from_list(site, egglib.alphabets.DNA))
        positions.append(int(pos))
n = len(sites)

# compute LD for all pairs
npairs = int(n * (n-1) / 2)
cnt = 0
T0 = time.time()
with open(PATH + '/LD-pairwise/ld-zoom-{0}-{1}-{2}.txt'.format(LG, first, last), 'w') as f:
    for i in range(n):
        for j in range(i+1, n):
            res = []
            for k in LEVELS:
                rsq = egglib.stats.pairwise_LD(sites[i], sites[j], struct=lib.dict_structs[k])['rsq']
                if rsq is None: res.append('NA')
                else: res.append(rsq)
            f.write('{0} {1} {2[0]} {2[1]} {2[2]} {2[3]}\n'.format(positions[i], positions[j], res))
            cnt += 1
            if cnt % 50000 == 0:
                t = (time.time() - T0)/cnt * (npairs - cnt)
                h = int(t // 3600)
                t -= h*3600
                m = int(t // 60)
                s = int(t - m*60)
                print('{0:.2f}% -- remaining: {1:0>2}:{2:0>2}:{3:0>2}'.format(cnt/npairs*100, h, m, s))
                
                
