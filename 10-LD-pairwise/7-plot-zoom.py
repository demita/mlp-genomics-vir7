"""
Plot LD for a zoomed region

LG: chromosome name.
first: first position to extract (included).
last: last position to extract (included).
"""

from matplotlib import pyplot, rcParams
import sys, os, math
LG = 'LG15'
first = 1800000
last = 2400000

a, b = rcParams['figure.figsize']
rcParams['figure.figsize'] = 2*a, 2*b

PATH = os.path.expanduser('~/WORK1/pop-genomics-work')

def colour(r2):
    return '{0:.2f}'.format(1-r2)

X = [[], [], [], []]
Y = [[], [], [], []]
Z = [[], [], [], []]

print('loading site positions')
positions = []
with open(PATH + '/LD-pairwise/sites-zoom-{0}-{1}-{2}.txt'.format(LG, first, last)) as f:
    for line in f:
        pos, site = line.split()
        positions.append(int(pos))
print(len(positions), 'positions loaded')

print('loading LD values')
with open(PATH + '/LD-pairwise/ld-zoom-{0}-{1}-{2}.txt'.format(LG, first, last)) as f:
    n = 0
    for line in f:
        x, y, *zs = line.split()
        x = positions.index(int(x))
        y = positions.index(int(y))
        for i, z in enumerate(zs):
            if z != 'NA':
                Y[i].append(y-x-1) # if sites are neighbour: closer to bottom
                X[i].append((x+y)//2) # shift on the right
                Z[i].append(colour(float(z)))
        n += 1
        if n%1e5 == 0:
            print('{0:.3f}Mbp'.format(positions[x]/1e6))

preticklabels = list(range(first, last+1, 40000))
i = 0
ticks = []
next_ = None
for v in preticklabels:
    if v > 2233055 and next_ is None:
        next_ = v
        ticks.append(positions.index(2233055))
    if i == len(positions):
        ticks.append(len(positions))
        break
    while positions[i] < v:
        i += 1
        if i == len(positions):
            ticks.append(len(positions))
            break
    else:
        if positions[i] == v:
            ticks.append(i)
        else:
            ticks.append(i-0.5)
ticklabels = ['{0:.2f}'.format(i/1e6) for i in preticklabels]
ticklabels.insert(preticklabels.index(next_) - 1, 'X')

print('generating plot')
fig, axes = pyplot.subplots(2, 2)

axes[0][0].scatter(X[0], Y[0], ls='None', s=1, marker='.', c=Z[0])
axes[0][0].set_title('Avr1993')
axes[0][0].set_xticks(ticks)
axes[0][0].set_xticklabels(ticklabels, fontsize=6)
axes[0][0].set_yticks([])
axes[0][0].spines["top"].set_visible(False)
axes[0][0].spines["left"].set_visible(False)
axes[0][0].spines["right"].set_visible(False)
axes[0][0].set_ylim(0, max(Y[0]))
axes[0][0].set_aspect("equal")

axes[0][1].scatter(X[1], Y[1], ls='None', s=1, marker='.', c=Z[1])
axes[0][1].set_title('Vir1994')
axes[0][1].set_xticks(ticks)
axes[0][1].set_xticklabels(ticklabels, fontsize=6)
axes[0][1].set_yticks([])
axes[0][1].spines["top"].set_visible(False)
axes[0][1].spines["left"].set_visible(False)
axes[0][1].spines["right"].set_visible(False)
axes[0][1].set_ylim(0, max(Y[1]))
axes[0][1].set_aspect("equal")

axes[1][0].scatter(X[2], Y[2], ls='None', s=1, marker='.', c=Z[2])
axes[1][0].set_title('Vir1998')
axes[1][0].set_xticks(ticks)
axes[1][0].set_xticklabels(ticklabels, fontsize=6)
axes[1][0].set_yticks([])
axes[1][0].spines["top"].set_visible(False)
axes[1][0].spines["left"].set_visible(False)
axes[1][0].spines["right"].set_visible(False)
axes[1][0].set_ylim(0, max(Y[2]))
axes[1][0].set_aspect("equal")

axes[1][1].scatter(X[3], Y[3], ls='None', s=1, marker='.', c=Z[3])
axes[1][1].set_title('Wild2008')
axes[1][1].set_xticks(ticks)
axes[1][1].set_xticklabels(ticklabels, fontsize=6)
axes[1][1].set_yticks([])
axes[1][1].spines["top"].set_visible(False)
axes[1][1].spines["left"].set_visible(False)
axes[1][1].spines["right"].set_visible(False)
axes[1][1].set_ylim(0, max(Y[3]))
axes[1][1].set_aspect("equal")
fig.savefig('plots/zoomed-{0}-{1}-{2}.png'.format(LG, first, last))
pyplot.close(fig)

