for i in plots/LG*.png
do
    s=`basename $i`
    num=${s:2:2}
    convert plots/LG${num}.png -draw "scale 3,3 text 20,20 'chr${num}'"  \( legend.png -resize 75% -geometry +1000+25 \) -composite tmp${num}.png
done
convert tmp*.png Supplementary_figure_3.pdf
rm tmp*.png
