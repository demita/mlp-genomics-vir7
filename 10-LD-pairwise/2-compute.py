"""
Compute pairwise LD between all pairs of SNPs, indexed by their kb

PATH: work directory.
LEVELS: name of samples.
MAX_THREADS: number of parallelization threads.
"""

import egglib, os, sys, multiprocessing, random
sys.path.append('../data')
import lib

# header
PATH = os.path.expanduser('~/WORK/pop-genomics-work')
LEVELS = '1993', '1994', '1998', '2008'
MAX_THREADS = 10


# processing function
def process(LGi, queue):
    LG = 'LG{0:0>2}'.format(LGi+1)

    # load sites to a list
    sites = [None for i in set([i//1000 for i in range(lib.LGs[LG])])] # one slot per Kbp of the chromosome
    print(LG, 'loading sites')
    with open(PATH + '/LD-pairwise/sites-{0}.txt'.format(LG)) as f:
        for line in f:
            kb, site = line.split()
            kb = int(kb)
            assert sites[kb] is None
            sites[kb] = egglib.site_from_list(site, egglib.alphabets.DNA)
    n = len(sites)
    print(LG, n, 'sites')

    # compute LD for all pairs
    npairs = int(n * (n-1) / 2)
    cnt = 0
    with open(PATH + '/LD-pairwise/ld-{0}.txt'.format(LG), 'w') as f:
        print('start', LG)
        for i in range(n):
            for j in range(i+1, n):
                if sites[i] is None or sites[j] is None:
                    res = ['NA'] * len(LEVELS)
                else:
                    res = []
                    for k in LEVELS:
                        rsq = egglib.stats.pairwise_LD(sites[i], sites[j], struct=lib.dict_structs[k])['rsq']
                        if rsq is None: res.append('NA')
                        else: res.append(rsq)
                f.write('{0} {1} {2[0]} {2[1]} {2[2]} {2[3]}\n'.format(i, j, res))
                cnt += 1
                if cnt % 50000 == 0:
                    print(LG, '{0:.2f}%'.format(cnt/npairs*100))
        print(LG, 'done')

    # finish
    queue.put(None)

# start processes
print('running on {0} CPUs'.format(MAX_THREADS))
queue = multiprocessing.Queue()
wait = []
for i in range(18):
    p = multiprocessing.Process(target=process, args=[i, queue])
    wait.append(p)
random.shuffle(wait)

# start job
for i in range(MAX_THREADS):
    wait.pop().start()

# monitor jobs
for i in range(18):
    queue.get()
    if len(wait) > 0:
        wait.pop().start()
