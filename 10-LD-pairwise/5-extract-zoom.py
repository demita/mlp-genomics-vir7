"""
Extract all SNPs of a given region of the genome

LG: chromosome name.
first: first position to extract (included).
last: last position to extract (included).
PATH: work directory.
"""

import os
LG = 'LG15'
first = 1800000
last = 2400000
PATH = os.path.expanduser('~/WORK/pop-genomics-work')

with open(PATH + '/filter-sites/{0}.txt'.format(LG)) as f:
    f.readline() # header

    with open(PATH + '/LD-pairwise/sites-zoom-{0}-{1}-{2}.txt'.format(LG, first, last), 'w') as outf:
        c = 0

        for line in f:
            pos, cat, *site = line.split()
            pos = int(pos)
            if pos%100000 == 0: print('{0:.2f}Mbp'.format(pos/1e6))
            if cat == 'V':
                if pos < first: continue
                if pos > last: break
                c += 1
                outf.write('{0} {1}\n'.format(pos, ''.join(site)))

print('number of sites:', c)
