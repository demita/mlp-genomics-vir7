"""
Extract ~10% of sites (more exactly, 1 SNP max per kbp). SNPs are saved
in ad hoc files.

PATH: work directory.
"""

import os, sys
sys.path.append('../data')
import lib

PATH = os.path.expanduser('~/WORK/pop-genomics-work')

nums = []

for LGi in range(18):
    LG = 'LG{0:0>2}'.format(LGi + 1)
    with open(PATH + '/filter-sites/{0}.txt'.format(LG)) as f:
        f.readline() # header

        with open(PATH + '/LD-pairwise/sites-{0}.txt'.format(LG), 'w') as outf:
            c = 0
            curkb = 0

            for line in f:
                pos, cat, *site = line.split()
                if cat == 'V':
                    pos = int(pos)
                    kb = pos//1000
                    if kb == curkb:
                        continue
                    c += 1
                    curkb = kb
                    print(LG, curkb, 'Kbp')
                    outf.write('{0} {1}\n'.format(kb, ''.join(site)))
            nums.append(c)

