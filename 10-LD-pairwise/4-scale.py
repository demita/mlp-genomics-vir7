"""
Generate a dummy plot with the legend for LD plots.
"""

from matplotlib import pyplot, colors

cmap = colors.ListedColormap([str(1-i/255) for i in range(256)])
fig, ax = pyplot.subplots(1, figsize=(3, 1))
gradient = [i/255 for i in range(256)]
gradient = [gradient, gradient]
ax.imshow(gradient, aspect=4, cmap=cmap)
ax.set_yticks([])
ax.set_xticks([0, 0.2*255, 0.4*255, 0.6*255, 0.8*255, 1*255])
ax.set_xticklabels([0, 0.2, 0.4, 0.6, 0.8, 1])
ax.set_xlabel('$r^2$')
fig.savefig('legend.png')
