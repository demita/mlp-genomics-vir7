"""
Compute statistics over all sites of the genome.

Requires that window statistics are available with specified parameters.

PATH: work directory.
NUM_THREADS: number of parallel threads
NUM_REPL: number of replicates (for subsample CI)
PROPOR: proportion of sites to consider for replications
WSIZE: size of nonoverlapping window (for window CI) 
"""

import egglib, os, sys, multiprocessing, random, math
sys.path.append('../data')
import lib
pops = ['1993', '1994', '1998', '2008', 'vir7', 'avr7']
pairs = ['vir7avr7', '1993v1994', '1993v1998', '1993v2008', '1994v1998', '1994v2008', '1998v2008']

# options
PATH = os.path.expanduser('~/disks/WORK1/pop-genomics-work/filter-sites/')
NUM_THREADS = 40
NUM_REPL = 1000
PROPOR = 0.5
WSIZE = 5000

# initialize IC dictionarys
def spawn():
    d = { 
        'thetaW': [],
        'Pi': [],
        'D': [],
        'D*': [],
        'F*': [],
        'Fis': [],
        'Gste': [],
        'Dj': [],
        'WC1_t': [],
        'WC2_f': [],
        'WC2_t': [],
        'WC2_F': [],
        'WC3_f': [],
        'WC3_t1': [],
        'WC3_t2': [],
        'WC3_F': []
    }
    for k in pops:
        d[(k, 'S')] = []
        d[(k, 'thetaW')] = []
        d[(k, 'Pi')] = []
        d[(k, 'D')] = []
        d[(k, 'D*')] = []
        d[(k, 'F*')] = []
        d[(k, 'Fis')] = []
    for p in pairs:
        d[(p, 'Gste')] = []
        d[(p, 'Da')] = []
        d[(p, 'Dxy')] = []
        d[(p, 'Dj')] = []
        d[(p, 'WC1_t')] = []
        d[(p, 'WC2_f')] = []
        d[(p, 'WC2_t')] = []
        d[(p, 'WC2_F')] = []
    return d
IC = spawn()# from replicates 
ICw = {k: [] for k in IC} # from windows

# getting ICs from windows
def helper_getter(LG, label, stats, dest, key=None):
    with open(os.path.join(PATH, '..', 'stats-windows-wsize{0}-wstep{0}/{1}-{2}.txt'.format(WSIZE, LG, label))) as f:
        h = f.readline().split()
        ls = []
        for line in f:
            line = line.split()
            assert len(line) == len(h)
            d = {}
            for k, v in zip(h, line):
                if v == 'None': v = None
                else:
                    try: v = int(v)
                    except ValueError:
                        try: v = float(v)
                        except ValueError:
                            v = map(float, v.split(','))
                d[k] = v
            for k in stats:
                dest[k if key is None else (key, k)].append(d[k])
            ls.append(d['ls'])
        return ls

LS = []
for LGi in range(18):
    LG = 'LG{0:0>2}'.format(LGi+1)
    print(LG, end='', flush=True)
    ls = helper_getter(LG, 'total', ['thetaW', 'Pi', 'D', 'Fis', 'D*', 'F*', 'Dj', 'Gste', 'WC1_t', 'WC2_f', 'WC2_t', 'WC2_F'], ICw)
    assert ls == helper_getter(LG, 'hierarchical', ['WC3_f', 'WC3_t1', 'WC3_t2', 'WC3_F'], ICw)
    for pop in pops:
        assert ls == helper_getter(LG, pop, ['S', 'thetaW', 'Pi', 'D', 'D*', 'F*', 'Fis'], ICw, pop)
    for pair in pairs:
        assert ls == helper_getter(LG, pair, ['Gste', 'Da', 'Dxy', 'Dj', 'WC1_t', 'WC2_f', 'WC2_t', 'WC2_F'], ICw, pair)
    LS.extend(ls)
    print('\b'*4, end='')

n = len(LS)
for k in ICw:
    if isinstance(k, tuple): s = '{0[0]}_{0[1]}'.format(k)
    else: s = k
    print('{0: <18}'.format(s), end='', flush=True)
    assert len(ICw[k]) == n

    # weighted average
    sum_ls = 0
    sum_stat = 0
    for ls, stat in zip(LS, ICw[k]):
        if stat is not None:
            sum_stat += stat * ls
            sum_ls += ls
    avgw = sum_stat/sum_ls

    # average
    ICw[k] = list(filter(None, ICw[k]))
    n2 = len(ICw[k])
    avg = sum(ICw[k]) / n2

    # CI
    ICw[k].sort()
    CIlo = ICw[k][int(math.floor(n2*0.025))]
    CIhi = ICw[k][int(math.ceil(n2*0.975))]

    ICw[k] = (avg, avgw, CIlo, CIhi)
    print('\b'*18, end='')

# function to run a replicate (or main analysis)
def process(label, queue, proportion=1):
    assert proportion > 0 and proportion <= 1
    cs = {}
    cs['4pop'] = egglib.stats.ComputeStats(lib.dict_structs['4pop'])
    cs['4pop'].add_stats('lseff', 'S', 'thetaW', 'Pi', 'D', 'D*', 'F*', 'Fis', 'FistWC', 'Dj', 'Gste', 'FstWC')
    cs['hier'] = egglib.stats.ComputeStats(lib.dict_structs['hier'])
    cs['hier'].add_stats('lseff', 'S', 'FisctWC')
    for k in pops:
        cs[k] = egglib.stats.ComputeStats(lib.dict_structs[k])
        cs[k].add_stats('lseff', 'S', 'thetaW', 'Pi', 'D', 'D*', 'F*', 'Fis')
    for p in pairs:
        cs[p] = egglib.stats.ComputeStats(lib.dict_structs[p])
        cs[p].add_stats('lseff', 'FstWC', 'FistWC', 'Gste', 'Dj', 'Dxy', 'Da')
    num_tot = 0
    num_var = 0
    for LGi in range(18):
        LG = 'LG{0:0>2}'.format(LGi+1)
        with open(PATH + LG + '.txt') as f:
            header = f.readline()
            for line in f:
                pos, status, *site = line.split()
                pos = int(pos)
                if pos%1000000 == 0:
                    print('{0} - {1} {2:.2f}%'.format(label, LG, 100*pos/lib.LGs[LG]))
                if proportion < 1 and random.random() < proportion:
                    continue
                num_tot += 1
                if status == 'V':
                    num_var += 1
                    site = egglib.site_from_list(''.join(site), alphabet=egglib.alphabets.DNA)
                    for v in cs.values():
                        v.process_site(site)
    for k,v in cs.items():
        cs[k] = v.results()
    queue.put((label, num_tot, num_var, cs))

# parametring threads
def key(i):
    return str('rep{0:0>4}'.format(i+1))
queue = multiprocessing.Queue()
processes = [multiprocessing.Process(target=process, args=('main', queue, 1))]
for i in range(NUM_REPL):
    processes.append(multiprocessing.Process(target=process, args=(key(i), queue, PROPOR)))

# function extracting a replicate
def extract(dest, label, nt, nv, stats):
    if label == 'main':
        dest['main'] = spawn()
        dest = dest['main']
        assert stats['4pop']['lseff'] == nv
        assert stats['hier']['lseff'] == nv
        assert stats['4pop']['S'] == nv
        assert stats['hier']['S'] == nv
        dest['nt'] = nt
        dest['nv'] = nv
    dest['thetaW'].append(stats['4pop']['thetaW']/nt)
    dest['Pi'].append(stats['4pop']['Pi']/nt)
    for k in 'D', 'D*', 'F*', 'Fis', 'Gste', 'Dj':
        dest[k].append(stats['4pop'][k])
    dest['WC1_t'].append(stats['4pop']['FstWC'])
    dest['WC2_f'].append(stats['4pop']['FistWC'][0])
    dest['WC2_t'].append(stats['4pop']['FistWC'][1])
    dest['WC2_F'].append(stats['4pop']['FistWC'][2])
    dest['WC3_f'].append(stats['hier']['FisctWC'][0])
    dest['WC3_t1'].append(stats['hier']['FisctWC'][1])
    dest['WC3_t2'].append(stats['hier']['FisctWC'][2])
    dest['WC3_F'].append(stats['hier']['FisctWC'][3])
    for k in pops:
        dest[(k, 'S')].append(stats[k]['S'])
        dest[(k, 'thetaW')].append(stats[k]['thetaW']/nt)
        dest[(k, 'Pi')].append(stats[k]['Pi']/nt)
        for s in 'D', 'D*', 'F*', 'Fis':
            dest[(k, s)].append(stats[k][s])
    for p in pairs:
        dest[(p, 'Gste')].append(stats[p]['Gste'])
        dest[(p, 'Da')].append(stats[p]['Da'])
        dest[(p, 'Dxy')].append(stats[p]['Dxy'])
        dest[(p, 'Dj')].append(stats[p]['Dj'])
        dest[(p, 'WC1_t')].append(stats[p]['FstWC'])
        dest[(p, 'WC2_f')].append(stats[p]['FistWC'][0])
        dest[(p, 'WC2_t')].append(stats[p]['FistWC'][1])
        dest[(p, 'WC2_F')].append(stats[p]['FistWC'][2])
    return label

# running threads
for i in range(NUM_THREADS):
    if len(processes):
        processes.pop(0).start()
for i in range(NUM_REPL+1):
    res = extract(IC, *queue.get())
    print('{0} finished'.format(res))
    if len(processes): 
        processes.pop(0).start()

# compute IC
main = IC.pop('main')
for k in IC:
    IC[k].sort()
    IC[k] = (IC[k][int(math.floor(NUM_REPL*0.025))],
              IC[k][int(math.ceil(NUM_REPL*0.975))])

# export results
hdr = '| level       | stat            |     value |    subsampling IC   |  win avg  |  weighted |      windows IC     |\n'
spc = '+-------------+-----------------+-----------+---------------------+-----------+-----------+---------------------+\n'
fmt1 = '| {0:<11s} | {1:<15s} | {2:+.6f} | {3[0]:+.6f} {3[1]:+.6f} | {4[0]:+.6f} | {4[1]:+.6f} | {4[2]:+.6f} {4[3]:+.6f} |\n'
fmt2 = '| {0:<11s} | {1:<15s} | {2:>9d} | {3[0]:>9d} {3[1]:>9d} | {4[0]:>9.2f} | {4[1]:>9.2f} | {4[2]:>9d} {4[3]:>9d} |\n'
def write(f, lbl, key, v, IC1, IC2):
    f.write((fmt2 if key == 'S' else fmt1).format(lbl, key, v, IC1, IC2))

_stat_mapping = {
    'Gste': 'Gst\'',
    'Dj': 'Jost\'s D',
    'WC1_t': 'WC\'s Fst (1lvl)',
    'WC2_f': 'WC\'s Fis (2lvl)',
    'WC2_t': 'WC\'s Fst (2lvl)',
    'WC2_F': 'WC\'s Fit (2lvl)',
    'WC3_f': 'WC\'s Fis (3lvl)',
    'WC3_t1': 'WC\'s Fsc (3lvl)',
    'WC3_t2': 'WC\'s Fct (3lvl)',
    'WC3_F': 'WC\'s Fit (3lvl)'
}
def stat_mapping(x):
    if x in _stat_mapping: return _stat_mapping[x]
    else: return x

with open('results.txt', 'w') as f:
    f.write('total number of sites: {0}\n'.format(main['nt']))
    f.write('variable sites: {0}\n\n'.format(main['nv']))
    f.write(spc)
    f.write(hdr)
    f.write(spc)

    for stat in ['thetaW', 'Pi', 'D', 'D*', 'F*', 'Fis', 'Gste', 'Dj',
                 'WC1_t', 'WC2_f', 'WC2_t', 'WC2_F', 'WC3_f', 'WC3_t1',
                 'WC3_t2', 'WC3_F']:
        write(f, 'global', stat_mapping(stat), main[stat][0], IC[stat], ICw[stat])

    for k in pops:
        f.write(spc)
        for stat in ['S', 'thetaW', 'Pi', 'D', 'D*', 'F*', 'Fis']:
            write(f, k, stat_mapping(stat), main[(k, stat)][0], IC[(k, stat)], ICw[(k, stat)])

    for p in pairs:
        f.write(spc)
        for stat in ['Gste', 'Da', 'Dj', 'WC1_t', 'WC2_f', 'WC2_t', 'WC2_F']:
            write(f, p, stat_mapping(stat), main[(p, stat)][0], IC[(p, stat)], ICw[(p, stat)])
    f.write(spc)

