convert BIC.pdf  -bordercolor White -border 50x50 -draw "scale 3,3 text 20,20 'A'"   A.gif
for i in 3 4 5; do qpdf --pages Rplots.pdf $i -- Rplots.pdf Rplots-${i}.pdf; done
convert Rplots-3.pdf  -bordercolor White -border 50x50 -draw "scale 3,3 text 20,20 'B'"   B.gif
convert Rplots-4.pdf  -bordercolor White -border 50x50 -draw "scale 3,3 text 20,20 'C'"   C.gif
convert Rplots-5.pdf  -bordercolor White -border 50x50 -draw "scale 3,3 text 20,20 'D'"  D.gif
convert bars.png  -bordercolor White -border 50x50 \
    -draw "scale 3,3 text 5,30 'E'" \
    -draw "scale 3,3 text 5,122 'F'" \
    -draw "scale 3,3 text 5,203 'G'" EFG.gif
montage -tile 3x1 -geometry +4+4 B.gif C.gif D.gif BCD.gif
montage -tile 1x3 -geometry +4+4 A.gif BCD.gif EFG.gif Supplementary_figure_1.png
rm A.gif B.gif C.gif D.gif BCD.gif EFG.gif Rplots-*.pdf
