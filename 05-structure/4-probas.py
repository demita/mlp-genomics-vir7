"""
Make a bar chart with group membership probabilities
"""

names = {}
P = {}

for K in [2, 3, 4]:
    names[K] = []
    P[K] = [[] for i in range(K)]
    with open('dapc-{0}.csv'.format(K)) as f:
        assert f.readline() == '"","",{0}\n'.format(','.join(['"{0}"'.format(i+1) for i in range(K)]))
        for line in f:
            idv, grp, *p, = line.strip().split(',')
            assert len(p) == K
            idv = idv.strip('"')
            grp = int(grp)
            p = list(map(float, p))
            assert p.index(max(p)) + 1 == grp
            names[K].append(idv)
            for i, v in enumerate(p):
                P[K][i].append(v)

assert names[2]==names[3] and names[3]==names[4]

from matplotlib import pyplot
fig, axes = pyplot.subplots(3, 1, figsize=(20, 8))

BLUE = [0.2, 0.2, 1]
BLUE1 = [0.2, 0.8, 1]
RED = [1, 0.2, 0.2]
RED1 = [1, 0.6, 0.4]


n = len(names[2])
X = range(n)
bot = [0] * n
axes[0].bar(x=X, height=P[2][0], bottom=bot, align='edge', color=BLUE, width=1, edgecolor='k')
bot = [i+j for i,j in zip(bot, P[2][0])]
axes[0].bar(x=X, height=P[2][1], bottom=bot, align='edge', color=RED, width=1, edgecolor='k')

bot = [0] * n
axes[1].bar(x=X, height=P[3][0], align='edge', color=RED, width=1, edgecolor='k')
bot = [i+j for i,j in zip(bot, P[3][0])]
axes[1].bar(x=X, height=P[3][1], bottom=bot, align='edge', color=BLUE, width=1, edgecolor='k')
bot = [i+j for i,j in zip(bot, P[3][1])]
axes[1].bar(x=X, height=P[3][2], bottom=bot, align='edge', color=BLUE1, width=1, edgecolor='k')

bot = [0] * n
axes[2].bar(x=X, height=P[4][0], align='edge', color=RED1, width=1, edgecolor='k')
bot = [i+j for i,j in zip(bot, P[4][0])]
axes[2].bar(x=X, height=P[4][1], bottom=bot, align='edge', color=RED, width=1, edgecolor='k')
bot = [i+j for i,j in zip(bot, P[4][1])]
axes[2].bar(x=X, height=P[4][2], bottom=bot, align='edge', color=BLUE1, width=1, edgecolor='k')
bot = [i+j for i,j in zip(bot, P[4][2])]
axes[2].bar(x=X, height=P[4][3], bottom=bot, align='edge', color=BLUE, width=1, edgecolor='k')

axes[0].set_xlim(0, X[-1]+1)
axes[1].set_xlim(0, X[-1]+1)
axes[2].set_xlim(0, X[-1]+1)

axes[0].set_xticks([i+0.5 for i in range(n)])
axes[0].set_xticklabels(names[2], rotation=90)
axes[0].tick_params(axis='x', bottom=False, labeltop=True, labelbottom=False)
axes[2].set_xticks([i+0.5 for i in range(n)])
axes[2].set_xticklabels(names[2], rotation=90)
axes[2].tick_params(axis='x', bottom=False)

lims = []
c = None
for i, name in enumerate(names[2]):
    if name[:2] != c:
        c = name[:2]
        lims.append(i)
del lims[0]
axes[0].set_xticks(lims, minor=True)
axes[0].tick_params(axis='x', which='minor', top=True, bottom=False, labelbottom=False, length=50, width=3)
axes[1].set_xticks(lims)
axes[1].tick_params(axis='x', top=True, labelbottom=False, length=20, width=3)
axes[2].set_xticks(lims, minor=True)
axes[2].tick_params(axis='x', which='minor', labelbottom=False, length=50, width=3)
for lim in lims:
    axes[0].axvline(lim, lw=3, ls='-', c='k')
    axes[1].axvline(lim, lw=3, ls='-', c='k')
    axes[2].axvline(lim, lw=3, ls='-', c='k')

axes[0].set_ylim(0, 1)
axes[1].set_ylim(0, 1)
axes[2].set_ylim(0, 1)
axes[0].set_ylabel('Membership coefficient')
axes[1].set_ylabel('Membership coefficient')
axes[2].set_ylabel('Membership coefficient')

fig.tight_layout(h_pad=6)

pyplot.savefig('bars.png', bbox_inches='tight')
