"""
This script takes all SNPs and generates a text file named genotypes.txt

Requires filtered sites in the specified directory.
"""

import os
PATH = os.path.expanduser('~/disks/backup2/work/pop-genomics-work')

#create output file
outputfile = open(os.path.join(PATH, 'dapc', 'genotypes.txt'), 'w')

#iteration per LG on sites files
for LGi in range(18):
    LG = 'LG{0:0>2}'.format(LGi+1)
    Mbp = 0.0
    with open(os.path.join(PATH, 'filter-sites', '{0}.txt'.format(LG))) as f:
        h = f.readline().split()
        assert h[0] == 'pos'
        for line in f:
            pos, mask, *sites = line.split()
            if mask == 'V' :
                outputfile.write(','.join(sites) + '\n')
            p = (int(pos) // 1e4) / 10
            if p != Mbp:
                print(LG, p, 'Mbp')
                Mbp = p
