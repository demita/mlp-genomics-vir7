# R script to run DAPC
# The adegenet module must be installed.
# The number of loci is hardcoded in the code, as well as the number of
# samples and the structure.
# Figures must be exported interactively.

library("adegenet")

populations <- c("Wild2008", "Wild2008", "Wild2008", "Wild2008", "Wild2008", "Wild2008", "Wild2008", "Wild2008", "Wild2008", "Wild2008", "Wild2008", "Wild2008", "Wild2008", "Wild2008", "Wild2008", "Wild2008", "Wild2008", "Wild2008", "Wild2008",
                 "Avr1993",  "Avr1993",  "Avr1993",  "Avr1993",  "Avr1993",  "Avr1993",  "Avr1993",  "Avr1993",  "Avr1993",  "Avr1993",  "Avr1993",  "Avr1993",  "Avr1993",  "Avr1993",  "Avr1993",  "Avr1993",  "Avr1993",  "Avr1993",
                 "Vir1994",  "Vir1994",  "Vir1994",  "Vir1994",  "Vir1994",  "Vir1994",  "Vir1994",  "Vir1994",  "Vir1994",  "Vir1994",  "Vir1994",  "Vir1994",  "Vir1994",  "Vir1994",  "Vir1994",  "Vir1994",  "Vir1994",  "Vir1994",
                 "Vir1998",  "Vir1998",  "Vir1998",  "Vir1998",  "Vir1998",  "Vir1998",  "Vir1998",  "Vir1998",  "Vir1998",  "Vir1998",  "Vir1998",  "Vir1998",  "Vir1998",  "Vir1998",  "Vir1998",  "Vir1998",  "Vir1998",  "Vir1998",  "Vir1998", "Vir1998", "Vir1998")

print(noquote("Loading data"))

n.loc <- 112729

ds <- read.structure("/home/user/WORK1/pop-genomics-work/dapc/genotypes.stru", n.ind=76, n.loc=n.loc,
                                       onerowperind=T, col.lab=1,
                                       row.marknames=1, NA.char=-9,
                                       ask=F, quiet=F)
ds

clus2 <-  find.clusters(ds, max.n.clust=20)
70
2
clus3 <-  find.clusters(ds, n.pca=70, n.clust=3)
clus4 <-  find.clusters(ds, n.pca=70, n.clust=4)
clus5 <-  find.clusters(ds, n.pca=70, n.clust=5)


table.value(table(populations, clus2$grp), col.lab=paste("cluster ", 1:2))
table.value(table(populations, clus3$grp), col.lab=paste("cluster ", 1:3))
table.value(table(populations, clus4$grp), col.lab=paste("cluster ", 1:4))
table.value(table(populations, clus5$grp), col.lab=paste("cluster ", 1:5))

dapc2 <- dapc(ds, clus2$grp, n.pca=20)
1
dapc3 <- dapc(ds, clus3$grp, n.pca=20)
2
dapc4 <- dapc(ds, clus4$grp, n.pca=20)
3
dapc5 <- dapc(ds, clus5$grp, n.pca=20)
3

scatter(dapc2, xax=1, yax=1)

mycolor<-c("#f10d0c", "#3465a4", "#069a2e")
scatter(dapc3, xax=1, yax=2, scree.da=T, scree.pca=T,col=mycolor, posi.da="topleft", posi.pca="topright")
scatter(dapc3, xax=1, yax=1)
scatter(dapc3, xax=2, yax=2)

mycolor2<-c("#f10d0c", "#f10d0c", "#069a2e", "#3465a4")
scatter(dapc4, xax=1, yax=2, scree.da=T, scree.pca=T, ,col=mycolor2,  posi.da="topleft", posi.pca="topright")
scatter(dapc4, xax=1, yax=3, scree.da=T, scree.pca=T, posi.da="bottomleft", posi.pca="bottomright")
scatter(dapc4, xax=1, yax=1)
scatter(dapc4, xax=2, yax=2)
scatter(dapc4, xax=3, yax=3)

scatter(dapc5, xax=1, yax=2, scree.da=T, scree.pca=T, posi.da="topleft", posi.pca="topright")
scatter(dapc5, xax=1, yax=3, scree.da=T, scree.pca=T, posi.da="bottomleft", posi.pca="bottomright")
scatter(dapc5, xax=1, yax=1)
scatter(dapc5, xax=2, yax=2)
scatter(dapc5, xax=3, yax=3)

write.csv(cbind(dapc2$assign,  dapc2$posterior),  file="dapc-2.csv")
write.csv(cbind(dapc3$assign,  dapc3$posterior),  file="dapc-3.csv")
write.csv(cbind(dapc4$assign,  dapc4$posterior),  file="dapc-4.csv")
write.csv(cbind(dapc5$assign,  dapc5$posterior),  file="dapc-5.csv")
