"""
This script transforms the genotypes.txt file in a file genotypes.stru
valid for DAPC. Since R cannot process all all, a random fraction of
sites are selected.
"""

import sys, os, random, re
sys.path.append('../data')
import lib
PATH = os.path.expanduser('~/disks/backup2/work/pop-genomics-work')

# Generate a list for the header with the pop abel for every isolates
pops = [(i, len(lib.pops[i])) for i in ['2008', '1993', '1994', '1998']]
nb_ind = len(lib.samples)
labels = []
for pop, ni in pops:
    for i in range(ni):
        labels.append('"' + pop + '"')
       
# Transpose DNA code (ACGT) in .stru code (1234)
f = open(os.path.join(PATH, 'dapc', 'genotypes.txt'))
loci = []
for line in f:
    locus = []
    if random.random() < 1/10.0 :
        for a,b in re.split('[;,]', line.strip()):
            if a in 'ACGT': locus.append('ACGT'.index(a)+1)
            else: locus.append(-9)
            if b in 'ACGT': locus.append('ACGT'.index(b)+1)
            else: locus.append(-9)
        loci.append(locus)
f.close()

# Assert number of isolates and pop are good
print('Number of loci:' , len(loci))
lens = set(map(len, loci))
assert len(lens) == 1
L = lens.pop()
assert L%2==0
n = L//2
print('number of isolates:', n)
assert n == nb_ind

# Generate inputfile for Dapc (.stru)
f = open(os.path.join(PATH, 'dapc', 'genotypes.stru'), 'w')
for i in range(len(loci)):
    f.write('\tlocus' + str(i+1).rjust(5, '0'))
f.write('\n')

for i in range(n):
    f.write(lib.samples[i])
    for j in range(len(loci)):
        f.write('\t' + str(loci[j][2*i]) + '\t' + str(loci[j][2*i+1]))
    f.write('\n')
f.close()
