"""
Compile lists of candidate sites

PATH: work directory
BP_THR: BayPass significance threshold
GW_THR: GWAS significance threshold
EHH_LVL: EHH detection level
HMM_K: freq-hmm sensitivity parameter
"""

import os, re, math
import openpyxl

PATH=os.path.expanduser('~/WORK1/pop-genomics-work')
BP_THR = 6
GW_THR = 6
EHH_LVL = 4
HMM_K = 20

# import BayPass results
BP1 = {}
with open('../11-baypass/core/markers_1e-{0}.txt'.format(BP_THR)) as f:
    for line in f:
        idx, LG, pos, v = line.split()
        idx = int(idx)
        pos = int(pos)
        v = float(v)
        k = LG, pos
        assert k not in BP1
        BP1[k] = v
print('BP1', len(BP1))
BP2 = {}
with open('../11-baypass/covariate/markers_1e-{0}.txt'.format(BP_THR)) as f:
    for line in f:
        idx, LG, pos, v = line.split()
        idx = int(idx)
        pos = int(pos)
        v = float(v)
        k = LG, pos
        assert k not in BP2
        BP2[k] = v
print('BP2', len(BP2))

# import GWAS results
GWcand0 = set()
GWcand = set()
GW0 = {}
GW7 = []
with open('../13-GWAS/GWAS_allPval_step0.txt') as f:
    assert f.readline().split()[0] == '"SNP"'
    for line in f:
        LG, pos, v = re.match('"SNP_(\d{2})_(\d+)"\t(.+)', line).groups()
        LG = 'LG'+LG
        pos = int(pos)
        v = math.log10(1/float(v))
        k = LG, pos
        if v > GW_THR:
            GWcand0.add(k)
            GWcand.add(k)
        GW0[k] = v
with open('../13-GWAS/GWAS_allPval_step7.txt') as f:
    assert f.readline().split()[0] == '"SNP"'
    for i in range(7):
        line = f.readline()
        LG, pos, v = re.match('"SNP_(\d{2})_(\d+)"\t(.+)', line).groups()
        LG = 'LG'+LG
        pos = int(pos)
        k = LG, pos
        GW7.append(k)
        GWcand.add(k)
print('GWAS: {0} + {1} = {2}'.format(len(GWcand0), len(GW7), len(GWcand)))

# import EHH results
EHH = {}
with open('../12-EHH/candidates/Rsb1_lvl{0}.txt'.format(EHH_LVL)) as f:
    f.readline()
    for line in f:
        LG, pos, iEG, Rsb = line.split()
        pos = int(pos)
        iEG = float(iEG)
        Rsb = float(Rsb)
        k = LG, pos
        assert k not in EHH
        EHH[k] = iEG, Rsb
print('EHH', len(EHH))

# import freq-hmm results
HMM = {}
HMM_regions = []
for LGi in range(18):
    LG = 'LG{0:0>2}'.format(LGi+1)
    with open(os.path.join(PATH, 'freq-hmm', '1998-{0}_1e-{1}.stat'.format(LG, HMM_K))) as f:
        num = int(f.readline())
        for line in f:
            first, last, p = line.split()
            first = int(first)
            last = int(last)
            p = float(p)
            HMM_regions.append((LG, first, last, p))
            for pos in range(first, last+1):
                k = LG, pos
                assert k not in HMM
                HMM[k] = math.log10(p)
print('HMM', len(HMM), 'regions:', len(HMM_regions))

# find common SNPs
cand = {}
for name, array in zip(['BP1', 'BP2', 'GW', 'EHH', 'HMM'], [BP1, BP2, GWcand, EHH, HMM]):
    for k in array:
        if k in cand: cand[k].append(name)
        else: cand[k] = [name]
for n in 2, 3, 4, 5:
    c = 0
    for k, v in cand.items():
        if len(v) >= n:
            c += 1
    print('common to {0}: {1}'.format(n, c))

set1 = set(BP1) | set(BP2) | set(GWcand)
set2 = set(EHH)
set3 = set(HMM)

print('BP1|BP2|GW & EHH', set1 & set2)
print('BP1|BP2|GW & HMM', set1 & set3)
print('EHH & HMM', set2 & set3)

### export lists of SNPs ##
wb = openpyxl.Workbook()
ws0 = wb.active

right_aln = openpyxl.styles.Alignment(horizontal='right')
center_aln = openpyxl.styles.Alignment(horizontal='center')
bold_font = openpyxl.styles.Font(bold=True)
large_font = openpyxl.styles.Font(size=20)
def header(ws, header, right, center=None):
    cells = [ws.cell(column=i+1, row=1, value=s) for (i, s) in enumerate(header)]
    for i in right: cells[i].alignment = right_aln
    if center is not None:
        for i in center: cells[i].alignment = center_aln
    for c in cells: c.font = bold_font
def save(ws, data):
    for i, ((LG, pos), v) in enumerate(data.items()):
        ws.cell(column=1, row=i+2, value=LG)
        C2 = ws.cell(column=2, row=i+2, value=pos)
        C3 = ws.cell(column=3, row=i+2, value=v)
        C2.number_format = '0,0'
        C3.number_format = '0.0000'
    ws.column_dimensions['B'].width = 11
    ws.column_dimensions['C'].width = 15

# BayPass core
ws1 = wb.create_sheet(title='BayPass core')
header(ws1, ['chr', 'position', 'log10(1/P)'], right=[1,2])
save(ws1, BP1)

# BayPass covariate
ws2 = wb.create_sheet(title='BayPass covariate')
header(ws2, ['chr', 'position', 'log10(1/P)'], right=[1,2])
save(ws2, BP2)

# GWAS
ws3 = wb.create_sheet(title='GWAS')
header(ws3, ['chr', 'position', 'P value', 'cofactor'], right=[1,2], center=[3])
n1 = 0
n2 = 0
for i, (LG, pos) in enumerate(sorted(GWcand)):
    ws3.cell(column=1, row=i+2, value=LG)
    C2 = ws3.cell(column=2, row=i+2, value=pos)
    C2.number_format = '0,0'
    p = GW0[(LG,pos)]
    Cx = ws3.cell(column=3, row=i+2, value=p)
    Cx.number_format = '0.0000'
    if (LG,pos) in GW7:
        Cx = ws3.cell(column=4, row=i+2, value=GW7.index((LG,pos))+1)
        Cx.alignment = center_aln
ws3.column_dimensions['B'].width = 11
ws3.column_dimensions['C'].width = 15
ws3.column_dimensions['D'].width = 10

# EHH
ws4 = wb.create_sheet(title='EHH')
header(ws4, ['chr', 'position', 'iEG (Vir1994)', 'ln(Rsb)'], right=[1,2,3])
for i, (LG, pos) in enumerate(sorted(EHH)):
    iEG, Rsb = EHH[(LG, pos)]
    ws4.cell(column=1, row=i+2, value=LG)
    C2 = ws4.cell(column=2, row=i+2, value=pos)
    C3 = ws4.cell(column=3, row=i+2, value=iEG)
    C4 = ws4.cell(column=4, row=i+2, value=Rsb)
    C2.number_format = '0,0'
    C3.number_format = '0,0.00'
    C4.number_format = '0.0000'
ws4.column_dimensions['B'].width = 11
ws4.column_dimensions['C'].width = 15
ws4.column_dimensions['D'].width = 15

# freq-hmm
ws5 = wb.create_sheet(title='freq-hmm')
header(ws5, ['chr', 'start', 'end', 'log10(P)'], right=[1,2,3])
for i, (LG, first, last, v) in enumerate(HMM_regions):
    ws5.cell(column=1, row=i+2, value=LG)
    C2 = ws5.cell(column=2, row=i+2, value=first)
    C3 = ws5.cell(column=3, row=i+2, value=last)
    C4 = ws5.cell(column=4, row=i+2, value='inf' if math.isinf(v) else v)
    C2.number_format = '0,0'
    C3.number_format = '0,0'
    if math.isinf(v): C4.alignment= right_aln
    else: C4.number_format = '0.0000'
ws5.column_dimensions['B'].width = 11
ws5.column_dimensions['C'].width = 11
ws5.column_dimensions['D'].width = 15

# common candidates
ws0.title = 'Common SNPs'

c = ws0.cell(column=1, row=1, value='chr')
c.font = bold_font
ws0.merge_cells('A1:A2')

c = ws0.cell(column=2, row=1, value='position')
c.alignment = right_aln
c.font = bold_font
ws0.merge_cells('B1:B2')

c = ws0.cell(column=3, row=1, value='BayPass')
c.alignment = center_aln
c.font = bold_font
ws0.merge_cells('C1:D1')

c = ws0.cell(column=3, row=2, value='core')
c.alignment = center_aln
c.font = bold_font

c = ws0.cell(column=4, row=2, value='covariate')
c.alignment = center_aln
c.font = bold_font

c = ws0.cell(column=5, row=1, value='GWAS')
c.alignment = center_aln
c.font = bold_font
ws0.merge_cells('E1:E2')

c = ws0.cell(column=6, row=1, value='EHH')
c.alignment = center_aln
c.font = bold_font
ws0.merge_cells('F1:F2')

c = ws0.cell(column=7, row=1, value='freq-hmm')
c.alignment = center_aln
c.font = bold_font
ws0.merge_cells('G1:G2')

ws0.column_dimensions['B'].width = 11
ws0.column_dimensions['C'].width = 11
ws0.column_dimensions['D'].width = 12
ws0.column_dimensions['E'].width = 12
ws0.column_dimensions['F'].width = 12
ws0.column_dimensions['G'].width = 12

idx = 0
for k in sorted(cand):
    meths = cand[k]
    if len(meths) > 1:
        ws0.row_dimensions[idx+3].height = 20
        c1 = ws0.cell(column=1, row=idx+3, value=k[0])
        c2 = ws0.cell(column=2, row=idx+3, value=k[1])
        c2.number_format = '0,0'
        c3 = ws0.cell(column=3, row=idx+3, value='+' if 'BP1' in meths else '-')
        c4 = ws0.cell(column=4, row=idx+3, value='+' if 'BP2' in meths else '-')
        c5 = ws0.cell(column=5, row=idx+3, value='+' if 'GW' in meths else '-')
        c6 = ws0.cell(column=6, row=idx+3, value='+' if 'EHH' in meths else '-')
        c7 = ws0.cell(column=7, row=idx+3, value='+' if 'HMM' in meths else '-')

        for c in [c3, c4, c5, c6, c7]:
            c.alignment = center_aln
            c.font = large_font

        idx += 1


print('writing file')
wb.save("Supplementary_table_6.xlsx")
print('done')
