"""
Make plots of statistics computed with compute-stats.py (process all
directories matching pattern "stats-windows-wsize*-wstep*").

PATH: work directory.
"""

from matplotlib import pyplot, rcParams
import sys, glob, re, math, os
sys.path.append('../data')
import lib

PATH = os.path.expanduser('~/WORK1/pop-genomics-work')
LGs = ['LG{0:0>2}'.format(i+1) for i in range(18)]
OPTS = {'marker': 'o', 'markersize': 1, 'ls': 'None'}

a, b = rcParams['figure.figsize']
rcParams['font.size'] *= 2.5

# function to get statistics from a file
def getter(fname):
    with open(fname) as f:
        header = f.readline().split()
        stats = {k:[] for k in header}
        stats['mid'] = []
        #c = 0
        for line in f:
            #if c < 100: # THINNING
            #    c += 1
            #    continue
            #else:
            #    c = 0
            line = line.split()
            assert len(line) == len(header)
            for k, v in zip(header, line):
                if v == 'None': 
                    v = None
                elif k == 'FistWC':
                    v = [None if i=='None' else float(i) for i in v.split(',')]
                    assert len(v) == 3
                elif k == 'FisctWC':
                    v = [None if i=='None' else float(i) for i in v.split(',')]
                    assert len(v) == 4
                else:
                    try: v = int(v)
                    except ValueError: v = float(v)
                stats[k].append(v)
            # start: 1-based, first position
            # end: 1-based, last position (not stop)
            stats['mid'].append( int((stats['start'][-1]+stats['stop'][-1])/2) )
    return stats

# process all folders
for path in glob.glob(PATH + '/stats-windows-wsize*-wstep*'):
    WSIZE, WSTEP = map(int, re.match(PATH + '/stats-windows-wsize(\d+)-wstep(\d+)', path).groups())
    DST = 'plots-wsize{0}-wstep{1}/'.format(WSIZE, WSTEP)
    if not os.path.isdir(DST):
        os.mkdir(DST)

    # import all data
    data = {}
    for fname in sorted(glob.glob(path + '/*-*.txt')):
        LG, sample = re.match(PATH + '/stats-windows-wsize{0}-wstep{1}/(.+)-(.+)\.txt'.format(WSIZE, WSTEP), fname).groups()
        print(WSIZE, WSTEP, LG, sample)
        stats = getter(fname)
        if LG not in data: data[LG] = {}
        data[LG][sample] = stats

    # each chromosome
    FIG, AXES = pyplot.subplots(12, 1, figsize=(60, 30))
    TICKS = []
    offset = 0
    for LG in LGs:
        print(WSIZE, WSTEP, LG)

        # get theta_2 per site
        Xsite = []
        Ysite = []
        with open(os.path.join(PATH, 'stats-sites', LG + '.txt')) as f:
            h = f.readline().split()
            idx = h.index('theta_viravr')
            for line in f:
                bits = line.split()
                assert len(bits) == len(h)
                Xsite.append(int(bits[0]) + offset)
                Ysite.append(float(bits[idx]))

        fig, axes = pyplot.subplots(3, 1)
        axes[0].plot(data[LG]['total']['mid'], data[LG]['total']['thetaW'], c='0.2', **OPTS)
        axes[1].plot(data[LG]['total']['mid'], data[LG]['total']['D'], c='0.2', **OPTS)
        axes[2].plot(data[LG]['total']['mid'], data[LG]['vir7avr7']['WC1_t'], c='0.2', **OPTS)
        axes[0].set_title(r'$\hat{\theta}_W$',rotation='horizontal')
        axes[1].set_title(r'$D$',rotation='horizontal')
        axes[2].set_title(r'$F_{ST}$ (Vir7)',rotation='horizontal')
        [ax.set_xticks([]) for ax in axes]
        ticks = [i*0.5 for i in range(math.ceil(lib.LGs[LG]/5e5)+1)]
        axes[-1].set_xticks([i*1e6 for i in ticks])
        axes[-1].set_xticklabels(ticks)
        axes[-1].set_xlabel('position (Mbp)')
        [ax.set_xlim(0, ticks[-1]*1e6) for ax in axes]
        fig.savefig(DST + LG + '.png')
        pyplot.close(fig)
        X = [offset+x for x in data[LG]['total']['mid']]
        col = ['0.0', '0.5'][LGs.index(LG) % 2]
        AXES[0].plot(X, data[LG]['total']['thetaW'], **OPTS, mec=col)
        AXES[1].plot(X, data[LG]['total']['D'], **OPTS, mec=col)
        AXES[2].plot(X, data[LG]['1993']['D'], **OPTS, mec=col)
        AXES[3].plot(X, data[LG]['1994']['D'], **OPTS, mec=col)
        AXES[4].plot(X, data[LG]['1998']['D'], **OPTS, mec=col)
        AXES[5].plot(X, data[LG]['2008']['D'], **OPTS, mec=col)
        AXES[6].plot(Xsite, Ysite, **OPTS, mec=col)
        AXES[7].plot(X, data[LG]['hierarchical']['WC3_t2'], **OPTS, mec=col)
        AXES[8].plot(X, data[LG]['vir7avr7']['WC2_t'], **OPTS, mec=col)
        AXES[9].plot(X, data[LG]['vir7avr7']['Dj'], **OPTS, mec=col)
        AXES[10].plot(X, data[LG]['vir7avr7']['Da'], **OPTS, mec=col)
        AXES[11].plot(X, data[LG]['vir7avr7']['Gste'], **OPTS, mec=col)
        TICKS.append((2 * offset + lib.LGs[LG])/2)
        offset += lib.LGs[LG]
    [AX.set_xticks([]) for AX in AXES]
    AXES[-1].xaxis.set_tick_params(length=0)
    AXES[-1].set_xticks(TICKS)
    AXES[-1].set_xticklabels(['chr' + i[2:] for i in LGs])
    AXES[0].set_ylabel('$\hat{\\theta}_W$')
    AXES[0].set_ylim(0, 0.01)
    AXES[1].set_ylabel('$D$')
    AXES[2].set_ylabel('$D$\nAvr1993')
    AXES[3].set_ylabel('$D$\nVir1994')
    AXES[4].set_ylabel('$D$\nVir1998')
    AXES[5].set_ylabel('$D$\nWild2008')
    AXES[6].set_ylabel('$\hat{\\theta}_2$\nper site')
    AXES[6].set_ylim(AXES[6].get_ylim()[0], 1)
    AXES[7].set_ylabel('$\hat{\\theta}_2$')
    AXES[7].set_ylim(AXES[7].get_ylim()[0], 1)
    AXES[8].set_ylabel('$\hat{\\theta}$\nvir vs. avr')
    AXES[8].set_ylim(AXES[8].get_ylim()[0], 1)
    AXES[9].set_ylabel('Jost\'s $D$\nvir vs. avr')
    AXES[9].set_ylim(AXES[9].get_ylim()[0], 1)
    AXES[10].set_ylabel('$D_a$')
    AXES[10].set_ylim(AXES[10].get_ylim()[0], 1)
    AXES[11].set_ylabel('$G_{ST}\'$')
    AXES[11].set_ylim(AXES[11].get_ylim()[0], 1)
    for ax in AXES:
        ax.set_xlim(0, sum(lib.LGs.values()))
        ax.tick_params(axis='y', length=10, left=True, right=True)
    FIG.savefig(DST + 'ALL.png', bbox_inches='tight')
