"""
Compute statistics over windows across the genome.
Windows are defined by a fixed number of bp over the reference.
Unused stretch of reference is spread (almost) equally at both ends.
Skip windows with too many sites

MAX_MISSING: maximum proportion of missing data.
MAX_THREADS: number of parallelization threads.
PATH: work directory.

Command line arguments: <window size> <window step>.
"""

import egglib, sys, multiprocessing, time, random, os
sys.path.append('../data')
import lib

MAX_MISSING = 0.10
MAX_THREADS = 40
PATH = os.path.expanduser('WORK1/pop-genomics-work')


if len(sys.argv) != 3:
    sys.exit('usage: python {0} <WSIZE> <WSTEP>'.format(sys.argv[0]))

WSIZE, WSTEP = map(int, sys.argv[1:])

# define list of stats for a given level of diversity
list_stats = {
    'total':    ['S', 'D', 'D*', 'F*', 'Pi', 'Ss', 'thetaW',
                 'Dj', 'FistWC', 'FstWC', 'Gst', 'Gste', 'Hst', 'nseff', 'lseff', 'Fis', 'Dj'],
    'intra':    ['S', 'D', 'D*', 'F*', 'Pi', 'Ss', 'thetaW',
                 'Fis', 'nseff', 'lseff'],
    'hier':     ['FisctWC', 'nseff', 'lseff'],
    'pairwise': ['FistWC', 'FstWC', 'Gst', 'Gste', 'Hst', 'Da', 'Dxy', 'Dj', 'nseff', 'lseff']
}

list_stats_output = {
    'total':    ['S', 'D', 'D*', 'F*', 'Pi', 'Ss', 'thetaW',
                 'Dj', 'WC2_f', 'WC2_t', 'WC2_F', 'WC1_t', 'Gst', 'Gste', 'Hst', 'nseff', 'lseff', 'Fis', 'Dj'],
    'intra':    ['S', 'D', 'D*', 'F*', 'Pi', 'Ss', 'thetaW',
                 'Fis', 'nseff', 'lseff'],
    'hier':     ['WC3_f', 'WC3_t1', 'WC3_t2', 'WC3_F', 'nseff', 'lseff'],
    'pairwise': ['WC2_f', 'WC2_t', 'WC2_F', 'WC1_t', 'Gst', 'Gste', 'Hst', 'Da', 'Dxy', 'Dj', 'nseff', 'lseff']
}


# define structure objects
structs = [
    ('total', lib.dict_structs['4pop'],  'total'),
    ('hierarchical',  lib.dict_structs['hier'],  'hier'),
    ('1993',  lib.dict_structs['1993'],  'intra'),
    ('1994',  lib.dict_structs['1994'],  'intra'),
    ('1998',  lib.dict_structs['1998'],  'intra'),
    ('2008',  lib.dict_structs['2008'],  'intra'),
    ('1993v2008', lib.dict_structs['1993v2008'], 'pairwise'),
    ('1993v1994', lib.dict_structs['1993v1994'], 'pairwise'),
    ('1993v1998', lib.dict_structs['1993v1998'], 'pairwise'),
    ('1994v2008', lib.dict_structs['1994v2008'], 'pairwise'),
    ('1994v1998', lib.dict_structs['1994v1998'], 'pairwise'),
    ('1998v2008', lib.dict_structs['1998v2008'], 'pairwise'),
    ('vir7avr7',  lib.dict_structs['vir7avr7'],  'pairwise'),
    ('vir7',  lib.dict_structs['vir7'],  'intra'),
    ('avr7',  lib.dict_structs['avr7'],  'intra')]

# slide for a contig
def process(LGi):
    LG = 'LG{0:0>2}'.format(LGi+1)

    # make one compute stats for type of level of diversity
    dict_cs = {}
    for k, stats in list_stats.items():
        cs = egglib.stats.ComputeStats()
        cs.add_stats(*stats)
        dict_cs[k] = cs

    # define list of windows
    ls = lib.LGs[LG] # length of chrososome
    starts = [] # create list of start of windows
    pos = 0
    while True:
        if pos+WSIZE > ls: break # don't include truncated window
        starts.append(pos)
        pos += WSTEP
    leftover = ls-starts[-1]+WSIZE # unused data at the end of chromosome
    offset = leftover//2
    bounds = [[i+offset, i+offset+WSIZE, None] for i in starts]

    # open file
    with open(PATH + '/filter-sites/{0}.txt'.format(LG)) as f:

        # open output files (there might be a way to do that in a dynamic multiple context manager...)
        outf = {}
        for label, struct, cat in structs:
            outf[label] = open(PATH + '/stats-windows-wsize{0}-wstep{1}/{2}-{3}.txt'.format(WSIZE, WSTEP, LG, label), 'w')
            outf[label].write('start stop ls ' + ' '.join(list_stats_output[cat]) + '\n')

        f.seek(0, 2)
        END = f.tell()
        f.seek(0)
        f.readline() # header

        # locate first site of all windows
        curwin = 0
        while curwin < len(bounds):
            line = f.readline()
            if line == '': break
            pos, *trash = line.split()
            pos = int(pos) - 1
            if pos < bounds[curwin][0]:
                pass
            elif pos < bounds[curwin][1]:
                bounds[curwin][2] = f.tell()
                curwin += 1
            else:
                curwin += 1
        f.seek(0)

        # process each window
        for cnt, (start, stop, index) in enumerate(bounds):
            if cnt %100 == 0:
                print('{0} {1:.2f}% {2}-{3}'.format(LG, start/lib.LGs[LG]*100, WSIZE, WSTEP))

            # empty window
            if index is None:
                for label, struct, cat in structs:
                    outf[label].write('{0} {1} 0 {2}\n'.format(start+1,
                                                             stop,
                                ' '.join(['None' for i in list_stats_output[cat]])))

            # window has at least 1 (maybe fixed) site
            else:

                # load sites
                ls = 0 # total number of sites (including fixed)
                window = []
                f.seek(index)
                while f.tell() != END:
                    pos, cat, *site = f.readline().split()
                    pos = int(pos) - 1
                    if pos >= stop: break
                    ls += 1
                    if cat == 'F': continue
                    window.append(egglib.site_from_list(''.join(site), alphabet=egglib.alphabets.DNA))

                # compute stats
                for label, struct, cat in structs:
                    dict_cs[cat].configure(struct=struct)
                    stats = dict_cs[cat].process_sites(window)
                    if 'thetaW' in stats and stats['thetaW'] is not None:
                        stats['thetaW'] /= ls
                        stats['Pi'] /= ls
                    if 'FstWC' in stats:
                        stats['WC1_t'] = stats['FstWC']
                        del stats['FstWC']
                    if 'FistWC' in stats:
                        if stats['FistWC'] is None:
                            stats['WC2_f'] = None
                            stats['WC2_t'] = None
                            stats['WC2_F'] = None
                        else:
                            stats['WC2_f'], stats['WC2_t'], stats['WC2_F'] = stats['FistWC']
                        del stats['FistWC']
                    if 'FisctWC' in stats:
                        if stats['FisctWC'] is None:
                            stats['WC3_f'] = None
                            stats['WC3_t1'] = None
                            stats['WC3_t2'] = None
                            stats['WC3_F'] = None
                        else:
                            stats['WC3_f'], stats['WC3_t1'], stats['WC3_t2'], stats['WC3_F'] = stats['FisctWC']
                        del stats['FisctWC']
                       
                    outf[label].write('{0} {1} {2} {3}\n'.format(start+1,
                                                             stop, ls,
                                ' '.join([str(stats[i]) for i in list_stats_output[cat]])))

        for i in outf.values():
            i.close()

# start processes
print('running on {0} CPUs'.format(MAX_THREADS))
wait = []
running = []
done = 0
for i in range(18):
    p = multiprocessing.Process(target=process, args=[i])
    wait.append(p)
random.shuffle(wait)

# run
while done < 18:
    i = 0
    while i < len(running):
        if not running[i].is_alive():
            del running[i]
            done += 1
        else:
            i += 1
    while len(running) < MAX_THREADS and len(wait) > 0:
        running.append(wait.pop())
        running[-1].start()
