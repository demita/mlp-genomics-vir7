convert plots/LD-1993-zoom-0.png  -bordercolor White -border 50x50 \
    -draw "scale 3,3 text 20,20 'A'" \
    -draw "scale 3,3 text 105,30 'Avr1993'"   A.png
convert plots/LD-1994-zoom-0.png  -bordercolor White -border 50x50 \
    -draw "scale 3,3 text 20,20 'B'" \
    -draw "scale 3,3 text 105,30 'Vir1994'"   B.png
convert plots/LD-1998-zoom-0.png  -bordercolor White -border 50x50 \
    -draw "scale 3,3 text 20,20 'C'" \
    -draw "scale 3,3 text 105,30 'Vir1998'"   C.png
convert plots/LD-2008-zoom-0.png  -bordercolor White -border 50x50 \
    -draw "scale 3,3 text 20,20 'D'" \
    -draw "scale 3,3 text 105,30 'Wild2008'"   D.png
montage -tile 2x -geometry +4+4 A.png B.png C.png D.png "a.png"
mv a.png Supplementary_figure_2.png
rm A.png B.png C.png D.png
