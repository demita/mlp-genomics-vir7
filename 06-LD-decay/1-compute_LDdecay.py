"""
Compute pairwise LD between all pairs of sites (belonging to the same
chromosome). The data are saved in the work directory.

Warning: this script runs automatically on 18 threads.

NS: number of isolates (must match the data in lib).
MAX_DIST: maximal distance between two sites.
MAX_MAJ: maximal frequency of the majority genotype
MIN_NON_MISSING: minimal proportion of non-missing genotypes.
PATH: path of work directory.
"""

import egglib, multiprocessing, sys
sys.path.append('../data')
import lib

NS = 76
MAX_DIST = 1000000
MAX_MAJ = 0.75
MIN_NON_MISSING = 0.9
PATH = '/home/user/WORK/pop-genomics-work'

alphabet = egglib.Alphabet('string', ['AA', 'AC', 'AG', 'AT', 'CC', 'CG', 'CT', 'GG', 'GT', 'TT'], ['??'])

# make structures
structs = lib.structs_indiv
frq = egglib.stats.Freq()

# main functions
def process(LGi, queue):
    LG = 'LG{0:0>2}'.format(LGi+1)
    queue.put((LGi, '  .....'))

    # getting all sites
    positions = []
    sites = []
    with open(PATH + '/filter-sites/{0}.txt'.format(LG)) as f:
        header = f.readline().split()
        assert header[0] == 'pos'
        c = 0
        for line in f:
            pos, cat, *site = line.split()
            if cat == 'F': continue
            pos = int(pos)-1
            site = [''.join(sorted(i)) for i in site]
            sites.append(egglib.site_from_list(site, alphabet=alphabet))
            positions.append(pos)
            c += 1
            if c % 10000:
                queue.put((LGi, '{0:.2f}Mbp'.format(pos/1e6).rjust(7)))

    with open(PATH + '/LD-decay/{0}-0000.txt'.format(LG), 'w') as f1:
        with open(PATH + '/LD-decay/{0}-1993.txt'.format(LG), 'w') as f2:
            with open(PATH + '/LD-decay/{0}-1994.txt'.format(LG), 'w') as f3:
                with open(PATH + '/LD-decay/{0}-1998.txt'.format(LG), 'w') as f4:
                    with open(PATH + '/LD-decay/{0}-2008.txt'.format(LG), 'w') as f5:
                        files = f1, f2, f3, f4, f5

                        # computing all pairs
                        num = int(len(sites) * (len(sites)-1) / 2)
                        cur = 0
                        c = 0.0

                        for i in range(len(sites)):
                            for j in range(i+1, len(sites)):
                                c += 1
                                if int(c/num*10000) != cur:
                                    cur = int(c/num*10000)
                                    queue.put((LGi, '{0:.2f}%'.format(c/num*100).rjust(7)))
                                p1 = positions[i]
                                p2 = positions[j]
                                if p2-p1 > MAX_DIST: break
                                site1 = sites[i]
                                site2 = sites[j]
                                for idx, struct in enumerate(structs):
                                    frq.from_site(site1, struct=struct)
                                    if frq.num_alleles == 1: continue
                                    if max([frq.freq_allele(k)/struct.ns for k in range(frq.num_alleles)]) > MAX_MAJ: continue
                                    if frq.nseff()/struct.ns < MIN_NON_MISSING: continue
                                    frq.from_site(site2, struct=struct)
                                    if frq.num_alleles == 1: continue
                                    if max([frq.freq_allele(k)/struct.ns for k in range(frq.num_alleles)]) > MAX_MAJ: continue
                                    if frq.nseff()/struct.ns < MIN_NON_MISSING: continue
                                    rsq = egglib.stats.pairwise_LD(locus1=site1, locus2=site2)["rsq"]
                                    if rsq is not None:
                                        files[idx].write('{0} {1}\n'.format(p2-p1, rsq))

    queue.put((LGi, '   100%'.format(cur)))
    queue.put(None)
queue = multiprocessing.Queue()

# start processes
proc = []
for i in range(18):
    p = multiprocessing.Process(target=process, args=[i, queue])
    p.start()
    proc.append(p)

print(' '.join(['   LG{0:0>2}'.format(i+1) for i in range(18)]))
current = ['xxxxxxx'] * 18
print(' '.join(current), sep='', end='', flush=True)
ln = len(' '.join(current))
ndone = 0

while True:
    val = queue.get()
    if val is None:
        ndone += 1
        if ndone == 18:
            break
    else:
        current[val[0]] = val[1]
    print('\b'*ln, ' '.join(current), sep='', end='', flush=True)
print('\n\n', end='')

# terminate
for p in proc:
    p.join()


