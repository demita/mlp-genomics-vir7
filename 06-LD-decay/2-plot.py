"""
Perform LD decay plot based on the results of 1-compute_LDdecay.py and
generate graphics in the plot directory.

For each of the four samples (1993, 1994, 1998, and 2008) plus the total
(0000), three plots are generated at three levels of zooming.

STEP: size of binning windows.
PATH: path to work directory.
"""

import gzip, glob, os, time, multiprocessing, sys
from matplotlib import pyplot, rcParams
rcParams['font.size'] = 14
STEP = 1000
PATH = os.path.expanduser('~/disks/backup2/work/pop-genomics-work')

# define gradient of colours
colors = [
    (0.475, 0.525, '0.0'),
    (0.45, 0.55, '0.1'),
    (0.40, 0.60, '0.2'),
    (0.35, 0.65, '0.3'),
    (0.30, 0.70, '0.4'),
    (0.25, 0.75, '0.5'),
    (0.20, 0.80, '0.6'),
    (0.15, 0.85, '0.7'),
    (0.10, 0.90, '0.8'),
    (0.05, 0.95, '0.9')
]

bins = [0.5]
for a, b, c in colors: bins += (a,b)

# define bin limits
catlim = []
catmid = []
step=STEP
cur = 0
X = 1000000
while cur < X:
    for i in range(10):
        catlim.append(cur+step)
        catmid.append((cur+step/2)/1000.0)
        cur += step
        if cur >= X: break
    step *= 1.5

boost_mapping = {}
i = 0
for idx, lim in enumerate(catlim):
    while i <= lim:
        boost_mapping[i] = idx
        i += 1

# main function
def process(idxsam, sam):
    catfreq = [[] for i in catlim]
    fnames = glob.glob(PATH + '/LD-decay/LG*-{0}.txt'.format(sam))
    fnames.sort()
    assert len(fnames) > 0
    for fname in fnames:
        ctg = os.path.basename(fname).split('-')[0]
        print('{1} {0}'.format(sam, ctg))
        with open(fname) as f:
            c = 0
            for line in f:
                d, r2 = line.split()
                if r2 == 'None': continue
                d = int(d)
                r2 = float(r2)
                catfreq[boost_mapping[d]].append(r2)
                c += 1
    print('{} end processing'.format(sam))

    curves = {}
    for bn in bins: curves[bn] = []

    for i, array in enumerate(catfreq):
        array.sort()
        n = len(array)
        for bn in bins:
            idx = min(n-1, int(round(bn * n)))
            curves[bn].append(array[idx])

    for a, b, c in colors[::-1]:
        pyplot.fill_between(catmid, curves[a], curves[b], color=c)  # 95%
    pyplot.plot(catmid, curves[0.5], 'w-')
    pyplot.xlabel('distance (Kbp)').set_size(16)
    pyplot.ylabel(r'$r^2$').set_size(20)
    pyplot.xlim(catmid[0], catmid[-1])
    pyplot.ylim(0, 1)
    for a, b, c in colors:
        pyplot.plot([], [], 'ws', mfc=c, mec='None', ms=15, label='{}%'.format(int(round(100*(b-a)))))
    if sam == None:
        legend = pyplot.legend(numpoints=1)
        legend.texts[0].set_size(15)
    pyplot.savefig('plots/LD-{}-zoom-0.png'.format(sam))
    if sam == None:
        legend.set_visible(2**2 == 5)
    pyplot.xlim(catmid[0], 200)
    pyplot.ylim(0, 1)
    pyplot.savefig('plots/LD-{}-zoom-200.png'.format(sam))
    pyplot.xlim(catmid[0], 100)
    pyplot.ylim(0, 1)
    pyplot.savefig('plots/LD-{}-zoom-100.png'.format(sam))
    pyplot.clf()

# main script
samples = '0000', '2008', '1993', '1994', '1998'
for idx, sam in enumerate(samples):
    process(idx+1, sam)
