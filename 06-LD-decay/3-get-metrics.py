"""
This script computes some statistics to compare LD decay between
samples. It requires that 1-compute_LDdecay.py has been run and
generates the metrics.txt file.

STEP: size of binning windows.
PATH: path to work directory.
THRESHOLDS: list of r2 thresholds to monitor LD decay.
POINTS: list of distance points to monitor LD decay.
"""

import os, glob

# parameters
STEP = 1000
PATH = os.path.expanduser('~/disks/backup2/work/pop-genomics-work')
THRESHOLDS = 0.60, 0.50, 0.40, 0.30, 0.20, 0.10
POINTS = 10000, 25000, 50000, 75000, 100000, 250000, 500000

# define list of bin limits
lims = [STEP+i for i in range(0, 500000, STEP)]
boost_mapping = {i+1: idx for (idx, lim) in enumerate(lims) for i in range(lim-STEP, lim)}

# get stats
stats = []
for sam in '1993', '1994', '1998', '2008':
    fnames = glob.glob(PATH + '/LD-decay/LG*-{0}.txt'.format(sam))
    fnames.sort()
    sum_r2 = [[0,0] for lim in lims]
    assert len(fnames) > 0
    for fname in fnames:
        c = 0
        with open(fname) as f:
            print(os.path.basename(fname))
            for line in f:
                d, r2 = line.split()
                if r2 == 'None': continue
                d = int(d)
                r2 = float(r2)
                assert d > 0
                i = boost_mapping.get(d)
                if i is not None:
                    sum_r2[i][0] += 1
                    sum_r2[i][1] += r2
                    c += 1
                    if c%1000000 == 0: print(os.path.basename(fname), c)

    # compute metrics
    decay = {i: None for i in THRESHOLDS}
    values = {i: None for i in POINTS}
    for lim, (n, tot) in zip(lims, sum_r2):
        r2 = tot/n # average r2
        for i in THRESHOLDS:
            if r2 < i and decay[i] is None:
                decay[i] = int(lim-STEP/2)
        if lim in POINTS:
            assert values[lim] is None
            values[lim] = r2
    stats.append((decay, values))

# write file
with open('stats.txt', 'w') as f:
    f.write('stat 1993 1994 1998 2008\n')
    for thre in THRESHOLDS:
        f.write('dist(r^2<{0})'.format(thre))
        for dec, val in stats:
            f.write(' {0}'.format(dec[thre]))
        f.write('\n')
    for pt in POINTS:
        f.write('decay(dist={0})'.format(pt))
        for dec, val in stats:
            f.write(' {0:.4f}'.format(val[pt]))
        f.write('\n')


