"""
Select sites based on depth filters and a minimum of non-missing data.

PATH: work directory.
THRESHOLD: genotype calling threshold (must be already available).
MINI: minimal individual depth.
MAXI: maximal individual depth.
MIN_PER_POP: minimal number of non-missing individuals per sample.
"""

import multiprocessing, sys
sys.path.append('../data')
import lib

PATH = '/home/user/WORK/pop-genomics-work'
THRESHOLD = 30
MINI = 20
MAXI = 200
MIN_PER_POP = 10

pops = [[lib.samples.index(i) for i in pop] for pop in lib.pops.values()]

def process(LGi, queue):
    LG = 'LG{0:0>2}'.format(LGi+1)
    fname = PATH + '/vcf/{0}.vcf'.format(LG)

    n1 = 0
    n2 = 0
    n3 = 0
    with open(PATH + '/raw-sites/THRESHOLD{0}-SNP-{1}.txt'.format(THRESHOLD, LG)) as f:
        with open(PATH + '/filter-sites/{0}.txt'.format(LG), 'w') as outf:
            header = ' '.join(f.readline().split()[1:])
            outf.write('pos type ' + header + '\n')
            for line in f:
                n1 += 1
                pos, site = line.split()
                pos = int(pos) - 1
                if n1%100000 == 0:
                    print('{0}:{1:.2f}Mbp'.format(LG, pos/1e6))
                site = [i.split(':') for i in site.split(',')]
                site = ['?' if int(i) < MINI or int(i) > MAXI else j for (i,j) in site]
                num = [len([site[i] for i in pop if site[i] != '?']) for pop in pops]
                if min(num) < MIN_PER_POP:
                    continue
                n2 += 1

                site = ['??' if i=='?' else (i[0]+i[2]) for i in site]
                na = len(set(''.join(site)) - set('?'))
                if na == 1:
                    outf.write('{0} F {1}\n'.format(pos+1, ' '.join(site)))
                elif na == 2:
                    n3 += 1
                    outf.write('{0} V {1}\n'.format(pos+1, ' '.join(site)))
                else:
                    raise AssertionError

    print('{0} {1}/{2} -> {3}'.format(LG, n2, n1, n3))
    queue.put((LG, n1, n2, n3))

queue = multiprocessing.Queue()

proc = []
for i in range(18):
    p = multiprocessing.Process(target=process, args=[i, queue])
    p.start()
    proc.append(p)

for p in proc:
    p.join()

counts = {}
for i in range(18):
    LG, *val = queue.get()
    counts[LG] = val

with open('results.txt', 'w') as f:
    n1 = 0
    n2 = 0
    n3 = 0
    for k in sorted(counts):
        f.write('{0} {1[0]} {1[1]} {1[2]}\n'.format(k, counts[k]))
        n1 += counts[k][0]
        n2 += counts[k][1]
        n3 += counts[k][2]
    f.write('total {0} {1} {2}\n'.format(n1, n2, n3))
