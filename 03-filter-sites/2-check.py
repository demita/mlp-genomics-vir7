"""
Check filter-sites files so that all positions are consecutive and all
sites have 76 genotypes.

PATH: work directory.
"""

PATH = '/home/user/WORK/pop-genomics-work'

import os

c1 = 0
c2 = 0
c3 = 0

for i in range(18):
    with open(os.path.expanduser(os.path.join(PATH, '/filter-sites/LG{0:0>2}.txt'.format(i+1)))) as f:
        bits = f.readline().split()
        assert bits[0] == 'pos'
        assert bits[1] == 'type'
        assert len(bits[2:]) == 76
        cur_pos = 0
        cur_status = 'N'
        previous_duplicated = False
        for line in f:
            pos, status, *site = line.split()
            assert len(site) == 76
            pos = int(pos)
            if pos == cur_pos:
                assert previous_duplicated == False
                c1 += 1
                if cur_status == 'V': c2 += 1
                if status == 'V': c3 += 1
                previous_duplicated = True
            else:
                previous_duplicated = False
            cur_pos = pos
            cur_status = status

            if pos%100000 == 0:
                print('LG{0:0>2}'.format(i+1), pos/1e6, 'Mbp', c1, c2, c3)


print('total:')
print('   duplicated positions:', c1)
print('   first being variable:', c2)
print('   second being variable:', c3)
