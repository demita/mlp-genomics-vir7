=============================================================================================
Pipeline analysing the genomic consequences of the R7 breakdown in Melamspora larici-populina
=============================================================================================

Principle
=========

This package contains scripts used to perform most of the analyses in
Persoons *at al.* "Genetic Determinant of a Major Event of Adaptation in
the Pathogenic Fungus *Melampsora larici-populina*", as well as (final)
output files.

Reuse of scripts in academic work is welcome. For enquiries, please
contact Stéphane De Mita <demita@gmail.com>.

The package is organized in numbered sections as listed below.

To save disk space within the project directory, the source files and
large intermediate files are placed in a working directory and are not
included in the package. The path to the work directory needs to be
specified in the header of most scripts. The work directory must also
have the appropriate structure (that is, contain the needed directories)
as specified in the requirements list.

The sections are meant to be executed in the specified order, but not
all are required. The third section generates the actual dataset on
which the rest is based. It itself requires the first section to
generate the preliminary dataset. To run the first script, one
uncompressed VCF file per chromosome containing all samples must be
present in the ``vcf`` subdirectory of the work directory.

All scripts requires python 3 except a few bash scripts. All plotting
script required ``matplotlib``. A short summary is present at the
beginning of all scripts, along with additional requirements.

Requirements
============

* Python 3 with the modules matplotlib, EggLib, and openpyxl.
* Python 2 with the modules numpy and scipy (for freq-hmm).
* R with the package adegenet.
* PHYLIP package with the neigbhor program present in the path.
* BayeScan.
* BayPASS.
* Selestim.
* freq-hmm.
* makeblastdb, blastn, and tblastx programs of the BLAST suite
  configured through EggLib.
* Melampsora larici-populina reference genome.
* For GWAS: emma, emmax, and mlmm R scripts.
* For making the figure for DAPC result: ImageMagick system commands
  convert and montage.
* a LaTeX implementation for generating Additional files.

The work directory must contain the following subdirectories:

* ``vcf`` with uncompressed VCF files (one per chromosome).
* ``raw-sites``.
* ``filter-sites``.
* ``LD-decay``.
* ``stats-sites``.
* ``stats-windows-wsize<WSIZE>-wstep<WSTEP>`` (depending on arguments
  passed to ``08-stats-per-window/compute-stats.py``).
* ``LD-pairwise``.
* ``EHH``.
* ``depths``.
* ``blast``.

Sections
========

data
----

This directory contains a single module providing other scripts with
static information (size of each chromosome, sample structure). Data
files are present in the directory. It is needed by many of the scripts
in the other sections.

01-get-snp
----------

This section contains the script ``indexalign.job`` which is not
actually part of the pipeline. It is provided as a reference of the
command lines used to perform read mapping.

The first script of the pipeline is ``get-sites.py`` which extracts
diallelic SNPs and indels. The script ``save-depths.py`` generates file
with the average depth for each diallelic SNP.


02-correl-heterozygosity
------------------------

This section performs an analysis of detected genotypes with the aim of
a rough estimate of the falsely detected polymorphisms. We expect that a
part of the false polymorphisms are caused by sequencing errors,
possibly when the sequencing depth is very low and that another part are
caused by repeated DNA which causes erroneous mapping. The latter is
expected to occur in regions tagged as repeated DNA (masked regions in
the genome assmebly) and exhibiting high sequencing depth.

The scripts of this section use the ``raw-sites`` SNPs to correlate
polymorphism and (1) sequencing depth and (2) repeated DNA in order to
define valid parameters for final filtering.

``depth/cp-correl-depths.py`` generates a data table to monitor the
effect of depth on uncalled, fixed, and variable sites and on
heterozygote genotypes. ``depth/plot.py`` generates the plots.

``repeats/cp-correl-repeat.py`` generates a data table to plot the
distribution of depth in masked and non-masked regions.
``repeats/plot.py`` generates the plots.

03-filter-sites
---------------

This section generates the final dataset for subsequent analyses.
``1-filter.py`` processes again all VCF files with more restrictive
parameters and generates a final list of sites (including fixed sites
and sites with maximum 2 alleles).

A second script, ``2-check.py`` checks that the generated data files are
consistent.

04-tree
-------

This section generates a neighbour-joining tree connecting all isolates.
The three numbered scripts must be run in order to generate the tree,
requiring the ``neighbor`` program from the PHYLIP package. In addition
the resulting tree in the png format is present in the directory.

05-structure
------------

This section uses DAPC to analyze the structure in isolates. It contains
two python scripts to generate the input file for DAPC
(``1-generate_genotype.py`` and ``2-convert2structure.py``) and a R
script (``3-dapc.r``) which uses the ``adegenet`` module and should be
run interactively. A number of pdf output file are present in the
directory, as well as csv output files for different values of the
number of clusters (in one case, the csv has been replaced by a
spreadsheet file in ods format because a figure has been generated). In
addition, ``4-probas.py`` generates three bar plots with membership
coefficients. Finally, a shell script (``4-merge_pictures.sh``) combines
R output and the latter plot in a single picture using commands of the
ImageMagick package.

06-LD-decay
-----------

This section monitors LD decay by aggregating data from all chromosomes.

``1-compute_LDdecay.py`` compute r2 between all pair of sites (based on
a few parameters). The other two scripts use these data to generate LD
decay plots (``2-plot.py``) and to compute decay statistics
(``3-get-metrics.py``). There is an additional script
``4-make-picture.sh`` combining the decay picture for the four samples
in a single picture using commands of the ImageMagick package. There is
also a latex source file to genere the corresponding additional file,
combining the decay plot and statistics. Note that this latex file is
not updated automatically in case of update of decay statistics!

07-stats-per-sites
------------------

This section computes site-wise statistics. There are three directories:

* ``afs``: compute the allele frequency spectrum. This directory
  contains the scripts ``spectrum.py`` computing statistics and
  ``plot.py`` which generates the single graphic.

* ``pop-tree``: generate a population tree using two metrics: Weir and
  Cockerham Fst and Jost's D. There are two scripts:
  ``1-cp-distance.py`` (which computes the pairwise distances) and
  ``2-build-tree.py`` (which reconstructs the two trees). The output
  files are placed in the same directory.

* ``stats``: compute a small set of statistics for all sites of the
  genome. As usual there is a script to compute statistics
  (``cp-stats.py``) and a second one to generate the plots
  (``plot-stats.py``). The last script also generates the list of
  candidate SNPs.

08-stats-per-windows
--------------------

This section computes statistics over windows. There is a script to
perform the sliding window (``compute-stats.py``) which takes the
window size and the step as argument. It is necessary to create the
target directory in the work directory before running it. Then
``plot.py`` generates the plots.

09-bayescan
-----------

A script allows to generate the input file and a logfile with locus
coordinate (``bayescan_converter.py``). Then BayeScan is executed
through a shell command (included in the folder). A file with the 
outliers markers at a given FDR value can be obtained with a R script 
(``get_outlier.R``) which use the R function privided with the sofware 
(``plot_R.r``). The Manhattan plot can be realised in a specific folder 
(/plots) by generating the input file (with the script 
``make_input_plot.py``) and then running the plot generator script 
(``plot_proba.py``).

10-LD-pairwise
--------------

There is a series of scripts computing one LD matrix per chromosome.
Scripts number 1 through 3 perform the pairwise LD computations and
generate a plot for all chromosome. Script number 4 generate a legend
file. The other scripts repeat the analysis for a given region. There is
an additional script ``8-make-picture.sh`` combining the chromosome
plots in a single PDF document using commands of the ImageMagick
package.

11-baypass
----------

Like for Bayescan, a script performs the conversion and the program is
run through a shell script (specifying under which model). A file with 
the outliers markers can be obtained with a script (``markers_list.py``)
. The Manhattan plot can be realised in a specific folder (/plots) by 
generating the input file (with the script make_input_plot.py) and then
 running the plot generator script (``plot_xtx.py``).

12-EHH
------

The analysis is done is three steps: perform computations using
``1-compute.py``, which generates files (in work directory) with iEG
values for all samples. Then, run ``2-process.py`` which computes Rsb
ratios (as in Tang, Thornton and Stoneking 2007) and generates a local
file ``data.txt`` as well as files containing lists of candidate SNPs.
Finally and generating plots with ``plot.py``.

13-GWAS
-------

The script generating the input file is available (``convert.py``).
Two R scripts are included allowing to perform the analysis, as well as
some of the output.

14-SelEstim
-----------

A script allows to generate the input file (``map.dat``)and a logfile 
with locus coordinate (``selestim_converter.py``). Then selestim is 
executed through a R script (``run.R``).  A file with the outliers 
markers can be obtained with a script (``markers_list.py``). The 
Manhattan plot can be realised in a specific folder (/plots) by 
generating the input file (with the script make_input_plot.py) and then
 running the plot generator script (``plot_kld.py``).

15-global-stats
---------------

A single script computes overall statistics (incorporating all sites of
the genome). It also computes confidence intervals based on random
subsampling of sites and from windows (based on non-overlapping 5000 bp
windows that must have been computed in section 08-stats-per-windows.
A text file is automatically generated. Besides, a latex source file has
been written to generate Additional file 4 (with list of statistics and
the content of the results files). Note that this latex file is not
updated automatically!

16-freq-hmm
-----------

There is a ``convert.py`` that convert the SNP data to the proper file
format. The data is split per chromosome and saved in the work
directory. Then the analysis is performed by ``wrapper.py`` which has a
system to skip calls to freq-hmm is the output file is already present.
Finally, ``plot.py`` generate the main figure, the figures per
chromosome and a text file with the list of sweeps sorted by chromosome
and per position.

17-candidates
-------------

Create a file with lists of candidate SNPs.

18-fig2
-------

This section contains a script which can generate a figure for all 
chromosomes (or a single chromosome if a command line argument is 
passed) with the value of a few statistics per window  and the results 
of several genome scan analysis. It uses previously generated candidate 
files (only performing an outlier screen of Fct itself) to mark 
candidates. It must be called after all the used analysis have been 
run.

19-fig3
-------

This section generates another figure understood like a zoom of fig3,
with in addition the position of genes and non-synonymous sites. There
are two scripts to be executed in the order: ``annotate.py``,
and ``zoom.py``. The first script generates three files with data on
sites of the specified region (all polymorphic sites, full data of
non-synonymous sites, and full data of sites with a genotype nearly
fixed in 94/98).

20-test-global-stats
--------------------

Coalescent simulations are performed to test statistical significance of
neutrality test and diversity statistics in each of the four samples (as
well as the difference between them). The statistics are computed in a
subset of sites to make it possible to test it with a simple (and more
robust) simulation model.

21-protein
----------

This section only contains a simple script used to extract the sequence
of the candidate protein, and the output of the online Provean run.
