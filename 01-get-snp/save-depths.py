import os, multiprocessing

max_threads = 20
threshold = 30
PATH = os.path.expanduser('~/WORK1/pop-genomics-work')
LGs = [f'LG{i+1:0>2}' for i in range(18)]

def process(LG, q):
    print(f'{LG} starting')
    with open(os.path.join(PATH, f'raw-sites', f'THRESHOLD{threshold}-SNP-{LG}.txt')) as f:
        with open(os.path.join(PATH, 'depths', f'{LG}.txt'), 'w') as outf:
            cur = 0
            pos, *header = f.readline().split()
            assert pos == 'pos'
            assert len(header) == 76
            for line in f:
                pos, dpt = line.split()
                pos = int(pos) - 1
                dpt = dpt.split(',')
                assert len(dpt) == len(header)
                dpt = sum(int(i.split(':')[0]) for i in dpt)/len(header)
                outf.write(f'{pos+1} {dpt}\n')
                pos //= 100000
                if pos != cur:
                    cur = pos
                    print(f'{LG}: {cur/10}Mbp')
    print(f'{LG} done')
    q.put(None)

queue = []
q = multiprocessing.Queue()
for LG in LGs:
    queue.append(multiprocessing.Process(target=process, args=(LG, q)))

for i in range(max_threads):
    if len(queue): queue.pop(0).start()

for i in range(len(LG)):
    q.get()
    if len(queue):
        queue.pop(0).start()
