"""
This script imports data from uncompressed VCF files, call genotypes
based on a given threshold and exports diallelic SNPs and indels in two
different files.

PATH: work directory.
THRESHOLDS: A list of thresholds values.
MAX_THREADS: Number of parellelized threads to use.
"""

import egglib, sys, os, multiprocessing, random
sys.path.append('../data')
import lib

PATH = os.path.expanduser('~/WORK1/pop-genomics-work')
THRESHOLDS = [30] #15, 30, 45
MAX_THREADS = 40

# function to process each chromosome
def process(LGi, THRESHOLD, queue):
    LG = 'LG{0:0>2}'.format(LGi+1)
    print('start {0}:{1}'.format(LG, THRESHOLD))
    fname = PATH + '/vcf/{0}.vcf'.format(LG)
    vcf = egglib.io.VcfParser(fname, threshold_PL=THRESHOLD)
    all_samples = [os.path.basename(vcf.get_sample(i)) for i in range(vcf.num_samples)]
    all_samples = [sample[:-6] for sample in all_samples]
    samples_idx = [all_samples.index(sample) for sample in lib.samples] # should be always the same

    # counters
    nfix = 0
    nsnp = 0
    nindel = 0
    n3 = 0
    n4 = 0

    cur = 0
    with open(PATH + '/raw-sites/THRESHOLD{0}-SNP-{1}.txt'.format(THRESHOLD, LG), 'w') as fsnp:
        with open(PATH + '/raw-sites/THRESHOLD{0}-INDELS-{1}.txt'.format(THRESHOLD, LG), 'w') as findels:
            fsnp.write('pos ' + ' '.join(lib.samples) + '\n')
            findels.write('pos ' + ' '.join(lib.samples) + '\n')

            # each position
            while vcf.good:
                lg, pos, na = vcf.readline()
                assert lg == LG
                var = vcf.get_variant()
                DP = sum([var.samples[idx]['DP'][0] for idx in samples_idx]) # total depth (after filtering)

                if pos//50000 != cur:
                    cur = pos//50000
                    print('{0}/{1}: {2:.2f} / {3:.2f} Mbp'.format(LG, THRESHOLD, pos/1e6, lib.LGs[LG]/1e6))

                # get the list of genotypes
                site = []
                sizes = set()
                alls = set()
                idv_missing = 0
                for i, idx in enumerate(samples_idx):
                    a, b = var.GT[idx]
                    if a is None and b is None:
                        site.append('{0}:?'.format(var.samples[idx]['DP'][0]))
                        idv_missing += 1
                        continue
                    assert a is not None and b is not None
                    sizes.add(len(a))
                    sizes.add(len(b))
                    alls.add(a)
                    alls.add(b)
                    site.append('{0}:{1}/{2}'.format(var.samples[idx]['DP'][0], a, b))

                # export
                if idv_missing < 76: # at least one non-missing data

                    # get allele sizes (from variant header, not actual list of genotypes, because indels might have no polymorphism and would be treated as monomorphic SNPs)
                    alleles = set(var.alleles)
                    alleles.discard('<*>')
                    for i in alleles:
                        assert set(i) <= set('ACGTN'), i
                    alleles_sizes = set(map(len, alleles))

                    if len(alls) and alleles_sizes == {1}: # no indel
                        assert sizes == {1}
                        if len(alls) in [1, 2]:
                            fsnp.write('{0} {1}\n'.format(pos+1, ','.join(site)))
                        if len(alls) == 1: # fixed site
                            nfix += 1
                        elif len(alls) == 2: # SNP
                            nsnp += 1
                        elif len(alls) == 3:
                            n3 += 1
                        elif len(alls) == 4:
                            n4 += 1
                        else:
                            raise AssertionError(
                                'LG: {0} / threshold: {1} / pos 1-based: {2} / alleles: {3}'.format(
                                        LG, THRESHOLD, pos+1, ','.join(alls)))
                    else:
                        if len(alls) > 1:
                            nindel += 1
                            findels.write('{0} {1}\n'.format(pos+1, ','.join(site)))
                        else:
                            pass # ignore non-varying non-single-nucleotide sites
 
    print('finished {0}:{1}'.format(LG, THRESHOLD))
    queue.put(None)

# define list of tasks
queue = multiprocessing.Queue()
tasks = [[ch, thr, queue] for thr in THRESHOLDS for ch in range(18)]
num = len(tasks)

# setup list of processes
wait = [multiprocessing.Process(target=process, args=args) for args in tasks]

# start enough jobs
for i in range(MAX_THREADS):
    if len(wait):
        wait.pop(0).start()

# wait for jobs to complete
done = 0
while done < num:
    queue.get()
    done += 1
    if len(wait) > 0:
        wait.pop(0).start()
    print('{0} to go'.format(num-done))
