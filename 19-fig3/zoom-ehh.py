"""
Exactly like zoom.py, but make a zoom for EHH statistics.
"""

import os, sys, math, matplotlib, re, egglib
from matplotlib import pyplot
import zoom_lib

# parameters
PATH = os.path.expanduser('~/disks/backup2/work/pop-genomics-work')
WSIZE = 1000
WSTEP = 250
EHH_THR = 4
LG = 'LG15'
FIRST = 2100000
LAST = 2300000

# initiate picture
matplotlib.rc('font', size=20)
fig, axes = pyplot.subplots(8, 1, figsize=(40, 30))

# get annotation
sites, nsyn, genes = zoom_lib.get_annotation(LG, FIRST, LAST)

# import values
print('processing EHH...', end='', flush=True)
X = [[], [], [], [], [], [], []]
Y = [[], [], [], [], [], [], []]
with open(os.path.join('..', '12-EHH', 'data.txt')) as f:
    h = f.readline().split()
    for line in f:
        line = line.split()
        assert len(line) == len(h)
        d = dict(zip(h, line))
        if d['LG'] == LG:
            pos = int(d['pos'])
            if pos >= FIRST and pos <= LAST:
                for i, k in enumerate(['1993', '1994', '1998', '2008', 'Rsb1', 'Rsb2', 'Rsb4']):
                    if d[k] != 'NA':
                        X[i].append(pos)
                        Y[i].append(float(d[k]))

# get significant SNPs
candidates_EHH = []
for rsb in 1, 4:
    candidates_EHH.append([])
    with open(os.path.join('..', '12-EHH', 'candidates', 'Rsb{0}_lvl{1}.txt'.format(rsb, EHH_THR))) as f:
        f.readline()
        for line in f:
            lg, pos, iEG, Rsb = line.split()
            if lg == LG:
                pos = int(pos)
                if pos >= FIRST and pos <= LAST:
                    candidates_EHH[-1].append(pos)
print('\b\b\b done')

## PLOT ##
def plot(idx, x, y, lbl, candidates=None):
    d = dict(zip(x, y))
    if candidates is not None:
        x = sorted(set(d) - set(candidates))
        xc = sorted(set(candidates) & set(d))
        y = [d[i] for i in x]
        yc = [d[i] for i in xc]
    else:
        xc = []
        yc = []
        xcn = []
        ycn = []

    axes[idx].plot(x, y, marker='o', ms=10, ls='None', mec='k', mfc='None')
    axes[idx].plot(xc, yc, marker='o', ms=10, ls='None', mec='r', mfc='None')
    axes[idx].set_ylabel(lbl)

plot(0, X[0], [i for i in Y[0]], 'iEG Avr1993')
plot(1, X[1], [i for i in Y[1]], 'iEG Vir1994', candidates_EHH[0])
plot(2, X[2], [i for i in Y[2]], 'iEG Vir1998', candidates_EHH[1])
plot(3, X[3], [i for i in Y[3]], 'iEG Wild2008')
plot(4, X[4], Y[4], 'lnRsb\nVir1994/Avr1993')
plot(5, X[5], Y[5], 'lnRsb\nVir1994/Wild2008')
plot(6, X[6], Y[6], 'lnRsb\nVir1998/Avr1993')
axes[0].set_ylim(0, 50000)
axes[1].set_ylim(0, 240000)
axes[2].set_ylim(0, 240000)
axes[3].set_ylim(0, 50000)
axes[0].set_yticks([0, 20000, 40000])
axes[0].set_yticklabels([0, '2e4', '4e4'])
axes[1].set_yticks([0, 100000, 200000])
axes[1].set_yticklabels([0, '1e5', '2e5'])
axes[2].set_yticks([0, 100000, 200000])
axes[2].set_yticklabels([0, '1e5', '2e5'])
axes[3].set_yticks([0, 20000, 40000])
axes[3].set_yticklabels([0, '2e4', '4e4'])

axes[0].set_ylim(*axes[0].get_ylim())
axes[1].set_ylim(*axes[1].get_ylim())
axes[2].set_ylim(*axes[2].get_ylim())
axes[3].set_ylim(*axes[3].get_ylim())
axes[4].set_ylim(*axes[4].get_ylim())
axes[5].set_ylim(*axes[5].get_ylim())
axes[6].set_ylim(*axes[6].get_ylim())

### PLOT GENES ###
for ax in axes[:7]:
    a, b = ax.get_ylim()
    b -= a
    ax.broken_barh([(d['first'], d['last']-d['first']) for d in genes], (a,b), fc='0.8', ec='None')
axes[7].broken_barh([(d['first'], d['last']-d['first']) for d in genes if d['strand'] == '+'], (0.2, 0.2), fc='0.8', ec='None')
axes[7].broken_barh([(d['first'], d['last']-d['first']) for d in genes if d['strand'] == '-'], (0.0, 0.2), fc='0.8', ec='None')
axes[7].axhline(0.2, ls='-', c='k', lw=8)
nsyn_up = []
nsyn_dw = []
for i in nsyn:
    if i<FIRST or i>LAST: continue
    for d in genes:
        if i>=d['first'] and i<=d['last']:
            if d['strand'] == '+': nsyn_up.append(i)
            else: nsyn_dw.append(i)
            break
    else:
        raise ValueError

axes[7].plot(nsyn_up, [0.5] * len(nsyn_up), marker='|', ms=20, mec='k', mfc='None', c='None')
axes[7].plot(nsyn_dw, [-0.1] * len(nsyn_dw), marker='|', ms=20, mec='k', mfc='None', c='None')

### FINISH ####

# configure and save figure
print('saving graph...', end='', flush=True)
for ax in axes[:7]:
    ax.set_xlim(FIRST, LAST)
    ax.tick_params(axis='y', length=10, left=True, right=True)

ticksM = list(range(FIRST, LAST+1, 20000))
ticksm = list(range(FIRST, LAST+1, 5000))
for ax in axes[0:7]:
    ax.set_xticks(ticksM, minor=False)
    ax.set_xticks(ticksm, minor=True)
    ax.tick_params(axis='x', which='major', length=10, labelbottom=False, bottom=True, top=True, direction='inout')
    ax.tick_params(axis='x', which='minor', length=5, bottom=True, top=True, direction='inout')

axes[6].set_xticks(ticksM, minor=False)
axes[6].set_xticks(ticksm, minor=True)
axes[6].set_xticklabels(['{0:.2f}'.format(j/1e6) for j in ticksM], minor=False)
axes[6].tick_params(axis='x', which='major', labelbottom=True)

axes[7].text((FIRST+LAST)/2, 0.9, 'Position on chromosome {0} (Mbp)'.format(LG[2:]), va='center', ha='center')
axes[7].text((FIRST+LAST)/2, -0.45, 'Genes and non-synonymous mutations', va='center', ha='center')
axes[7].set_xlim(FIRST, LAST)
axes[7].set_ylim(-0.5, 1)
axes[7].tick_params(axis='both', left=False, bottom=False, labelleft=False, labelbottom=False)
axes[7].spines['top'].set_color('none')
axes[7].spines['left'].set_color('none')
axes[7].spines['right'].set_color('none')
axes[7].spines['bottom'].set_color('none')

# savefig
fig.savefig('Supplementary_figure_7.png', bbox_inches='tight')
print('\b\b\b done')
