"""
Annotate SNPs

PATH: work directory
LG: which LG to use
FIRST/LAST: limits of the zoom
"""

### LEGEND OF Additional file corresponding to annotated sites (backup)

"""
\subsection*{Additional file 13 --- Annotation of SNPs in focus region}
    The file is a plain text space-separated table with a header line. 
    Column \#1: SNP position. Column \#2: status flag. 0: intergenic, 3: 
    5' untranscribed region (promoter region), 1: 3' untranscribed 
    region, 7: pre-start intron, 23: 5' untranslated region exon, 5: 
    post-stop intron, 21: 3' untranslated region exon, 13: intron in 
    coding region, 29: synonymous, 61: non-synonymous. The flag can be 
    manipulated bit-wise (bit 0x1: genic, bit 0x10: before ATG, bit 
    0x100: premature mRNA, bit 0x1000: coding region, including exons, 
    bit 0x10000: exons, including UTR, bit 0x1000000non-synonymous). 
    Column \#3: gene name if relevant. Column \#4: gene strand if 
    relevant. Column \#5: protein ID if relevant. Column \#6: protein 
    length if relevant. Column \#7: codon index from start if relevant. 
    Column \#8: qualifier flag. Bit 0x1: uncertain because at least one 
    of the two other sites was missing (reference allele was assumed), 
    bit 0x10: more than one position of codon has a substitution, bit 
    0x100: more than two possible codons for at least one individual 
    (because the phase is unknown), bit 0x1000: more than two codons 
    overall (might be the effect of the previous), bit 0x10000: only 
    one codon allele due to missing data, bit 0x10000: amino acid 
    alleles include a stop codon. Qualifier flag is 0 when not 
    relevant. Column \#9: comma-separated list of codons if relevant. 
    Column \#10: comma-separated list of amino acids (matching codons) 
    if relevant. If a column is not relevant, it is replaced by a dot 
    unless otherwise specified.

\subsection*{Additional file 14 --- Genotypes for non-synonymous SNPs in focus region}
    The file is a plain text space-separated table with a header line. 
    Column \#1: SNP position. Column \#2: protein ID. Column \#3: status 
    flag. Column \#4: qualifier flag. Column \#5: list of codons. Column 
    \#6: list of amino acids (matching codons). Column \#7: gene strand. 
    Column \#8: position of the site within the codon (on the strand of 
    the gene). Columns \#9 to \#84: genotype for all isolates (on the strand 
    of the gene).
"""









import os, egglib, math, sys
sys.path.append('../data')
import lib


PATH = os.path.expanduser('~/disks/backup2/work/pop-genomics-work')
GFF = os.path.expanduser('~/data/projects/rust_genome/Mellp2_3/Mellp2_3_GeneCatalog_20151130.gff3')
REF = os.path.expanduser('~/data/projects/rust_genome/Mellp2_3/Mellp2_3_AssemblyScaffolds_Repeatmasked.fasta')
PROT = os.path.expanduser('~/data/projects/rust_genome/Mellp2_3/Mellp2_3_GeneCatalog_proteins_20151130.aa.fasta')
LG = 'LG15'
FIRST = 2100000
LAST = 2300000

# load genes
fname = os.path.join('cache', 'genes_{0}_{1}_{2}.txt'.format(LG, FIRST, LAST))
if not os.path.isfile(fname):
    with open(fname, 'w') as f:
        print('loading reference sequence')
        ref = egglib.io.from_fasta(REF, alphabet=egglib.alphabets.DNA)
        ref = ref.find('LG_' + LG[2:]).sequence

        print('loading protein database')
        protdb = egglib.io.from_fasta(PROT, alphabet=egglib.alphabets.protein)
        for seq in protdb: seq.name = seq.name.split('|')[2]

        print('loading annotation')
        gff = egglib.io.GFF3(GFF)
        for feat in gff.iter_features('LG_' + LG[2:], None, None, True):
            if feat.type == 'CDS':

                # extract information
                gene = feat.parents[0].parents[0]
                protID = gene.attributes['proteinId']
                cds_lims = [(i, j) for (i,j,k) in feat.segments]
                cds_pos = [b for (i,j,k) in feat.segments for b in range(i, j+1)]
                while len(cds_pos)%3 != 0:
                    if feat.strand == '+': cds_pos.append(None)
                    else: cds_pos.insert(0, None)
                cds_bases = ''.join([ref[i] if i is not None else '?' for i in cds_pos])

                # check validity of feature
                x = [j for i in feat.segments for j in i[:2]]
                assert x == sorted(x)
                if feat.strand == '+': testseq = egglib.tools.translate(cds_bases)
                else: testseq = egglib.tools.translate(egglib.tools.rc(cds_bases))
                testseq = testseq.rstrip('X')
                protseq = protdb.find(protID).sequence[:].rstrip('X')
                if testseq != protseq:
                    assert len(testseq) == len(protseq)
                    for i,j in zip(testseq, protseq):
                        if i!=j and j != 'X': raise AssertionError
                assert '*' not in testseq or testseq.index('*') == len(testseq) - 1

                # export name / strand / limits / exons / CDS
                if gene.end >= FIRST and gene.start <= LAST:
                    name = gene.attributes['Name']
                    cds = ['{0}-{1}'.format(i, j) for i, j in cds_lims]
                    cds_pos = ','.join(map(str, cds_pos))
                    exons = [(i.start, i.end) for i in feat.parents[0].descendants if i.type == 'exon']
                    exons.sort()
                    c = 0
                    for a, b in exons:
                        assert b>=a and a>c
                        c = b
                    exons = ['{0}-{1}'.format(i, j) for (i, j) in exons]
                    f.write('{0} {1} {2} {3} {4} {5} {6} {7} {8}\n'.format(name,
                                                                       protID,
                                                                       gene.strand,
                                                                       gene.start,
                                                                       gene.end,
                                                                       ','.join(exons),
                                                                       ','.join(cds),
                                                                       cds_pos,
                                                                       cds_bases))

print('loading gene positions')
genes = []
with open(fname) as f:
    for line in f:
        name, protID, strand, start, end, exons, cds_lims, cds_pos, cds_bases = line.split()
        start = int(start)
        end = int(end)
        exons = [tuple(map(int, i.split('-'))) for i in exons.split(',')]
        cds_lims = [tuple(map(int, i.split('-'))) for i in cds_lims.split(',')]
        cds_pos = list(map(lambda x: None if x == 'None' else int(x), cds_pos.split(',')))
        assert len(cds_pos)%3 == 0
        assert len(cds_pos) == len(cds_bases)
        cds_bases = dict(zip(cds_pos, cds_bases))
        codons = [cds_pos[i:i+3] for i in range(0, len(cds_pos), 3)]

        codon_db = {}
        for i, (a,b,c) in enumerate(codons):
            codon_db[a] = i
            codon_db[b] = i
            codon_db[c] = i
        if cds_pos.count(None) == 2:
            assert set(cds_pos) == set(codon_db) | {None}
        else:
            assert set(cds_pos) == set(codon_db)

        genes.append({'name': name, 'protID': protID, 'strand': strand,
                        'start': start, 'end': end,
                        'cds_lims': cds_lims,
                        'cds_pos': cds_pos, 'exons': exons,
                        'codons': codons, 'codon_db': codon_db, 'cds_bases': cds_bases})


# load raw sites (no filter on depth and on number of alleles)
print('importing sites: 0.00Mbp', end='')
sites = {}
cur = 0
n = len(sites)
with open(os.path.join(PATH, 'filter-sites', LG+'.txt')) as f:
    h = f.readline().split()
    isolate_list = h[2:]
    for line in f:
        pos, status, *site = line.split()
        assert len(site) == len(isolate_list)
        pos = int(pos) - 1
        if int(pos/1e4) != cur:
            cur = int(pos/1e4)
            print('\b\b\b\b\b\b\b{0:.2f}Mbp'.format(pos/1e6), end='', flush=True)
        if pos < FIRST: continue
        if pos > LAST: break
        alls = set(''.join(site))
        alls.discard('?')
        if len(alls) > 1: assert status == 'V'
        else: assert status == 'F'
        sites[pos] = site, status

print('\b\b\b\b\b\b\b{0} sites imported'.format(len(sites)))

# querying functions
def find_gene(p):
    for gene in genes:
        if p >= gene['start'] and p <= gene['end']:
            return gene
    return None

def in_segment(p, array):
    for a,b in array:
        if p>=a and p<=b: return True
    return False

# process sites
with open('annotation.txt', 'w') as outf:
    outf.write('position status gene strand protein_ID protein_len codon_index qualifiers codons amino_acids\n')
    with open('annotation-sites.txt', 'w') as outf2:
        outf2.write('position protein_ID status qualifiers codons amino_acids strand codon_pos ' + ' '.join(isolate_list) + '\n')
        for pos in sites:
            if sites[pos][1] == 'V':

                # analyse site
                line = ['.'] * 10
                status = 0
                qualifiers = 0
                line[0] = pos + 1

                g = find_gene(pos)
                # genic
                if g is not None:
                    status |= 1
                    line[2] = g['name']
                    line[3] = g['strand']
                    line[4] = g['protID']
                    line[5] = len(g['codons'])

                    # before ATG
                    if g['strand'] == '+':
                        if pos < g['codons'][0][0]:
                            status |= 2
                    else:
                        if pos > g['codons'][-1][2]:
                            status |= 2

                    # in pre-mRNA
                    if pos >= g['exons'][0][0] and pos <= g['exons'][-1][1]:
                        status |= 4

                        # in CDS
                        if pos >= g['cds_lims'][0][0] and pos <= g['cds_lims'][-1][1]:
                            status |= 8

                        # in exon (CDS or not)
                        if in_segment(pos, g['exons']):
                            status |= 16

                    # in CDS exon
                    if status&24==24:

                        # get the three positions in which the site falls
                        pp = g['codons'][g['codon_db'][pos]]
                        codon_pos = g['codon_db'][pos] + 1
                        if g['strand'] == '-':
                            codon_pos = line[5] - codon_pos + 1
                        line[6] = codon_pos

                        triplet = []
                        variable = [0] * 3
                        for i, p in enumerate(pp):
                            if p in sites:
                                triplet.append(sites[p][0])
                                if sites[p][1] == 'V': variable[i] = 1
                            else:
                                triplet.append(([g['cds_bases'][p] * 2 for i in range(76)]))
                                qualifiers |= 1

                        # check if more than one site
                        assert sum(variable) > 0
                        if sum(variable) > 1:
                            qualifiers |= 2

                        # gets codons for all isolates
                        assert list(map(len, triplet)) == [76] * 3
                        all_codons = set()
                        missing = 0
                        for trp in zip(*triplet):
                            if '??' in trp:
                                missing += 1
                                continue # skip codons with missing data altogether
                            codons = set()
                            for i in trp[0]:
                                for j in trp[1]:
                                    for k in trp[2]:
                                        codon = i+j+k
                                        if g['strand'] == '-': codon = egglib.tools.rc(codon)
                                        codons.add(codon)
                            if len(codons) > 2:
                                qualifiers |= 4
                            all_codons.update(codons)
                        codons = list(all_codons)
                        assert len(codons) > 0
                        if len(codons) > 2:
                            qualifiers |= 8
                        if len(codons) == 1:
                            assert missing > 0
                            qualifiers |= 16

                        # check translations
                        aas = list(map(egglib.tools.translate, codons))
                        assert 'X' not in aas
                        if len(set(aas)) > 1:
                            if '*' in aas:
                                qualifiers |= 32
                            status |= 32

                            loc = pp.index(pos)
                            if g['strand'] == '-':
                                loc = 3-loc
                                site = [i.translate(str.maketrans('ACGT', 'TGCA')) for i in sites[pos][0]]
                            else:
                                site = sites[pos][0]
                            outf2.write(' '.join([str(pos+1), g['protID'], str(status), str(qualifiers),
                                    ','.join(codons), ','.join(aas), g['strand'], str(loc)] + site) + '\n')

                        line[8] = ','.join(codons)
                        line[9] = ','.join(aas)

                # export
                assert status in [0, 3, 1, 7, 23, 5, 21, 13, 29, 61]
                line[1] = status
                line[7] = qualifiers
                outf.write(' '.join(map(str, line)) + '\n')

print('done')

"""
Output table columns:
    #1 position
    #2 status flag:
        0: intergenic
        3: 5' untranscribed region (promoter region)
        1: 3' untranscribed region
        7: pre-start intron
        23: 5' untranslated region exon
        5: post-stop intron
        21: 3' untranslated region exon
        13: intron in coding region
        29: synonymous
        61: non-synonymous
        bit-wise flags:
            status&1: genic
            status&2: before ATG
            status&4: premature mRNA
            status&8: coding region (including exons)
            status&16: exons (including UTR)
            status&32: non-synonymous
        useful bits combinations:
            status&20==20: mRNA exons
            status&24==24: coding sequence exons
            status&7==7: 5' UTR
            status&3==3: anything in gene before start codon
    #3 gene name (. for intergenic)
    #4 gene strand
    #5 protein ID (. for intergenic)
    #6 protein length (. for intergenic)
    #7 codon index from start (. if not coding sequence exons)
    #8 qualifier flag with bits:
        flag&1: uncertain because at least one of the two other sites
                was missing (reference allele was assumed)
        flag&2: more than one position of codon has a substitution
        flag&4: more than two possible codons for at least one
                individual (phase unknown)
        flag&8: more than two codons overal (might be the effect of the
                previous)
        flag%16: only one codon allele due to missing data
        flag%32: substitution adding/removing a stop codon
        qualifiers is 0 when not relevant
    #9 comma-separated list of codons (. outside CDS exons)
    #10 comma-separated list of amino acids (. outside CDS exons)
"""
