"""
PATH: work directory
WSIZE/WSTEP: which sliding window analysis to use
K: which parameter value of the freq-hmm analysis to use
BP_THR: BayPass significance threshold
GW_THR: GWAS significance threshold
EHH_THR: EHH detection threshold
LG: which LG to use
FIRST/LAST: limits of the zoom
"""

import os, sys, math, matplotlib, re, egglib
from matplotlib import pyplot
import zoom_lib

# parameters
PATH = os.path.expanduser('~/WORK1/pop-genomics-work')
WSIZE = 1000
WSTEP = 250
K = '1e-20'
BP_THR = 6
GW_THR = 6
EHH_THR = 4
LG = 'LG15'
FIRST = 2100000
LAST = 2300000

# initiate picture
matplotlib.rc('font', size=20)
fig, axes = pyplot.subplots(7, 1, figsize=(40, 30))
candidates_all = set()

### GET ANNOTATION ###
sites, nsyn, genes = zoom_lib.get_annotation(LG, FIRST, LAST)

#### GET SUMMARY STATISTICS ####

# get stats
xD = []
yD = []
boundsD = []
for sample in 'avr7', 'vir7':
    boundsD.append([])
    xD.append([])
    yD.append([])
    print('processing {0}'.format(sample), end='', flush=True)
    with open(os.path.join(PATH, 'stats-windows-wsize{0}-wstep{1}'.format(WSIZE, WSTEP), '{0}-{1}.txt'.format(LG, sample))) as f:
        header = f.readline().split()
        for line in f:
            line = line.split()
            assert len(line) == len(header)
            d = dict(zip(header, line))

            bounds = int(d['start']), int(d['stop'])
            if bounds[0] >= FIRST and bounds[1] <= LAST:
                v = d['D']
                if v != 'None':
                    boundsD[-1].append(bounds)
                    xD[-1].append((bounds[0]+bounds[1])/2)
                    yD[-1].append(float(v))
    print(' done')

xtheta = []
ytheta = []
with open(os.path.join(PATH, 'stats-sites', '{0}.txt'.format(LG))) as f:
    h = f.readline().split()
    for line in f:
        line = line.split()
        assert len(line) == len(h)
        d = dict(zip(h, line))
        pos = int(d['pos'])
        if pos >= FIRST and pos <= LAST:
            assert d['theta2'] != 'None'
            xtheta.append(pos)
            ytheta.append(float(d['theta2']))

xdpt = []
ydpt = []
print('getting depth') # only for sites with polymorphism
with open(os.path.join(PATH, 'depths', f'{LG}.txt')) as f:
    print('0.0Mbp\b\b\b\b\b\b', end='', flush=True)
    cur = 0
    for line in f:
        pos, d = line.split()
        pos = int(pos) - 1
        if pos > LAST: break
        d = float(d)
        p = pos//100000
        if p!=cur:
            print(f'{p/10}\b\b\b', end='', flush=True)
            cur=p
        if pos in xtheta:
            xdpt.append(pos+1)
            ydpt.append(d)

print('depth values imported')


### PLOT XTX TEST FROM BAYPASS ###

# get P-values
print('processing Baypass results...', end='', flush=True)
Pval = []
with open(os.path.join('..', '11-baypass', 'core', 'output_summary_pi_xtx.out')) as f:
    h = f.readline().split()
    idx = h.index('log10(1/pval)')
    for line in f:
        line = line.split()
        assert len(line) == len(h)
        Pval.append(float(line[idx]))

# get marker positions
markers = []
with open(os.path.join('..', '11-baypass', 'logfile.txt')) as f:
    for line in f:
        idx, lg, pos = line.split()
        idx = int(idx)
        pos = int(pos)
        markers.append((lg, pos))
        assert idx == len(markers)
assert len(markers) == len(Pval)

# get significant markers
candidates_BP = []
with open(os.path.join('..', '11-baypass', 'core', 'markers_1e-{0}.txt'.format(BP_THR))) as f:
    for line in f:
        idx, lg, pos, P = line.split()
        idx = int(idx) - 1
        pos = int(pos)
        P = float(P)
        assert markers[idx] == (lg, pos)
        assert Pval[idx] == P
        candidates_BP.append(pos)
candidates_all.update(candidates_BP)

# organize data and plot
xBayPass = []
yBayPass = []
for (lg, pos), P in zip(markers, Pval):
    if lg == LG and pos >= FIRST and pos <= LAST:
        xBayPass.append(pos)
        yBayPass.append(P)

print('\b\b\b: done')


### PLOT GWAS ###
print('processing GWAS...', end='', flush=True)

topX = []
with open(os.path.join('..', '13-GWAS', 'GWAS_variance-partition.txt')) as f:
    f.readline()
    for line in f:
        last = line.split()[-1]
        if last[1] == '+':
            a, b, c = last.split('_')
            c = int(c.rstrip('"'))
            if b == LG[2:] and c >= FIRST and c <= LAST:
                topX.append(c)

xGWAS = []
yGWAS = []
candidates_GW = []
topY = [None] * len(topX)

with open(os.path.join('..', '13-GWAS', 'GWAS_allPval_step0.txt')) as f:
    h = f.readline()
    for line in f:
        loc, P = line.split()
        lg, pos = re.match('"SNP_(\d{2})_(\d+)"', loc).groups()
        pos = int(pos)
        if lg == LG[2:] and pos >= FIRST and pos <= LAST:
            P = float(P)
            P = math.log10(1/P)
            if pos in topX:
                assert topY[topX.index(pos)] is None
                topY[topX.index(pos)] = P
            else:
                xGWAS.append(pos)
                yGWAS.append(P)
            if P > GW_THR: candidates_GW.append(pos)
candidates_all.update(candidates_GW)
assert None not in topY

print('\b\b\b done')

### PLOT RSB FROM EHH ###

# import values
print('processing EHH...', end='', flush=True)
xiEG94 = []
yiEG94 = []
with open(os.path.join('..', '12-EHH', 'data.txt')) as f:
    h = f.readline().split()
    for line in f:
        line = line.split()
        assert len(line) == len(h)
        d = dict(zip(h, line))
        if d['LG'] == LG:
            pos = int(d['pos'])
            if pos >= FIRST and pos <= LAST:
                if d['1994'] != 'NA':
                    xiEG94.append(pos)
                    yiEG94.append(float(d['1994']))

# get significant SNPs
candidates_EHH = []
rsb = 1
with open(os.path.join('..', '12-EHH', 'candidates', 'Rsb{0}_lvl{1}.txt'.format(rsb, EHH_THR))) as f:
    f.readline()
    for line in f:
        lg, pos, iEG, Rsb = line.split()
        if lg == LG:
            pos = int(pos)
            if pos >= FIRST and pos <= LAST:
                candidates_EHH.append(pos)
candidates_all.update(candidates_EHH)
print('\b\b\b done')


### PLOT FREQ-HMM

print('processing freq-hmm...', end='', flush=True)

# get data
xP = []
yP = []
sweeps = []

# load probas
with open(os.path.join(PATH, 'freq-hmm', 'vir7-{0}_{1}.post'.format(LG, K))) as f:
    for i, line in enumerate(f):
        pos, p = line.split()
        pos = int(pos)
        p = float(p)
        if pos >= FIRST and pos <= LAST:
            xP.append(pos)
            yP.append(math.log10(p))

# load sweep locations
with open(os.path.join(PATH, 'freq-hmm', 'vir7-{0}_{1}.stat'.format(LG, K))) as f:
    num = int(f.readline())
    cur = 0
    for i in range(num):
        p1, p2, P = f.readline().split()
        p1 = int(p1)
        p2 = int(p2)
        p = float(p)
        assert p1 > cur and p2 > p1
        cur = p2
        if p1 >= FIRST and p2 <= LAST:
            sweeps.append((p1, p2-p1))
    assert f.readline() == ''
assert len(sweeps) == 0 # if not, update candidates_all
print('\b\b\b done')

## PLOT ##
def plot(idx, X, Y, lbl, candidates=None):
    d = dict(zip(X, Y))
    if candidates is not None:
        X = sorted(set(d) - set(candidates))
        Xc = sorted(set(candidates) & set(d))
        Y = [d[i] for i in X]
        Yc = [d[i] for i in Xc]
    else:
        Xc = []
        Yc = []
        Xcn = []
        Ycn = []

    axes[idx].plot(X, Y, marker='o', ms=10, ls='None', mec='k', mfc='None')
    axes[idx].plot(Xc, Yc, marker='o', ms=10, ls='None', mec='r', mfc='None')

    Xcn = sorted(set(d) & set(nsyn) & set(candidates_all))
    Ycn = [d[i] for i in Xcn]
    a, b = axes[idx].get_ylim()
    offset = 0.07 * (b-a)
    axes[idx].plot(Xcn, [i+offset for i in Ycn], marker='v', ms=12, ls='None', mec='m', mfc='m')
    axes[idx].set_ylabel(lbl)

plot(0, xdpt, ydpt,  'Sequencing\ndepth')
#plot(, xD[0], yD[0],  'Tajima\'s $D$\navirulent')
#plot(, xD[1], yD[1],  'Tajima\'s $D$\nvirulent')
plot(1, xP, yP, 'freq-hmm\n$log_{10}(P)$')
axes[1].set_yticks([-60, -40, -20, 0])
plot(2, xtheta, ytheta, '$\hat{\\theta_2}$ vir v. avr\n(per site)')
plot(3, xBayPass, yBayPass, 'Baypass\n$log_{10}\\left( 1/P \\right)$', candidates_BP)
axes[3].set_ylim(0, axes[3].get_ylim()[1])
axes[3].axhline(BP_THR, c='k', ls=':')
plot(4, xGWAS, yGWAS, 'GWAS\n$log_{10} \\left( 1/P \\right)$', candidates_GW)
axes[4].plot(topX, topY, '*', mec='r', mfc='None', ms=18)
axes[4].axhline(GW_THR, c='k', ls=':')
axes[4].set_yticks([0, 5, 10])
plot(5, xiEG94, [i for i in yiEG94], 'EHH\niEG Vir1994', candidates_EHH)
axes[5].set_yticks([0, 1e5, 2e5])
axes[5].set_yticklabels([0, '$1e5$', '$2e5$'])

axes[0].set_ylim(*axes[0].get_ylim())
axes[1].set_ylim(*axes[1].get_ylim())
axes[2].set_ylim(*axes[2].get_ylim())
axes[3].set_ylim(*axes[3].get_ylim())
axes[4].set_ylim(*axes[4].get_ylim())
axes[5].set_ylim(*axes[5].get_ylim())

### PLOT GENES ###
for ax in axes[:6]:
    a, b = ax.get_ylim()
    b -= a
    ax.broken_barh([(d['first'], d['last']-d['first']) for d in genes], (a,b), fc='0.8', ec='None')
axes[6].broken_barh([(d['first'], d['last']-d['first']) for d in genes if d['strand'] == '+'], (0.2, 0.2), fc='0.8', ec='None')
axes[6].broken_barh([(d['first'], d['last']-d['first']) for d in genes if d['strand'] == '-'], (0.0, 0.2), fc='0.8', ec='None')
axes[6].axhline(0.2, ls='-', c='k', lw=8)
nsyn_up = []
nsyn_dw = []
for i in nsyn:
    if i<FIRST or i>LAST: continue
    for d in genes:
        if i>=d['first'] and i<=d['last']:
            if d['strand'] == '+': nsyn_up.append(i)
            else: nsyn_dw.append(i)
            break
    else:
        raise ValueError

nsyn_up_sign = sorted(set(nsyn_up) & candidates_all)
nsyn_dw_sign = sorted(set(nsyn_dw) & candidates_all)
nsyn_up_nsig = sorted(set(nsyn_up) - candidates_all)
nsyn_dw_nsig = sorted(set(nsyn_dw) - candidates_all)

axes[6].plot(nsyn_up, [0.5] * len(nsyn_up), marker='|', ms=20, mec='k', mfc='None', c='None')
axes[6].plot(nsyn_dw, [-0.1] * len(nsyn_dw), marker='|', ms=20, mec='k', mfc='None', c='None')
axes[6].plot(nsyn_up_sign, [0.65] * len(nsyn_up_sign), marker='v', ms=12, mec='m', mfc='m')
axes[6].plot(nsyn_dw_sign, [-0.25] * len(nsyn_dw_sign), marker='^', ms=12, mec='m', mfc='m')

### FINISH ####

# configure and save figure
print('saving graph...', end='', flush=True)
for ax in axes[:6]:
    ax.set_xlim(FIRST, LAST)
    ax.tick_params(axis='y', length=10, left=True, right=True)

ticksM = list(range(FIRST, LAST+1, 20000))
ticksm = list(range(FIRST, LAST+1, 5000))
for ax in axes[0:6]:
    ax.set_xticks(ticksM, minor=False)
    ax.set_xticks(ticksm, minor=True)
    ax.tick_params(axis='x', which='major', length=10, labelbottom=False, bottom=True, top=True, direction='inout', labelcolor='w')
    ax.tick_params(axis='x', which='minor', length=5, bottom=True, top=True, direction='inout')

axes[5].set_xticks(ticksM, minor=False)
axes[5].set_xticks(ticksm, minor=True)
axes[5].set_xticklabels(['{0:.2f}'.format(j/1e6) for j in ticksM], minor=False)
axes[5].tick_params(axis='x', which='major', labelbottom=True, labelcolor='k')

axes[6].text((FIRST+LAST)/2, 0.9, 'Position on chromosome {0} (Mbp)'.format(LG[2:]), va='center', ha='center')
axes[6].text((FIRST+LAST)/2, -0.45, 'Genes and non-synonymous mutations', va='center', ha='center')
axes[6].set_xlim(FIRST, LAST)
axes[6].set_ylim(-0.5, 1)
axes[6].tick_params(axis='both', left=False, bottom=False, labelleft=False, labelbottom=False, labelcolor='w')
axes[6].spines['top'].set_color('none')
axes[6].spines['left'].set_color('none')
axes[6].spines['right'].set_color('none')
axes[6].spines['bottom'].set_color('none')

# savefig
fig.savefig('Fig3.png', bbox_inches='tight')
print('\b\b\b done')

# save candidates common to BP and GWAS
ad_hoc = [2232600, 2233055, 2233369, 2237229]
    # list of Baypass candidates in gene 1427900 (n=3) and 2237229 (top GWAS candidate)
sites = {}
with open(os.path.join(PATH, 'filter-sites', 'LG15.txt')) as f:
    a, b, *names = f.readline().split()
    assert a == 'pos'
    assert b == 'type'
    for line in f:
        pos, status, *site = line.split()
        pos = int(pos)
        if pos < FIRST: continue
        if pos > LAST: break
        if pos in ad_hoc:
            assert status == 'V'
            sites[pos] = site
assert sorted(sites) == ad_hoc

with open('selected-candidates.txt', 'w') as f:
    f.write('Position ' + ' '.join(map(str, ad_hoc)) + '\n')
    for i in sites.values():
        assert len(i) == len(names)
    for i, name in enumerate(names):
        f.write(name)
        for p in ad_hoc:
            f.write(' ' + sites[p][i])
        f.write('\n')
