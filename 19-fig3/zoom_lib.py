"""
Helper file to gather common code between zoom.py and zoom-ehh.py
"""

def get_annotation(LG, FIRST, LAST):
    sites = {}
    with open('annotation.txt') as f:
        h = f.readline().split()
        for line in f:
            line = line.split()
            assert len(line) == len(h)
            d = dict(zip(h, line))
            pos = int(d['position'])
            if (pos >= FIRST and pos <= LAST):
                status = int(d['status'])
                qualifiers = int(d['qualifiers'])
                sites[pos] = status, qualifiers, d['strand']
    nsyn = [pos for pos in sites if sites[pos][0] == 61]

    genes = []
    with open('cache/genes_{0}_{1}_{2}.txt'.format(LG, FIRST, LAST)) as f:
        for line in f:
            bits = line.split()
            genes.append({
                'name': bits[0],
                'prot': bits[1],
                'strand': bits[2],
                'first': int(bits[3]),
                'last': int(bits[4])})

    return sites, nsyn, genes
