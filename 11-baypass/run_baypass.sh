#under core model
baypass -gfile ~/WORK/pop_genomics_2020/baypass/input.txt -outprefix output -nthreads 3 
#under covariate model
baypass -gfile ~/WORK/pop_genomics_2020/baypass/input.txt -contrastfile ~/WORK/pop_genomics_2020/baypass/contrast.txt -outprefix output -nthreads 3 -print_omega_samples ~/WORK/pop_genomics_2020/baypass/omega_samples.txt
