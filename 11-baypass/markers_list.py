"""
generate list of significative markers 
"""

out3 = open('markers_1e-3.txt', 'w')
out4 = open('markers_1e-4.txt', 'w')
out5 = open('markers_1e-5.txt', 'w')
out6 = open('markers_1e-6.txt', 'w')

M3=[]
M4=[]
M5=[]
M6=[]

with open('output_summary_pi_xtx.out') as f:
    f.readline()
    for line in f:
        MRK, MP, SDP, MXtX, SDXtX, XtXst, val = line.split()
        if float(val) >= 3:
            M3.append((MRK, val))
        if float(val) >= 4:
            M4.append((MRK, val))
        if float(val) >= 5:
            M5.append((MRK, val))
        if float(val) >= 6:
            M6.append((MRK, val))


for i in range(len(M3)): 
    with open('logfile.txt') as f2:
        for lines in f2:
            pos_ab, LG, pos_rela = lines.split()
            if M3[i][0] == pos_ab:
                out3.write('{0}\t{1}\t{2}\t{3}\n'.format(pos_ab, LG, pos_rela, M3[i][1]))
f2.close()

for i in range(len(M4)): 
    with open('logfile.txt') as f2:
        for lines in f2:
            pos_ab, LG, pos_rela = lines.split()
            if M4[i][0] == pos_ab:
                out4.write('{0}\t{1}\t{2}\t{3}\n'.format(pos_ab, LG, pos_rela, M4[i][1]))
f2.close()

for i in range(len(M5)): 
    with open('logfile.txt') as f2:
        for lines in f2:
            pos_ab, LG, pos_rela = lines.split()
            if M5[i][0] == pos_ab:
                out5.write('{0}\t{1}\t{2}\t{3}\n'.format(pos_ab, LG, pos_rela, M5[i][1]))
f2.close()

for i in range(len(M6)): 
    with open('logfile.txt') as f2:
        for lines in f2:
            pos_ab, LG, pos_rela = lines.split()
            if M6[i][0] == pos_ab:
                out6.write('{0}\t{1}\t{2}\t{3}\n'.format(pos_ab, LG, pos_rela, M6[i][1]))
f2.close()
