"""
Prepare the input file for baypass (input.txt) and write down the list
of loci in logfile.txt

PATH: work directory.
"""

import re
import sys, os
import egglib
sys.path.append('../data')
import lib

PATH = os.path.expanduser('~/WORK/pop-genomics-work/filter-sites/')
logfile = open('logfile.txt', 'w')


pop = []
locus_index=0

for LGi in range(18):
    LG = 'LG{0:0>2}'.format(LGi+1)
    print(LG)
    with open(PATH + '{0}.txt'.format(LG)) as f:
        f.readline()
        for line in f:            
            pos, cat, *site = line.split()
            if (int(pos)-1) % 1000000 == 0:
                print('{0} {1:.2f}Mbp'.format(LG, (int(pos)-1)/1e6))
            if cat == 'F': continue
            site = egglib.site_from_list(''.join(site), alphabet=egglib.alphabets.DNA)
            frq = egglib.stats.freq_from_site(site, lib.dict_structs['4pop'])
            if frq.num_alleles < 2: continue
            locus_index += 1
            pop.append((frq.freq_allele(0, frq.population, 0) , frq.freq_allele(1, frq.population, 0), frq.freq_allele(0, frq.population, 1) , frq.freq_allele(1, frq.population, 1), frq.freq_allele(0, frq.population, 2) , frq.freq_allele(1, frq.population, 2), frq.freq_allele(0, frq.population, 3) , frq.freq_allele(1, frq.population, 3)))
            logfile.write('{0}\t{1}\t{2}\n'.format(locus_index, LG, pos))
print((len(pop)))

with open('input.txt', 'w') as out:
    for i in range(locus_index):
        out.write('{0}\n'.format("\t".join(map(str, pop[i]))))
