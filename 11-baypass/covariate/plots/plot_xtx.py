"""
Make Manhattan plots with XtX p-val statistic
"""
 
from matplotlib import pyplot, rcParams

a, b = rcParams['figure.figsize']
rcParams['figure.figsize'] = 6*a, 2*b
rcParams['font.size'] *= 2.5

offset = 0
length = [8567145,7400714,7995696,6413982,6145269,7015942,5819754,6014601,4829805,4781055,5560581,4992572,4013672,4116462,4671214,4243487,4451399,4402489]
length_mid=[4.2835725, 12.267502, 19.965707000000002, 27.170545999999998, 33.4501715, 40.030777, 46.44862500000001, 52.3658025, 57.7880055, 62.5934355, 67.76425350000001, 73.04083, 77.54395199999999, 81.609019, 86.00285699999999, 90.4602075, 94.80765050000001, 99.2345945]

LGname = ['LG01', 'LG02', 'LG03', 'LG04', 'LG05', 'LG06', 'LG07', 'LG08', 'LG09', 'LG10', 'LG11', 'LG12', 'LG13', 'LG14', 'LG15', 'LG16', 'LG17', 'LG18']
i=0
main_fig, main_axes = pyplot.subplots(1)

for LG in range(18):
    LG = 'LG{0:0>2}'.format(LG+1)
    print(LG)

    ax_lim=0
    pos_tot = []
    pos =[]
    val = []

    f = open('{0}.txt'.format(LG))
    for line in f:
        nb, pos_rea, xval = line.split()
        pos.append(float((int(pos_rea))/1000000.0))
        pos_tot.append(float((offset+(int(pos_rea)))/1000000.0))
        val.append(float(xval))

    lg_fig, lg_axes = pyplot.subplots(1)
    lg_axes.plot(pos, val, marker='o', color='k' ,markersize=6, ls='None')
    ax_lim = float(((length[i]+1)/1000000.0)+0.1)
    lg_axes.set_xlim(-0.1, ax_lim)
    lg_axes.set_ylabel('log10(1/pval)')
    lg_axes.set_xlabel('Mb')
    lg_axes.axhline(6, c='r', ls=':')
    lg_fig.savefig('{0}.png'.format(LG))
    main_axes.plot(pos_tot, val, marker='o',markersize=6, ls='None')
    main_axes.set_xticks([])
    pyplot.close(lg_fig)
    offset += int(length[i])
    i+=1

main_axes.set_ylabel('log10(1-pval)')
main_axes.set_xlim(-1,102.435839)
main_axes.set_xticks(length_mid)
main_axes.set_xticklabels(LGname)
main_axes.axhline(6, c='r', ls=':')
main_fig.savefig('pval.png')

