"""
Compute the number of differences between each pair of samples and
save data in an ad hoc table named matrix.txt.

PATH: work directory.
MAX_THREADS: number of parallelization threads.
"""

# loading
import sys, egglib, multiprocessing, random
sys.path.append('../data')
import lib
PATH = '/home/user/WORK/pop-genomics-work/filter-sites'
MAX_THREADS = 20

ns = 76

# initialize matrix
matrix = []
for sam1 in range(ns):
    matrix.append([])
    for sam2 in range(ns):
        if sam1 == sam2: matrix[sam1].append(('N', 'A'))
        else: matrix[sam1].append([0, 0])
print('number of samples:', ns)
def diff(a, b, c, d):
    if a==c and b==d: return 0
    if a==d and b==c: return 0
    if a==c: return 1
    if a==d: return 1
    if b==c: return 1
    if b==d: return 1
    return 2

def process(LGi, queue):
    LG = 'LG{0:0>2}'.format(LGi+1)
    cur = 0
    mat = []
    num = 0
    for sam1 in range(ns):
        mat.append([])
        for sam2 in range(ns):
            if sam1 == sam2: mat[sam1].append(('N', 'A'))
            else: mat[sam1].append([0, 0])
    with open(PATH + '/{0}.txt'.format(LG)) as f:
            f.readline() # header
            for line in f:
                pos, cat, *site = line.split()
                pos = int(pos) - 1
                if cat == 'F': continue
                if random.random() > 0.01: continue
                num += 1
                site = egglib.site_from_list(''.join(site), alphabet=egglib.alphabets.DNA)
                assert site.ns == 2*ns
                for i in range(ns):
                    for j in range(ns):
                        if i==j: continue
                        else:
                            if site[2*i] == '?' or site[2*j] == '?': continue
                            assert site[2*i+1] != '?' and site[2*j+1] != '?'
                            mat[i][j][0] += 1
                            mat[i][j][1] += diff(site[2*i], site[2*i+1], site[2*j], site[2*j+1])
                x = pos//20000
                if x != cur:
                    cur = x
                    print('{0}: {1:.2f} / {2:.2f} Mbp num: {3}'.format(LG, pos/1e6, lib.LGs[LG]/1e6, num))
    print('terminate', LG)
    queue.put(mat)

# start processes
print('running on {0} CPUs'.format(MAX_THREADS))
queue = multiprocessing.Queue()
wait = []
for i in range(18):
    p = multiprocessing.Process(target=process, args=[i, queue])
    wait.append(p)
random.shuffle(wait)
for i in range(MAX_THREADS):
    if len(wait) > 0:
        p = wait.pop()
        p.start()

# run
nt = 0
ndone = 0
while True:
    mat = queue.get()
    ndone += 1
    print('done: {0}'.format(ndone))
    for i, row in enumerate(mat):
        for j, (a, b) in enumerate(row):
            if (a, b) != ('N', 'A'):
                nt += a
                matrix[i][j][0] += a
                matrix[i][j][1] += b
    if ndone == 18:
        break
    elif len(wait) > 0:
        p = wait.pop()
        p.start()


f = open('matrix.txt', 'w')
f.write('total number of SNPs: {0}\n'.format(nt))
f.write('samples: {0}\n'.format(','.join(lib.samples)))
for row in matrix:
    f.write(' '.join(['{0};{1}'.format(i, j) for (i, j) in row]) + '\n')
f.close()
