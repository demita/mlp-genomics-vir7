"""
Build the neighbour-joining tree using PHYLIP. The neighbor program of
the PHYLIP package must be available in the path.

Generate a tree saved in output.tre in the current directory, and write
down the PHYLIP version in a file named phylip-version.txt.

The tree can be viewed with, e.g., iTOL @ https://itol.embl.de.
"""

import subprocess, os, re

if os.path.isfile('outfile'): os.unlink('outfile')
if os.path.isfile('outtree'): os.unlink('outtree')

p = subprocess.Popen(['neighbor'], stdin=subprocess.PIPE)
p.communicate(b"""input.txt
Y
""")

version = re.search('Neighbor-Joining/UPGMA method version (.+)', open('outfile').read()).group(1)
os.unlink('outfile')

os.rename('outtree', 'output.tre')
f = open('phylip-version.txt', 'w')
f.write(version + '\n')
f.close()


