"""
Plot data saved in the "data.txt" file.
"""

import matplotlib
from matplotlib import pyplot
import sys
sys.path.append('../data')
import lib

matplotlib.rc('font', size=20)

samples = {'1993': 'Avr1993',
           '1994': 'Vir1994',
           '1998': 'Vir1998',
           '2008': 'Wild2008',
           'avr7': 'avirulent',
           'vir7': 'virulent'}

# import data
iEG = {k: [] for k in samples}
Rsb = ([], [], [], [])

LG = 'LG00'
print('importing: LG00', end='')
with open('data.txt') as f:
    h = f.readline().split()
    for line in f:
        line = line.split()
        assert len(line) == len(h)
        d = dict(zip(h, line))
        if d['LG'] != LG:
            LG = d['LG']
            for k in samples: iEG[k].append(([], []))
            for i in Rsb: i.append(([], []))
            print('\b'*4+LG, end='', flush=True)
        pos = int(d['pos'])

        for k in samples:
            if d[k] != 'NA':
                iEG[k][-1][0].append(pos)
                iEG[k][-1][1].append(float(d[k]))
        for i in range(4):
            k = 'Rsb'+str(i+1)
            if d[k] != 'NA':
                Rsb[i][-1][0].append(pos)
                Rsb[i][-1][1].append(float(d[k]))
print('\b' * 4 + 'done')

# offset for all chromosomes
offset = {}
mids = []
acc = 0
for LG in lib.LGs:
    offset[LG] = acc
    mids.append(acc + lib.LGs[LG] / 2)
    acc += lib.LGs[LG]

# initiate main figure
geom = (40, 40)
fig0, axes0 = pyplot.subplots(10, 1, figsize=geom)

Rsb_names = 'Vir1994/Avr1993', 'Vir1994/Wild2008', 'virulent/avirulent', 'Vir1998/Avr1993'
def set_axes(axes):
    c = 0
    for ax, sam in zip(axes, samples.values()):
        t = ax.text(0.5, 0.90, sam, horizontalalignment='center', verticalalignment='center', transform=ax.transAxes)
        t.set_fontweight('bold')
        ax.set_ylabel('iEG')
        ax.set_ylim(0, 300000)
        ax.set_yticks([0, 100000, 200000, 300000])
        ax.set_yticklabels(['0', '1e5', '2e5', '3e5'])
        c += 1
    for idx in range(4):
        t = axes[c+idx].text(0.5, 0.05, Rsb_names[idx], horizontalalignment='center', verticalalignment='center', transform=axes[c+idx].transAxes)
        t.set_fontweight('bold')
        axes[c+idx].set_ylabel('lnRsb')
        axes[c+idx].set_yticks([-5, -2.5, 0, 2.5, 5])

# process chromosomes
print('plotting... 0000', end='')
for LGi, LG in enumerate(lib.LGs):
    print('\b'*4 + LG, end='', flush=True)
    col = ['0.0', '0.5'][LGi%2]

    fig, axes = pyplot.subplots(10, 1, figsize=geom)
    for i, k in enumerate(samples):
        x, y = iEG[k][LGi]
        axes[i].plot([i/1e6 for i in x], y, marker='o', ms=1, ls='None', c='k')
        x = [i+offset[LG] for i in x]
        axes0[i].plot(x, y, marker='o', ms=1, ls='None', mec=col)

    for Rsbi in range(4):
        x, y = Rsb[Rsbi][LGi]
        axes[len(samples)+Rsbi].plot([i/1e6 for i in x], y, marker='o', ms=1, ls='None', zorder=3, c='k')
        x = [i+offset[LG] for i in x]
        axes0[len(samples)+Rsbi].plot(x, y, marker='o', ms=1, ls='None', zorder=3, mec=col)

    # save LG figure
    for ax in axes:
        ax.set_xlim(0, lib.LGs[LG]/1e6)
        ax.xaxis.set_tick_params(color='None')
        if ax != axes[-1]: ax.set_xticks([])
    set_axes(axes)
    axes[-1].set_xlabel('Position on {0} (Mbp)'.format(LG))
    fig.savefig('plots/{0}.png'.format(LG))
    pyplot.close(fig)

print('\b'*4 + 'done')

# save main figure
for ax in axes0:
    ax.set_xlim(0, sum(lib.LGs.values()))
    ax.xaxis.set_tick_params(color='None')
    ax.set_xticks([])
    ax.tick_params(axis='y', length=10, right=True)
set_axes(axes0)
axes0[-1].set_xticks(mids)
axes0[-1].set_xticklabels(['chr' + i[2:] for i in lib.LGs.keys()])

fig0.savefig('Supplementary_figure_6.png', bbox_inches='tight')
