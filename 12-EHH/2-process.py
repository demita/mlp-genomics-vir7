"""
Process iEG values and compute Rsb ratio as defined in Tang et al. PLoS
Biology 2007 and create a "data.txt" file faster to load for plotting.

PATH: work directory.
MIN_N: minimum number of sites used to compute iEG.
MIN_DS: minimum distance used to compute iEG.
"""

import os, math, sys
sys.path.append('../data')
import lib

PATH = os.path.expanduser('~/disks/backup2/work/pop-genomics-work')
MIN_N = 10
MIN_DS = 1000

SAMPLES = ['1993', '1994', '1998', '2008', 'avr7', 'vir7']

# helper functions
def convert(x):
    if x == 'NA': return None
    try: return int(x)
    except ValueError: return float(x)

# import iEG values
data = []

print('importing... LG00', end='')
idx = 0
for LGi in range(18):
    LG = 'LG{0:0>2}'.format(LGi+1)

    print('\b\b\b\b' + LG, end='', flush=True)
    with open(PATH + '/EHH/{0}.txt'.format(LG)) as f:

        # import double header line
        keys = list(map(str.strip, f.readline().rstrip().strip('|').split('|')[1:]))
        header = list(map(str.strip, f.readline().rstrip().strip('|').split('|')))
        assert header[0] == 'pos'
        del header[0]
        header = [i.split() for i in header]

        # read each line
        for line in f:

            # map value to the double header items
            line = list(map(str.strip, line.rstrip().strip('|').split('|')))
            pos = int(line.pop(0)) # keep 1-based numbering
            line = {key: {k: convert(v) for (k, v) in zip(h, value.split())} for (key, h, value) in zip(keys, header, line)}

            # import values based on threshold
            item = {'LG': LG, 'pos': pos, 'idx': idx}
            idx += 1
            for k in SAMPLES:
                if line[k]['num'] >= MIN_N and line[k]['ds'] >= MIN_DS:
                    item[k] = line[k]['iEG']
            data.append(item)

print('\b\b\b\bdone')

# compute Rsb
print('computing Rsb... ', end='', flush=True)
for i, row in enumerate(data):
    if '1993' in row and '1994' in row: row['Rsb1'] = math.log(row['1994']/row['1993'])
    if '2008' in row and '1994' in row: row['Rsb2'] = math.log(row['1994']/row['2008'])
    if 'avr7' in row and 'vir7' in row: row['Rsb3'] = math.log(row['vir7']/row['avr7'])
    if '1998' in row and '1993' in row: row['Rsb4'] = math.log(row['1998']/row['1993'])
for k in ['Rsb1', 'Rsb2', 'Rsb3', 'Rsb4']:
    values = [item[k] for item in data if k in item]
    n = len(values)
    med = sorted(values)[int(n/2)]
    sd = math.sqrt(sum(map(lambda x: x**2, values)) / n - (sum(values) / n)**2)
    for item in data:
        if k in item:
            item[k] = (item[k] - med) / sd

# select candidates positions
for k in ['Rsb1', 'Rsb2', 'Rsb3', 'Rsb4']:
    copy = [item for item in data if k in item]
    copy.sort(key = lambda x: -x[k])

    for lvl in 2,3,4,5:
        c = int(10**-lvl * len(copy))
        candidates = copy[:c]
        ref = c-1
        while abs(copy[c][k] - candidates[ref][k]) < 1e-6:
            candidates.append(copy[c])
            c += 1
        with open('candidates/{0}_lvl{1}.txt'.format(k, lvl), 'w') as f:
            f.write('expected: {0} candidates: {1}\n'.format(ref+1, len(candidates)))
            for item in candidates:
                if k == 'Rsb3': iEG = item['vir7']
                elif k == 'Rsb4': iEG = item['1998']
                else: iEG = item['1994']
                f.write('{0[LG]} {0[pos]} {1} {2}\n'.format(item, iEG, item[k]))

# export a script-friendly table
print('exporting... ', end='', flush=True)
with open('data.txt', 'w') as f:
    f.write('LG pos ' + ' '.join(SAMPLES) + ' Rsb1 Rsb2 Rsb3 Rsb4\n')
    for item in data:
        f.write('{0[LG]} {0[pos]}'.format(item))
        for k in SAMPLES + ['Rsb1', 'Rsb2', 'Rsb3', 'Rsb4']:
            f.write(' {0}'.format(item[k]) if k in item else ' NA')
        f.write('\n')
print('done')
