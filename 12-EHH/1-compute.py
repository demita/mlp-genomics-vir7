"""
Compute genotypic iES for all sites of all chromosomes.
Only sites with all three genotypes above a given frequency are
considered for seeding (all biallelic sites used to compute EHH).

PATH: work directory.
THRESHOLD: EHHS limit to stop incrementing iEG.
MIN_MIN: minimal genotype frequency (for all three genotypes) to include site.
NUM_THREADS: number of parallelization threads.
"""

import egglib, os, sys, multiprocessing
sys.path.append('../data')
import lib

PATH = os.path.expanduser('~/WORK1/pop-genomics-work')
THRESHOLD = 0.20
NUM_THREADS = 18
keys = ['0000', '1993', '1994', '1998', '2008', 'vir7', 'avr7']

def process(LGi, queue):
    LG = 'LG{0:0>2}'.format(LGi+1)
    sites = []

    # load all SNPs of chromosome
    with open(PATH + '/filter-sites/{0}.txt'.format(LG)) as f:
        for line in f:
            pos, status, * site = line.split()
            if status == 'V':
                site = egglib.site_from_list(''.join(site), alphabet=egglib.alphabets.DNA)
                site.position = int(pos) - 1
                sites.append(site)
                if len(sites) % 5000 == 0:
                    print('loading {0}: {1:.2f}Mbp'.format(LG, int(pos)/1e6))


    # compute EHHG for sites
    with open(PATH + '/EHH/{0}.txt'.format(LG), 'w') as outf:

        outf.write('|         |           0000           |           1993           |           1994           |           1998           |           2008           |           vir7           |           avr7           |\n')
        outf.write('|    pos  |   num     ds         iEG |   num     ds         iEG |   num     ds         iEG |   num     ds         iEG |   num     ds         iEG |   num     ds         iEG |   num     ds         iEG |\n')

        EHH = egglib.stats.EHH()
        num = {i: 0 for i in keys}
        for idx in range(len(sites)):
            if (idx+1) % 1000 == 0:
                print('{0} {1:.2f}/{2:.2f}Mbp'.format(LG, sites[idx].position/1e6, lib.LGs[LG]/1e6))

            # process all pops (including overall)
            iEG = dict.fromkeys(keys, None)
            num_sites = dict.fromkeys(keys, 0)
            tot_len = dict.fromkeys(keys, 0)
            for pop in keys:

                iEG[pop] = 0.0
                num[pop] += 1
                EHH.set_core(sites[idx], struct=lib.dict_structs[pop])
                i = idx + 1
                lo = sites[idx].position
                hi = sites[idx].position
                while i < len(sites):
                    EHH.load_distant(sites[i])
                    i += 1
                    if EHH.get_EHHG() < THRESHOLD:
                        hi = sites[i-1].position
                        num_sites[pop] += i - idx - 1
                        break
                iEG[pop] += EHH.get_iEG()
                EHH.set_core(sites[idx], struct=lib.dict_structs[pop])
                i = idx - 1
                while i >= 0:
                    EHH.load_distant(sites[i])
                    i -= 1
                    if EHH.get_EHHG() < THRESHOLD:
                        lo = sites[i+1].position
                        num_sites[pop] += idx - i - 1
                        break
                iEG[pop] += EHH.get_iEG()
                tot_len[pop] = int(hi - lo)

            # export
            outf.write('| {0:>7d}'.format(int(sites[idx].position+1)))
            for pop in keys:
                outf.write(' | {0:>5d} {1:>6d}'.format(num_sites[pop], tot_len[pop]))
                if iEG[pop] is None: outf.write('          NA')
                else: outf.write(' {0:>11.4f}'.format(iEG[pop]))
            outf.write(' |\n')

    print(LG, 'number of sites:', len(sites), 'used per pop:', num['1993'], num['1994'], num['1998'], num['2008'], num['vir7'], num['avr7'])
    queue.put(LG)

# prepare process
wait = []
queue = multiprocessing.Queue()
for i in range(18):
    p = multiprocessing.Process(target=process, args=[i, queue])
    wait.append(p)
wait = wait[::-1]

# start processes
print('running on {0} CPUs'.format(NUM_THREADS))
for i in range(NUM_THREADS):
    wait.pop().start()

# monitor running
for i in range(18):
    res = queue.get()
    print('###', res, 'done')
    if len(wait):
        print('#### starting next')
        wait.pop().start()
    print('### to go:', 17 - i)


